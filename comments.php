 <?php 

/**************************************
COMMENTS CALLBACK
***************************************/

	function canon_venuex_comments($comment, $args, $depth) {
		$GLOBALS['comment'] = $comment;

		?>

		<li id="comment-<?php comment_ID(); ?>" <?php comment_class(); ?>>

			<div>

				<!-- AVATAR -->
				<?php 

					if (get_option('show_avatars') === '1' && get_avatar($comment, $args['avatar_size'], '', 'comment-avatar')) {
						echo '<div class="left">';
						echo get_avatar($comment,$args['avatar_size'],'', 'comment-avatar');
						echo '</div>';	
					}

				?>

				<!-- META -->
				<h5><?php comment_author_link(); ?></h5> 
				<h6><?php echo mb_localize_datetime(get_comment_date(get_option('date_format') . ' (' . get_option('time_format') .')')); ?></h6>

				<!-- REPLY AND EDIT LINKS -->
				<?php comment_reply_link(array_merge( $args, array('reply_text' => esc_html__('Reply', 'loc_canon_venuex'), 'depth' => $depth, 'max_depth' => $args['max_depth'])), $comment->comment_ID); ?>
				<?php edit_comment_link(esc_html__('Edit', 'loc_canon_venuex')); ?>

				<!-- THE COMMENT -->
				<?php if ($comment->comment_approved == '0') { printf('<span class="approval_pending_notice">%s</span>', esc_html__('Comment awaiting approval', 'loc_canon_venuex')); } ?>

				<?php comment_text(); ?>
				
			</div>

	<?php 
	}

?>
        	                       		

				<div class="coms"> 

					<!-- ANCHOR TAG -->
					<a name="comments"></a>

						
					<!-- DISPLAY COMMENTS -->
					<?php 
						echo "<h4>";
						comments_number(esc_html__('No Replies','loc_canon_venuex'), esc_html__('1 Reply','loc_canon_venuex'), '% ' . esc_html__('Replies','loc_canon_venuex') );
						printf(" %s \"%s\"",esc_html__('to', 'loc_canon_venuex'), wp_kses_post($post->post_title));
						echo "</h4>";

						echo "<ul class='comments'>";
						
							wp_list_comments(array(
								'avatar_size'	=> 65,
								'max_depth'		=> 5,
								'style'			=> 'ul',
								'callback'		=> 'canon_venuex_comments',
								'type'			=> 'all'
							));

					 	echo "</ul>";

						echo "<div id='comments_pagination'>";
							paginate_comments_links(array('prev_text' => '&laquo;', 'next_text' => '&raquo;'));
						echo "</div>";

						echo "<hr/>";

						$custom_comment_field = '<textarea class="full" placeholder="'.esc_html__('Comment', 'loc_canon_venuex').'" id="comment" name="comment" cols="20" rows="5" aria-required="true"></textarea>';  //label removed for cleaner layout

						//vars for fields
						$commenter = wp_get_current_commenter();
						$req = get_option( 'require_name_email' );
						$aria_req = ( $req ? " aria-required='true'" : '' );

						comment_form(array(
							'fields' => apply_filters( 'comment_form_default_fields', array(
										'author' => '<div class="clearfix"><input class="third" placeholder="'.esc_html__('Name', 'loc_canon_venuex').'" id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '"' . $aria_req . ' />',
										'email' => '<input class="third" placeholder="'.esc_html__('Email', 'loc_canon_venuex').'" id="email" name="email" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) . '"' . $aria_req . ' />',
										'url' => '<input class="third field-last" placeholder="'.esc_html__('Website', 'loc_canon_venuex').'" id="url" name="url" type="text" value="' . esc_attr( $commenter['comment_author_url'] ) . '"/></div>' 
									)),
							'comment_field'			=> $custom_comment_field,
							'comment_notes_before' 	=> '',
							'comment_notes_after'	=> '',
							'logged_in_as' 			=> '',
							'title_reply'			=> esc_html__('Got something to say?', 'loc_canon_venuex'),
							'cancel_reply_link'		=> esc_html__('Cancel reply', 'loc_canon_venuex'),
							'label_submit'			=> esc_html__('Send Message', 'loc_canon_venuex')
						));
					 ?>

				</div>
