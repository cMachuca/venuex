"use strict";

/*************************************************************
SCRIPTS INDEX

ISOTOPE: INIT
ISOTOPE: GALLERY FILTER MENU
ISOTOPE: MASONRY GALLERY FILTER MENU
FLEXSLIDER INIT
FANCYBOX INIT
OWL CAROUSEL INIT
MINOR PLUGINS INIT
	  	// SCROLLUP
		// FITVIDS
		// MOSAIC
 	  	// STELLAR PARALLAX
		// EMBED SCROLL PROTECT
SIDR RESPONSIVE MENU
MAIN.JS
ADD NAV-PARENT CLASS TO NAV MENU ITEMS WITH SUBMENUS
SLIDER NAVIGATION ON HOVER ONLY
TOGGLE
ACCORDION
HIGHLIGHT LAST MENU ITEM
EVEN HEIGHT
TIMELINE
AJAX MULTIPOST PAGINATION
COUNTDOWN
PAGEBUILDER TOP HORIZONTAL RULER
CLICKABLE BACKGROUND
STICKY HEADER
	SCROLL TO ANCHOR
	SEARCH BUTTON
SEARCH AUTOCOMPLETE
@FONT-FACE FIX
LAZY LOAD ANIMATION
MENU ANIMATION
TABLEPRESS TIMETABLE MOD: DETECT TIMETABLE SLOT STATUS
SET NEGATIVE MARGIN FOR OVERLAY-HEADER CLASS + APPLY IS-OVERLAID-HEADER CLASS
SCROLL TO MENU HIGHLIGHTING
POLLS

*************************************************************/



  

/*************************************************************
ISOTOPE: INIT
*************************************************************/


	jQuery(window).load(function($) {

		$=jQuery;

		if ($('.page_isotope_gallery').size() > 0) {
			$('.page_isotope_gallery').isotope({
				itemSelector: '.gallery_item',
				layoutMode: 'fitRows'
			});
		}

		//$('.page_isotope_gallery').isotope('layout');

		if ($('.page_masonry_gallery').size() > 0) {

			$('.page_masonry_gallery').isotope({
				itemSelector: '.gallery_item',
				layoutMode: 'masonry'
			});
		}


	});

	
	// RELAYOUT ON RESIZE
	jQuery(window).resize(function() {

		$ = jQuery;

		// bounce
		if ($('.page_masonry_gallery').size() == 0) { return; }

		$('.page_masonry_gallery').isotope('layout');
		
	});

/*************************************************************
ISOTOPE: GALLERY FILTER MENU
*************************************************************/

	jQuery(document).ready(function($) {
		if ($('.filters li').size() > 0) {
			
			//apply selected class to first menu item (show all)
			$('.filters li:eq(1) a').addClass('selected');

			$('.filters li a').on('click', function (event) {
				event.preventDefault();
				var $this = $(this);
				var $this_gallery = $this.closest('aside').next();
				var numColumns = $this_gallery.attr('data-num_columns');

				//update selected filter item
				$('.filters li a').removeClass('selected');
				$this.addClass('selected');


				var filterVar = $this.closest('li').attr('class');
				if ( (typeof filterVar == 'undefined') || (filterVar.indexOf('cat-item-all') != -1) )  {
					filterVar = "*";
				} else {
					filterVar = filterVar.split(' ');
					filterVar = "." + filterVar[1];
				}
				$this_gallery.isotope({ filter: filterVar});

				//recalculate last item
				var $filteredItems = $('.gallery_item:not(.isotope-hidden)');
				if ($filteredItems.size() > 0) {

					$filteredItems.each(function(index, e) {
						$this = $(this);
						$this.removeClass('last');
						if (((index+1) % numColumns) === 0) $this.addClass('last');
					});

				$('.page_isotope_gallery').isotope('layout');
						
				}
			});

		}
	});


/*************************************************************
ISOTOPE: MASONRY GALLERY FILTER MENU
*************************************************************/

	jQuery(document).ready(function($) {
		if ($('.gallery-filter li').size() > 0) {
			
			//apply selected class to first menu item (show all)
			$('.gallery-filter li:eq(0) a').addClass('selected');

			$('.gallery-filter li a').on('click', function (event) {
				event.preventDefault();
				var $this = $(this);
				var thisGallerySelector = $this.closest('.gallery-filter').attr('data-associated_gallery_selector');
				var $this_gallery = $(thisGallerySelector);
				var numColumns = $this_gallery.attr('data-num_columns');

				//update selected filter item
				$('.gallery-filter li a').removeClass('selected');
				$this.addClass('selected');


				var filterVar = $this.closest('li').attr('class');
				if ( (typeof filterVar == 'undefined') || (filterVar.indexOf('cat-item-all') != -1) )  {
					filterVar = "*";
				} else {
					filterVar = filterVar.split(' ');
					filterVar = "." + filterVar[1];
				}
				$this_gallery.isotope({ filter: filterVar});

			});

		}
	});


/*************************************************************
FLEXSLIDER INIT
*************************************************************/

	jQuery(window).load(function($){
		$ = jQuery;

		if ($('.flexslider-standard').size() > 0) {

			var canonAnimImgSliderSlidershow = (extData.canonOptionsAppearance['anim_img_slider_slideshow'] == 'checked') ? true : false;

			$('.flexslider-standard').flexslider({
				slideshow: canonAnimImgSliderSlidershow,
				slideshowSpeed: parseInt(extData.canonOptionsAppearance['anim_img_slider_delay']),
				animationSpeed: parseInt(extData.canonOptionsAppearance['anim_img_slider_anim_duration']),
				animation: "fade",
				smoothHeight: true,
				touch: true,
				controlNav: true,
				directionNav: true,
				prevText: "",
				nextText: "",
				start: function(slider){
					$('body').removeClass('loading');
				}
			});

		}

		if (($('.flexslider-quote').size() > 0) && ($('#hp_tweets').size() === 0)) {

			var canonAnimQuoteSliderSlidershow = (extData.canonOptionsAppearance['anim_quote_slider_slideshow'] == 'checked') ? true : false;

			$('.flexslider-quote').flexslider({
				slideshow: canonAnimQuoteSliderSlidershow,
				slideshowSpeed: parseInt(extData.canonOptionsAppearance['anim_quote_slider_delay']),
				animationSpeed: parseInt(extData.canonOptionsAppearance['anim_quote_slider_anim_duration']),
				animation: "fade",
				smoothHeight: true,
				touch: true,
				controlNav: true,
				directionNav: false,
				prevText: "",
				nextText: "",
				start: function(slider){
					$('body').removeClass('loading');
				}
			});	 

		}

		if ( ($('.flexslider-menu').size() > 0) ) {

			var canonAnimMenuSliderSlidershow = (extData.canonOptionsAppearance['anim_menu_slider_slideshow'] == 'checked') ? true : false;

			$('.flexslider-menu').flexslider({
				slideshow: canonAnimMenuSliderSlidershow,
				slideshowSpeed: parseInt(extData.canonOptionsAppearance['anim_menu_slider_delay']),
				animationSpeed: parseInt(extData.canonOptionsAppearance['anim_menu_slider_anim_duration']),
				animation: "fade",
				smoothHeight: true,
				touch: true,
				directionNav: false,
				controlNav: true,
				prevText: "",
				nextText: "",
				start: function(slider){
					$('body').removeClass('loading');
				}
			});	 

		}

	});


/*************************************************************
FANCYBOX INIT
*************************************************************/

	//attach fanybox class to all image links
	jQuery(document).ready(function($) {
		$("a[href$='.jpg'],a[href$='.png'],a[href$='.gif']").attr('rel','gallery').addClass('fancybox');
	});

	//init fancybox
	jQuery(document).ready(function($) {

		// default fancybox
		if ($(".fancybox").size() > 0) {
			var canonLightboxColor = extData.canonOptionsAppearance['lightbox_overlay_color'];
			var canonLightboxOpacity = extData.canonOptionsAppearance['lightbox_overlay_opacity'];;

			$(".fancybox").fancybox({

				openEffect	: 'fade',	//fade, elastic, none
				closeEffect	: 'fade',
				openSpeed	: 'normal',	//slow, normal, fast or ms
				closeSpeed	: 'fast',

				helpers : {
			        overlay : {
			            css : {
			                'background' : jQuery.GlobalFunctions.hexOpacityToRgbaString(canonLightboxColor, canonLightboxOpacity)
			            }
			        }
			    }
			});
		}


		// media fancybox - same as fancybox but lets you open media links in lightbox and set a max height of 500px
		if ($(".fancybox-media").size() > 0) {
			var canonLightboxColor = extData.canonOptionsAppearance['lightbox_overlay_color'];
			var canonLightboxOpacity = extData.canonOptionsAppearance['lightbox_overlay_opacity'];;

			$(".fancybox-media").fancybox({

				openEffect	: 'fade',	//fade, elastic, none
				closeEffect	: 'fade',
				openSpeed	: 'normal',	//slow, normal, fast or ms
				closeSpeed	: 'fast',

				helpers : {
			        overlay : {
			            css : {
			                'background' : jQuery.GlobalFunctions.hexOpacityToRgbaString(canonLightboxColor, canonLightboxOpacity)
			            },
			        },
					media : {},
			    },

			    maxHeight: '500',

			});
		}

	});


/*************************************************************
OWL CAROUSEL INIT
*************************************************************/
	
	jQuery(document).ready(function($) {


		if ($(".post-carousel").size() > 0) {
			
			// for each post-carousel read settings and init
			$('.post-carousel').each(function(index){
				var $this = $(this);
				var displayNumPosts = $this.attr('data-display_num_posts');
				var slideSpeed = $this.attr('data-slide_speed');
				var autoPlay = ($this.attr('data-autoplay_speed') === "0") ? false : parseInt($this.attr('data-autoplay_speed'));
				var stopOnHover = ($this.attr('data-stop_on_hover') == 'checked') ? true : false;
				var pagination = ($this.attr('data-pagination') == 'checked') ? true : false;

				var blockID = $this.closest('.pb_block').attr('id');

				$('#'+ blockID).find('.post-carousel').owlCarousel({
					items: parseInt(displayNumPosts),
					slideSpeed: parseInt(slideSpeed),
					autoPlay: autoPlay,
					stopOnHover: stopOnHover,
					pagination: pagination,
				});

			});

			// navigation: next
			$('.owlCustomNavigation .next2').on('click', function(event) {
				var $this = $(this);
				var $related_carousel = $this.closest('.text-seperator').next('.owl-carousel');
				$related_carousel.trigger('owl.next')
			});

			// navigation: prev
			$('.owlCustomNavigation .prev2').on('click', function(event) {
				var $this = $(this);
				var $related_carousel = $this.closest('.text-seperator').next('.owl-carousel');
				$related_carousel.trigger('owl.prev')
			});

		}

	});


/*************************************************************
MINOR PLUGINS INIT
*************************************************************/


	jQuery(document).ready(function($) {

	  	// SCROLLUP
	  	$.scrollUp({
	  	    scrollName: 'scrollUp', 	// Element ID
	        scrollDistance: 300,         // Distance from top/bottom before showing element (px)
	        scrollFrom: 'top',           // 'top' or 'bottom'
	        scrollSpeed: 500,            // Speed back to top (ms)
	        easingType: 'linear',        // Scroll to top easing (see http://easings.net/)
	        animation: 'fade',           // Fade, slide, none
	        animationSpeed: 200,         // Animation speed (ms)
	  	    scrollText: '',			 // Text for element, can contain HTML
	        // scrollTrigger: false,        // Set a custom triggering element. Can be an HTML string or jQuery object
	        // scrollTarget: false,         // Set a custom target element for scrolling to. Can be element or number
	        // scrollTitle: false,          // Set a custom <a> title if required.
	        // scrollImg: false,            // Set true to use image
	        // activeOverlay: false,        // Set CSS color to display scrollUp active point, e.g '#00FFFF'
	        // zIndex: 2147483647           // Z-Index for the overlay
		});


		// FITVIDS
		$(".outter-wrapper").fitVids();


		// MOSAIC
		$('.fade').mosaic();
	 

 	  	// STELLAR PARALLAX
		$(window).stellar({
	  		horizontalScrolling: false,
	  	});


		// EMBED SCROLL PROTECT
		$('.embed-scroll-protect').mbEmbedScrollProtect();
		$('.tribe-events-venue-map').mbEmbedScrollProtect();

	});

/*************************************************************
SIDR RESPONSIVE MENU
*************************************************************/

	jQuery(document).ready(function($) {

		// SIDR
		$('.responsive-menu-button').sidr({
		    name: 'sidr-main',
		    source: '#sidr-navigation-container',
		    renaming: false,
		});
		$('#sidr-main .closebtn').click(function() {
		    $.sidr('close', 'sidr-main');
		});

	});

	jQuery(window).resize(function() {

		// SIDR CLOSE ON RESIZE
		jQuery.sidr('close', 'sidr-main');
		
	});


/*************************************************************
ADD NAV-PARENT CLASS TO NAV MENU ITEMS WITH SUBMENUS
*************************************************************/

	jQuery(document).ready(function($) {
		var $subMenus = $('.sub-menu');
		$subMenus.each(function(index) {
			var $this = $(this);
			$this.closest('li').addClass('nav-parent');
		});

	});



/*************************************************************
MAIN.JS
*************************************************************/


	jQuery(document).ready(function($) {

		// Parent-Nav Hover	
		$("li.nav-parent").hover(function(){
			$(this).addClass("hover");

		}, function(){
		    $(this).removeClass("hover");
		});
		
	});


/*************************************************************
SLIDER NAVIGATION ON HOVER ONLY
*************************************************************/


	jQuery(window).load(function($) {
		$=jQuery;
		
		// bouncer
		if ($('.flexslider ul.flex-direction-nav').size() == 0) { return; }

		$('.flexslider').each(function (index) {

			var $this = $(this);

			var $slidesNavArrows = $this.find('.flex-direction-nav');
			var $slidesNavBullets = $this.find('.feature .flex-control-nav.flex-control-paging');
			$slidesNavArrows.hide();
			$slidesNavBullets.hide();

			//on slider hover
			$this.hover(function () {
				$slidesNavArrows.fadeIn();
				$slidesNavBullets.fadeIn();
			}, function() {
				$slidesNavArrows.hide();
				$slidesNavBullets.hide();
			});

			//on navigation button hover
			$slidesNavArrows.hover(function () {
				$slidesNavArrows.show();
				$slidesNavBullets.show();
			});

		});

	});



/*************************************************************
TOGGLE
*************************************************************/

	jQuery(document).ready(function($){

		if ($('.toggle-btn').size() > 0) {

			// toggle	  
			$('.toggle-btn').click(function(e){
				e.preventDefault();
				$(this).closest('li').find('.toggle-content').not(':animated').slideToggle();
				$(this).toggleClass("active");
			});
			
		}


	});


  


/*************************************************************
ACCORDION
*************************************************************/

	jQuery(document).ready(function($){

		if ($('.accordion-btn').size() > 0) {
			
			// initial states
			$('.accordion-content:not(.active)').hide();

			// accordion	  
			$('.accordion-btn').click(function(e){
				e.preventDefault();
				var $this = $(this);
				var $thisAccordionContent = $this.closest('li').find('.accordion-content');
				var currentStatus = "";
				if ($this.attr('class').indexOf('active') != -1) {
					currentStatus = "active";
				}
				//first close all and remove active class
				$this.closest('.accordion').find('li').each(function(index) {
					var $thisLi = $(this);
					$thisLi.find('.accordion-btn').removeClass('active');
					$thisLi.find('.accordion-content').slideUp('400', function() {
						$(this).removeClass('active');
					});
				});
				if (currentStatus != "active") {
					$thisAccordionContent.not(':animated').slideDown();
					$this.addClass('active');
					$thisAccordionContent.addClass('active');
				}
			});

		}
		
	});



/*************************************************************
EVEN HEIGHT

by: Michael Bregnbak
Use: apply even-height class to any container and add data-even_height_group with group name. Now all containers with even-height class and same group name will be even height. Recalculates on window resize for responsiveness.
*************************************************************/

	jQuery(document).ready(function($){

		// EVEN HEIGHT SCRIPT
		if ($('.even-height').size() > 0) {
			
			// first get even height groups
			var $evenHeightDivs = $('.even-height');
			var evenHeightGroupsArray = [];
			$evenHeightDivs.each(function(index, element) {
				var $this = $(this);
				evenHeightGroupsArray.push($this.data('even_height_group'));
			});
			var evenHeightGroupsArrayUnique = [];
			$.each(evenHeightGroupsArray, function(index, element) {
			    if ($.inArray(element, evenHeightGroupsArrayUnique) === -1) evenHeightGroupsArrayUnique.push(element);
			});

			// make each group same height
			make_same_height(evenHeightGroupsArrayUnique);

			//recalculate on window resize
			$(window).resize(function () {
				$evenHeightDivs.css('height','auto');
				make_same_height(evenHeightGroupsArrayUnique);
			});

			// trigger window resize event to accomodate late arrival content
			jQuery(window).load(function($) { jQuery(window).trigger('resize'); });

		}

		function make_same_height(evenHeightGroupsArrayUnique) {

			$.each(evenHeightGroupsArrayUnique, function(index, element) {
				var $thisEvenHeightGroup = $('.even-height[data-even_height_group="'+element+'"]');
				var heightsArray = [];
				$thisEvenHeightGroup.each(function(index, element) {
					var $this = $(this);
					heightsArray.push($this.height());
				});
				var tallest = 0;
				$.each(heightsArray, function(index, element) {
				    if (element > tallest) tallest = element;
				});
				$thisEvenHeightGroup.height(tallest);
					
			});
		}
		
	});


/*************************************************************
TIMELINE
*************************************************************/

	jQuery(document).ready(function($){


		/*************************************************************
		TIMELINE ASSIGN RIGHT LEFT
		*************************************************************/
				if ($('.timeline').size() > 0) {
				
					// init
					setTimeout(function(){
						
						var newContainerHeight = timelineSetSides();
						animateContainerHeight(newContainerHeight);
						fadeInNewLIs();

					}, parseInt(extDataTimeline.loadDelay));


					// on resize
					$( window ).resize(function() {
						timelineSetSides();
					});


				}	


				function timelineSetSides () {
					var $LIs = $('.timeline').find('li.milestone');
					var leftOffset = 0;
					var rightOffset = 0;
					var bottomMargin = 30;
					var timelineContainerTotalPadding = 40;
					var sideArray = [];
					var newContainerHeight = 0;

					//remove classes first
					$LIs.removeClass('tl_left tl_right');

					//make sidearray
					$LIs.each(function(index, el) {

						// console.log("leftOffset: "+leftOffset+ "// rightOffset: "+rightOffset );
						var $this = $(this);
						var thisHeight = $this.height();

						//var title = $this.find('h3').text();
						// console.log(title + " :"+thisHeight);

						if (rightOffset < leftOffset) {
							// assign right
							sideArray.push('tl_right')	
							rightOffset = (rightOffset + thisHeight + bottomMargin);
						} else {
							// assign left
							sideArray.push('tl_left')	
							leftOffset = (leftOffset + thisHeight + bottomMargin);
						}

					});

					//set height of timeline container
					if (rightOffset > leftOffset) {
						newContainerHeight = rightOffset+timelineContainerTotalPadding;
					} else {
						newContainerHeight = leftOffset+timelineContainerTotalPadding;
					}


					//assign classes
					$LIs.each(function(index, el) {
						var $this = $(this);
						$this.addClass(sideArray[index]);
					});

					return newContainerHeight;

				}

				function animateContainerHeight (newContainerHeight) {
					var newHeightAnimationSpeed = 1000;

					$('.timeline').animate({
						height: newContainerHeight,
					}, newHeightAnimationSpeed);

				}

				function fadeInNewLIs() {
					var offset = $('.timeline').attr('data-offset');
					var $newLIs = $('.timeline').find('li.milestone').slice(offset);
					var numLIs = $newLIs.length;
					var delayTime = 250;
					var fadeInTime	= 1000;
					var newHeightAnimationSpeed = 1000;

					$newLIs.each(function(index, el) {
						var $this = $(this);
						//now show
						$this.delay(delayTime*index).fadeIn(fadeInTime);
						// $this.show();
					});

					//final animation and release height to auto
					setTimeout(function(){

						var $timeline = $('.timeline');

						var currentHeight = $timeline.height();
						$('.timeline').css('height','auto');
						var autoHeight = $timeline.height();
						$timeline.height(currentHeight);

						$timeline.animate({
							height: autoHeight,
						}, newHeightAnimationSpeed, function () {
							$timeline.css('height', 'auto');
						});


					}, (numLIs*delayTime));
						
				}
		



		/*************************************************************
		TIMELINE LOAD MORE POSTS WITH AJAX

		ROADMAP:
		We get posts and try to get plus one post this determines if load more button is displayed.

		*************************************************************/


				$('.timeline_load_more').on('click', function () {
					var $timeline = $('.timeline');
					var offset = parseInt($timeline.attr('data-offset'));
					var newOffset = offset + parseInt(extDataTimeline.postsPerPage);
					var morePosts = false;

					$.ajax({
						type: 'post',
						dataType: 'html',
						url: extDataTimeline.ajaxUrl,
						data: {
							action: 'canon_venuex_timeline_load_more',
							offset: offset,
							posts_per_page: parseInt(extDataTimeline.postsPerPage),
							category: extDataTimeline.category,
							order: extDataTimeline.order,
							exclude_string: extDataTimeline.excludeString,
							link_through:  extDataTimeline.linkThrough,
							display_content:  extDataTimeline.displayContent,
							excerpt_length:  parseInt(extDataTimeline.excerptLength),
							nonce: extDataTimeline.nonce,
						},
						beforeSend: function(response) {
						    $('.timeline_load_img').show();
						    $('.timeline_load_more').hide();
						},
						success: function(response) {
							$timeline.append(response);

							//determine if more posts
							var $newLIs = $timeline.find('li.milestone').slice(newOffset);
							var numNewLIs = $newLIs.size();
							if (numNewLIs > parseInt(extDataTimeline.postsPerPage)) {
								$newLIs.last().remove();	
		    					morePosts = true;
							} else {
								$('.timeline_load_more').hide();
								morePosts = false;
							}

							//set new offset
							$timeline.attr('data-offset', newOffset);

							//reinit libraries
							$(".outter-wrapper").fitVids();
							$('.fade').mosaic();
							$("a[href$='.jpg'],a[href$='.png'],a[href$='.gif']").attr('rel','gallery').addClass('fancybox');

							// order timeline after load delay
							setTimeout(function(){
								var newContainerHeight = timelineSetSides();
								animateContainerHeight(newContainerHeight);
								fadeInNewLIs();

								//toggle load more button again
								if (morePosts == true) {
								    $('.timeline_load_img').hide();
								    $('.timeline_load_more').fadeIn();
								} else {
								    $('.timeline_load_img').hide();
								}

							}, parseInt(extDataTimeline.loadDelay));
						},


					}); //end ajax

				});

	});


	
/*************************************************************
AJAX MULTIPOST PAGINATION
*************************************************************/


	jQuery(document).ready(function($){

		if ($('.multi_nav_control').size() > 0) {

			// NEXT CLICK
			$('body').on('click', '.multipost_nav_forward', function(e) {
				e.preventDefault();
				var $this = $(this);
				ajaxLoadPostPage($this);
			});

			// PREV CLICK
			$('body').on('click', '.multipost_nav_back', function(e) {
				e.preventDefault();
				var $this = $(this);
				ajaxLoadPostPage($this);
			});

			// DETECT ARROW KEYPRESS
			document.onkeydown = mb_checkKey;

		}


		// DETECT ARROW KEYPRESS FUNCTION
		function mb_checkKey(e) {
		    e = e || window.event;
			// console.log(e.keyCode);		// remember to put focus on window not on console.

		    if (e.keyCode == '39') {
		        $('.multipost_nav_forward').click();
		    }
		    else if (e.keyCode == '37') {
		        $('.multipost_nav_back').click();
		    }
		}
		

		// AJAX
		function ajaxLoadPostPage($this) {
			var newHeightAnimationSpeed = 2300;
			var loadDelay = 500;
			var fadeInSpeed = 1500;
			var link = $this.closest('a').attr('href');
			var link = link + " #content_container > *";
			var $contentContainer = $('#content_container');

			$contentContainer.load(link, function () {
				var $this = $(this);
				$this.wrapInner('<div/>');
				var $innerDiv = $this.find('div').first();
				var $multiContent = $this.find('.multi_content');
				$multiContent.css('opacity',0);

				setTimeout(function() {
					var newHeight = $innerDiv.height();
					// console.log(newHeight);
					$contentContainer.animate({
						height: newHeight
					});
					$multiContent.animate({
						opacity: 1,
					}, fadeInSpeed);
	
				}, loadDelay);

			});


		}

	});



/*************************************************************
COUNTDOWN
*************************************************************/

	jQuery(document).ready(function($){

		if ($('.countdown').size() > 0) {

			$('.countdown').each(function(index, element) {
				var $this = $(this);

				//get vars
				var labelYears = $this.data('label_years');
				var labelMonths = $this.data('label_months');
				var labelWeeks = $this.data('label_weeks');
				var labelDays = $this.data('label_days');
				var labelHours = $this.data('label_hours');
				var labelMinutes = $this.data('label_minutes');
				var labelSeconds = $this.data('label_seconds');

				var labelYear = $this.data('label_year');
				var labelMonth = $this.data('label_month');
				var labelWeek = $this.data('label_week');
				var labelDay = $this.data('label_day');
				var labelHour = $this.data('label_hour');
				var labelMinute = $this.data('label_minute');
				var labelSecond = $this.data('label_second');

				var labelY = $this.data('label_y');
				var labelM = $this.data('label_m');
				var labelW = $this.data('label_w');
				var labelD = $this.data('label_d');

				var datetimeString = $this.data('datetime_string');
				var gmtOffset = $this.data('gmt_offset');
				var format = $this.data('format');
				var useCompact = $this.data('use_compact');
				var description = $this.data('description');
				var layout = $this.data('layout');


				//handle vars
				if (useCompact == "checked") { var useCompactBool = true; } else { var useCompactBool = false; }
				if (datetimeString == "") { datetimeString = "" }
				layout = (typeof layout != "undefined") ? layout : null;

				// set labels
				var defaultArgs = {
					labels: [labelYears, labelMonths, labelWeeks, labelDays, labelHours, labelMinutes, labelSeconds],
					labels1: [labelYear, labelMonth, labelWeek, labelDay, labelHour, labelMinute, labelSecond],
					compactLabels: [labelY, labelM, labelW, labelD],
					windowhichLabels: null,
					digits: ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'],
					timeSeparator: ':', isRTL: false};
				$.countdown.setDefaults(defaultArgs);

				// set date
				var countdownDatetime = new Date(); 
				// countdownDatetime = new Date(2013, 11, 31, 23, 59, 59, 100);
				countdownDatetime = new Date(datetimeString);

				$this.countdown({
					compactLabels: ['y', 'm', 'w', 'd'],
					until: countdownDatetime,
					timezone: parseInt(gmtOffset),
					format: format,
					compact: useCompactBool,					
					description: description,
					layout: layout,
				}); 
				 
			});

		}



	});

/*************************************************************
PAGEBUILDER TOP HORIZONTAL RULER
*************************************************************/

	jQuery(document).ready(function($){

		if ($('.pb_hr').size() > 0) {

			var firstBlockClass = $('.pb_block').first().attr('class');

			//update this with blocks slugs
			var noHrBlocks = new Array(
				'pb_featured_img',
				'pb_featured_video',
				'pb_supporters',
				'pb_revslider',
				'pb_divider',
				'pb_no_top_hr'		// universal class
			);

			for (var i = 0; i < noHrBlocks.length; i++) {
				if (firstBlockClass.indexOf(noHrBlocks[i]) != -1) {
					$('.pb_hr').remove();
					
				}
			}

		}

	});

/*************************************************************
CLICKABLE BACKGROUND
*************************************************************/

	jQuery(document).ready(function($){

		if (extData.canonOptions['use_boxed_design'] == "checked") {

			var bgLink = extData.canonOptionsAppearance['bg_link'];

			if (typeof bgLink != "undefined") {
				if (bgLink != "") {
						
					$(document).on('click','body', function(event) {
						if (event.target.nodeName == "BODY") {
							window.open(bgLink, "_blank");
						}
					});
					
				}
			}

		}
	});

/*************************************************************
STICKY HEADER
*************************************************************/

	jQuery(document).ready(function($){

		if ($('.canon_sticky').size() > 0) {

			// add stickyness to these elements
			var stickySelectorClass = ".canon_sticky";
			var $stickyElements = $(stickySelectorClass);
			$stickyElements.each(function(index) {
				$(this).wrap('<div class="sticky_placeholder"></div>');	
			});

			// add canon_sticky_shadow class
			if (extData.canonOptionsFrame['use_sticky_shadow'] == "checked") { $('.sticky-header-wrapper .canon_sticky').last().addClass('canon_sticky_shadow'); }
			
			// init vars
			var adjustedOffset = 0;
         	var windowPosition = 0;
         	var placeholderOffset = 0;
	        var $win = $(window);

			updateStickyPlaceholderHeights();				// set height of this subject's placeholder
			$win.on('scroll', stickyScrollEvent);

			// UPDATE PLACEHOLDER HEIGHTS ON WINDOW LOAD (TO PROTECT AGAINST LATE ARRIVAL CONTENT)
			$(window).load(function() {
				updateStickyPlaceholderHeights();
			});

         	// ON RESIZE
			$(window).resize(function () {

				// turn off old scroll event to allow for new
				$(window).off('scroll', stickyScrollEvent);

				updateStickyPlaceholderHeights();
				$win.on('scroll', stickyScrollEvent);	

			}); 


		}

		function updateStickyPlaceholderHeights () {

 			$('.canon_sticky').each(function (index) {
				var $stickySubject = $(this);
				var $stickyPlaceholder = $stickySubject.closest('.sticky_placeholder');
				var stickySubjectHeight = $stickySubject.height();

				//maintain height of placeholder
				$stickyPlaceholder.css({
					"display": "block",
					"height": stickySubjectHeight,
				});
			});


		}

		function stickyScrollEvent () {

 			$('.canon_sticky').each(function (index) {
				var $stickySubject = $(this);
		        var $stickyPlaceholder = $stickySubject.closest('.sticky_placeholder');

	        	var originalOffset = getWPAdminBarOffset();

				var placeholderOffset = $stickyPlaceholder.offset().top;
				var adjustedOffset = getAdjustedOffset($stickySubject);
				var startingZIndex = 999;
				var thisZIndex = startingZIndex - index;
				// console.log(thisZIndex);

	     		windowPosition = $win.scrollTop();

	     		// console.log("windowposition+adjustedoffset: " + (windowPosition+adjustedOffset));
	     		// console.log("placeholderOffset: " + placeholderOffset);

	     		// apply stickyness when scrolling past
	     		if (windowPosition+adjustedOffset > placeholderOffset) {
	     			// console.log("STICKY ON");
					$stickySubject.css({
					    "position": "fixed",
					    "top": adjustedOffset,
					    "z-index": thisZIndex,
					});
					$stickySubject.addClass('canon_stuck');
	     		}

	     		// remove stickyness when scrolling back over
	     		if (windowPosition+adjustedOffset <= placeholderOffset) {
	     			// console.log("STICKY OFF");
					$stickySubject.css({
					    "position": "static",
					    "top": "auto",
					    "z-index": thisZIndex,
					});
					$stickySubject.removeClass('canon_stuck');
	     		}

	     		// if turn off in responsive then remove stickyness from all
				var windowWidth = $(window).width();
				var turnOffWidth = extData.canonOptionsFrame['sticky_turn_off_width'];
				// console.log("window width: " + windowWidth);
				// console.log("turn off width: " + turnOffWidth);
	     		if (windowWidth < turnOffWidth) {
	     			// console.log("STICKY OFF");
					$stickySubject.css({
					    "position": "static",
					    "top": "auto",
					    "z-index": thisZIndex,
					});
					$stickySubject.removeClass('canon_stuck');
	     		}

 			});
		}
		
		function getAdjustedOffset ($stickySubject) {
			var index = $('.sticky_placeholder').index($stickySubject.closest('.sticky_placeholder'));
         	var originalOffset = getWPAdminBarOffset();

			var adjustedOffset = originalOffset;
			var $placeholdersAbove = $('.sticky_placeholder').slice(0, index);
			$placeholdersAbove.each(function (index) {
				var $thisPlaceholder = $(this);
				adjustedOffset = adjustedOffset + $thisPlaceholder.height();	
			});
			return adjustedOffset;
		}

		function getWPAdminBarOffset () {

			var offset = 0;
         	var $wpAdminBar = $('#wpadminbar');
         	if ($wpAdminBar.size() > 0) {
	         	var wpAdminBarPosition = $wpAdminBar.css('position');
	         	var wpAdminBarHeight = $wpAdminBar.height();
	         	offset = (wpAdminBarPosition == "fixed") ? wpAdminBarHeight : 0;
         	}
         	return offset;

		}

	/*************************************************************************
	SCROLL TO ANCHOR

	USE: link #example will scroll to the first element with class "example"
	Can also be linked to other internal pages. So link "http://www.mypage.com/#example" will first load http://www.mypage.com/example and then after a short delay scroll to first element with class "example".
	*************************************************************************/

			jQuery(window).load(function($) {
				$=jQuery;

				var scrollToSelector = '#';
				var onLoadScrollToDelay = 1500;

				// on window load grab the url and scan for # then initiate scroll if this is found
				var href = isolateScrollToHref(document.URL, scrollToSelector);
				if (href !="") { setTimeout(function() { canonScrollToAnchor(href) }, onLoadScrollToDelay); }

				// on click a tag
				$('body').on('click', 'a', function (event) {

					if (typeof $(this).attr('href') == "undefined") { return; } // failsafe against a tags with no href
					var href = isolateScrollToHref($(this).attr('href'), scrollToSelector)
					if (href != "") { canonScrollToAnchor(href); }
						
				});

			});

			function isolateScrollToHref (source, scrollToSelector) {
				if (source.indexOf('#') != -1) {
					var splitArray = source.split(scrollToSelector);
					return splitArray[1];
				} else {
					return "";	
				}
			}

			function canonScrollToAnchor(href){
				var target = $("."+href);

				if (target.size() > 0) {
					var originalOffset = getWPAdminBarOffset();
					var adjustedOffset = originalOffset;

					// first adjust for header stickies
					var $headerStickies = $('.sticky-header-wrapper .canon_sticky');
					$headerStickies.each(function(index) {
						var $thisSticky = $(this);
						adjustedOffset = adjustedOffset + $thisSticky.height();	
					});

					// next adjust for block stickies
					if (target.hasClass('pb_block')) {
						var $blockClosest = target;	
					} else {
						var $blockClosest = target.closest('.pb_block');	
					}
					var $prevStickyBlocks = $blockClosest.prevAll('.sticky_placeholder');
					$prevStickyBlocks.each(function(index) {
						var $thisSticky = $(this);
						adjustedOffset = adjustedOffset + $thisSticky.height();	
					});

					var scrollToOffset = target.offset().top - adjustedOffset;

					$('html,body').animate({scrollTop: scrollToOffset},'slow');
					
				}
			}



	/*************************************************************
	SEARCH BUTTON
	*************************************************************/

			if ($('.toolbar-search-btn').size() > 0) {

				// SEARCH BUTTON CLICK
				$('body').on('click','.toolbar-search-btn', function (event) {
					event.preventDefault();
					var $searchHeaderContainer = $('.search-header-container');
					var status = $searchHeaderContainer.attr('data-status');

					//update status
					if (status == "closed") {
						$searchHeaderContainer.fadeIn();
						$searchHeaderContainer.attr('data-status', 'open');
					} 

					//calculate offset
					var originalOffset = getWPAdminBarOffset();
					var adjustedOffset = originalOffset;

					var $headerStickies = $('.sticky-header-wrapper .canon_sticky');
					$headerStickies.each(function(index) {
						var $thisSticky = $(this);
						adjustedOffset = adjustedOffset + $thisSticky.height();	
					});

					var scrollToOffset = $searchHeaderContainer.offset().top - adjustedOffset;

					//scroll
					$('html,body').animate({
						scrollTop: scrollToOffset
					},'slow',function () {
						$searchHeaderContainer.find('#s').focus();
					});

					// reinit fitText to fix fitText not appearing in search-header issue
					if ($('.search-header-container .fittext').size() > 0) {
						if (typeof fitText == "function") {
							$('.fittext').each(function(index, el) {
								var $this = $(this);
								var ratio = $this.attr('data-ratio');
								fitText($this, ratio);
							});
						}
					}

				});

				// SEARCH CONTROL SEARCH
				$('body').on('click','.search_control_search', function (event) {
					$('.search-header-container #searchform').submit();
				});

				// SEARCH CONTROL CLOSE
				$('body').on('click','.search_control_close', function (event) {
					var $searchHeaderContainer = $('.search-header-container');
					$searchHeaderContainer.fadeOut();
					$searchHeaderContainer.attr('data-status', 'closed');
					// $('html,body').animate({scrollTop: 0},'slow');
				});


			}

	});


/*************************************************************
SEARCH AUTOCOMPLETE
*************************************************************/

	jQuery(document).ready(function($){

		if (typeof extDataAutocomplete != "undefined") {

			var autocompleteArray = extDataAutocomplete.autocompleteArray;
			$( ".search-header-container #s" ).autocomplete({ source: autocompleteArray });
			
		}

	});

/*************************************************************
@FONT-FACE FIX
*************************************************************/

	jQuery(window).load(function($){

		if (extData.canonOptions['fontface_fix'] == 'checked') {

			$ = jQuery;
	 		$('body').hide().show();
				
		}
 		
	});

/*************************************************************
LAZY LOAD ANIMATION
*************************************************************/

	jQuery(document).ready(function($){

		// apply data-scroll-reveal attribute
		if (extData.canonOptionsAppearance['lazy_load_on_pagebuilder_blocks'] == 'checked') { $('.pb_block:not(.pb_block_first.overlay-header)').attr('data-scroll-reveal', ''); }
		if (extData.canonOptionsAppearance['lazy_load_on_widgets'] == 'checked') { $('.widget').attr('data-scroll-reveal', ''); }
		if (extData.canonOptionsAppearance['lazy_load_on_blog'] == 'checked') { 
			$('.canon_blog .post').attr('data-scroll-reveal', ''); 
			$('.canon_archive .post').attr('data-scroll-reveal', ''); 
		}

		// handle exception: tab blocks
		$('.pb_tabs').each(function (index, element) {
			
			var $thisBlock = $(this);
			var $tabs = $thisBlock.find('ul.block-tabs li');

			var numTabs = $tabs.size();
			var $tabBlocks = $thisBlock.nextAll('.pb_block').slice(0, numTabs);
			var activeIndex = $thisBlock.find('ul.block-tabs .active').index();

			$tabBlocks.not($tabBlocks.eq(activeIndex)).removeAttr('data-scroll-reveal');
		});		

		var config = {
            after: extData.canonOptionsAppearance['lazy_load_after'] + 's',
            enter: extData.canonOptionsAppearance['lazy_load_enter'],
            move: extData.canonOptionsAppearance['lazy_load_move'] +'px',
            over: extData.canonOptionsAppearance['lazy_load_over'] +'s',
            easing: 'ease-in-out',
            viewportFactor: parseInt(extData.canonOptionsAppearance['lazy_load_viewport_factor']),
            reset: false,
            init: true
		};

    	window.scrollReveal = new scrollReveal( config );

	});

/*************************************************************
MENU ANIMATION
*************************************************************/

	jQuery(document).ready(function($){

		if (extData.canonOptionsAppearance['anim_menus'] != 'anim_menus_off') {

			$(extData.canonOptionsAppearance['anim_menus']).each(function(index) {
				var $this_menu = $(this);
				$($this_menu.children('li').get().reverse()).each(function(index) {
					var $this_li = $(this);
					$this_li.delay(index*parseInt(extData.canonOptionsAppearance['anim_menus_delay'])).animate({
						'opacity': '1',
						'top': '0px',
						'right': '0px',
						'bottom': '0px',
						'left': '0px',
					}, parseInt(extData.canonOptionsAppearance['anim_menus_duration']));
					
				});
			});

		}

	});



/*************************************************************
TABLEPRESS TIMETABLE MOD: DETECT TIMETABLE SLOT STATUS
*************************************************************/

	jQuery(document).ready(function($){

		// params
		var timetableClass = "timetable";
		var occupiedClass = "occupied";
		var emptyClass = "empty";

		var selectorString = 'table.tablepress.' + timetableClass;

		// bouncer
		if ($(selectorString).size() == 0) { return; }

		// for each tablepress timetable do
		$(selectorString).each(function (index) {
			var $this = $(this);

			// for each td do
			$this.find('td').each(function (index) {
				
				var $this = $(this);

				// return if this is a column-1 td
				if ($this.hasClass('column-1')) { return; }

				if ($this.text() != "") {
					$this.addClass(occupiedClass);
				} else {
					$this.addClass(emptyClass);
				}

			});
		});

	});


/*************************************************************
SET NEGATIVE MARGIN FOR OVERLAY-HEADER CLASS + APPLY IS-OVERLAID-HEADER CLASS
*************************************************************/

	jQuery(document).ready(function($){

		// bouncer
		if ($('.overlay-header').size() == 0 || $('.dev-cancel-overlay-header').size() > 0) { return; }

		// set vars
		var $stickyHeaderWrapper = $('.sticky-header-wrapper');
		var headerHeight = 0;
		if ($('.pb_block_first').size() > 0) {
			var $topOverlayHeaderObject = $('.overlay-header.pb_block_first');	// if this is a pagebuilder page
		} else {	
			var $topOverlayHeaderObject = $('.overlay-header');	// if this is not a pagebuilder page (e.g. single post, page etc)
		}

		// init
		updateOverlayHeaderMargin();

		// on window resize
		$(window).resize(function () { updateOverlayHeaderMargin(); }); 

		function updateOverlayHeaderMargin () {
			var windowWidth = $(window).width();
			var turnOffWidth = extData.canonOptions['overlay_header_turn_off_width'];

	     	if (windowWidth < turnOffWidth) {
				$topOverlayHeaderObject.css('margin-top', 0);
				 $stickyHeaderWrapper.removeClass('is-overlaid-header');
			} else {
				headerHeight = $stickyHeaderWrapper.height() * -1;
				$topOverlayHeaderObject.css('margin-top', headerHeight);
				if ($topOverlayHeaderObject.size() === 1) { $stickyHeaderWrapper.addClass('is-overlaid-header'); }
			}
		}

	});



/*************************************************************
SCROLL TO MENU HIGHLIGHTING
*************************************************************/


	jQuery(document).ready(function($){

		var statusOfScrollToMenuItems = (typeof extData.canonOptionsFrame.status_of_scroll_to_menu_items != 'undefined') ? extData.canonOptionsFrame.status_of_scroll_to_menu_items : 'auto';

		if (statusOfScrollToMenuItems == 'normal') { return; }

		currentMenuItemClassReset();

		if (statusOfScrollToMenuItems == 'disable') { return; }

		markScrollToTargets();

		$(window).scroll(function(){
			currentMenuItemClassReset();
			checkScrollToTargetPosition();
		});


		function currentMenuItemClassReset() {
				
			var scrollToSelector = '#';
			var $aTags = $('nav a');	// get all level anchor tags

			// remove current-menu-item class from all anchor tags that contain the scrollToSelector
			$aTags.each(function (index) {
				var $this = $(this);
				var href= $this.attr('href');
				if (typeof href != "undefined") {	// failsafe against anchor links with no href
					if (href.indexOf(scrollToSelector) != -1) {
						var splitArray = href.split(scrollToSelector);
						href = splitArray[1];
						if (href != "") {
							$this.closest('li').removeClass('current-menu-item');
						}
					}
				}
			});

			// now that current-menu-item class has been removed from all scroll to menu items search for current-menu-parent and current-menu-ancestor then check if they have a child with current-menu-item class. If they do they are regular parents/ancestors and should remain otherwise they must belong to a scroll to link and must be removed.
			var $ancestors = $('nav .current-menu-parent, nav .current-menu-ancestor');

			$ancestors.each(function (index) {
				var $this = $(this);

				if ($this.find('.current-menu-item').length === 0) {
					$this.removeClass('current-menu-parent').removeClass('current-menu-ancestor');	
				}	
			});

		}

		function markScrollToTargets () {
				
			var scrollToSelector = '#';
			var $aTags = $('nav a');	// get all level anchor tags
			var url = window.location.href;
			var urlSplitArray = url.split(scrollToSelector);
			url = urlSplitArray[0];
			var $target;

			$aTags.each(function (index) {
				var $this = $(this);
				var splitArray = $this.attr('href').split(scrollToSelector);
				var thisURL = splitArray[0];
				var thisTargetClass = splitArray[1];

				if (typeof thisTargetClass != "undefined") {	// check if link has a scrollto selector
					if (thisTargetClass != "") {	// check if link has scrollto selector but no actual target class (like just #-link)
						if (thisURL != "") {	// if link has url before scrollto selector
							if (thisURL == url)	{ 	// only consider if the link url is the same as current url
								$("." + thisTargetClass).addClass('scroll-to-target').addClass('scroll-to-target-class-' + thisTargetClass);
							}
						} else {
							$("." + thisTargetClass).addClass('scroll-to-target').addClass('scroll-to-target-class-' + thisTargetClass);
						}
					}
				}
	
			});
		}

		function checkScrollToTargetPosition() {
			var $scrollToTargets = $('.scroll-to-target');
			var scrollTop = $(window).scrollTop();

			var targetClassSplitArray1;
			var targetClassSplitArray2;
			var targetClass;

			var topOffsetAdjustment = 30; // distance before element to trigger highlight
			var bottomOffsetAdjustment = 10; // distance before element (height) ends to disable highlight
			var topOffset;
			var bottomOffset;

			// adjust scrollTop for stickyheader and wpadminbar
			var stickyHeaderWrapperHeight = $('.sticky-header-wrapper').outerHeight();
			var wpAdminBarHeight = $('#wpadminbar').outerHeight();
			scrollTop = scrollTop + stickyHeaderWrapperHeight + wpAdminBarHeight;

			$scrollToTargets.each(function(index) {
				var $this = $(this);
				var itemOffset = $this.offset().top;

				// console.log(scrollTop);
				// console.log(itemOffset);
				// console.log("*");

				// calculate bottomOffsetAdjustment from the height of the target element
				var thisHeight = $this.outerHeight();

				// calculate offsets
				topOffset = itemOffset-topOffsetAdjustment;
				bottomOffset = itemOffset+thisHeight-bottomOffsetAdjustment;

			    // add current-menu-item
			    if( (scrollTop >= topOffset) && (scrollTop < bottomOffset) ) {
			    	targetClassSplitArray1 = $this.attr('class').split('scroll-to-target-class-');
			    	if (typeof targetClassSplitArray1[1] != "undefined") {
			    		targetClassSplitArray2 = targetClassSplitArray1[1].split(' ');
			    		targetClass = targetClassSplitArray2[0];
			    		currentMenuItemClassAdd(targetClass);
			    	}
				}

			});


		}

		function currentMenuItemClassAdd(targetClass) {
			var scrollToSelector = '#';
			var $aTags = $('nav a');	// get all level anchor tags
			var url = window.location.href;
			var urlSplitArray = url.split(scrollToSelector);
			url = urlSplitArray[0];

			$aTags.each(function(index){
				var $this = $(this);
				var href= $this.attr('href');
				if (typeof href != "undefined") {	// failsafe against anchor links with no href
					if (href.indexOf(scrollToSelector + targetClass) != -1) {
						var splitArray = $this.attr('href').split(scrollToSelector);
						var thisURL = splitArray[0];
						var thisTargetClass = splitArray[1];

						if (thisURL != "") {	// if link has url before scrollto selector
							if (thisURL == url)	{ 	// only consider if the link url is the same as current url
								$this.closest('li').addClass('current-menu-item');
							}
						} else {
							$this.closest('li').addClass('current-menu-item');
						}
					}
				}
					
			});

			currentMenuItemAddAncestors();
		}

		function currentMenuItemAddAncestors() {
			var $currentMenuItems = $('nav .current-menu-item');
			var currentMenuID = $currentMenuItems.closest('nav').attr('id');

			$currentMenuItems.each(function(index) {
				var $this = $(this);

				// first add current-menu-parent
				var $parent = $this.parents('li').first();
				if ($parent.length > 0) {
					var parentMenuID = $parent.closest('nav').attr('id');
					if (typeof parentMenuID != "undefined") {
						if (currentMenuID == parentMenuID) {
							$parent.addClass('current-menu-parent');	
						}
					}
				}

				// then add current-menu-ancestor
				var $ancestors = $this.parents('li');
				$ancestors.each(function(index) {
					var $thisAncestor = $(this);
					var ancestorMenuID = $thisAncestor.closest('nav').attr('id');
					if (typeof ancestorMenuID != "undefined") {
						if (currentMenuID == ancestorMenuID) {
							$thisAncestor.addClass('current-menu-ancestor');	
						}
					}
				});

			});
		}

	});


/*************************************************************
POLLS
*************************************************************/

	jQuery(document).ready(function($){

		// bouncer
		if ($('.poll').size() == 0) { return; }

		// init
		updatePollResultBars();

		// POLL-GO-RESULT-BTN
		$('body').on('click', '.poll-go-result-btn', function(event) {
			event.preventDefault();
			var $this = $(this);
			var $thisPollVoteContainer = $this.closest('.poll').find('.poll-vote-container');
			var $thisPollResultContainer = $this.closest('.poll').find('.poll-result-container');
			$thisPollVoteContainer.hide();
			$thisPollResultContainer.show();
		})

		// POLL-GO-VOTE-BTN
		$('body').on('click', '.poll-go-vote-btn', function(event) {
			event.preventDefault();
			var $this = $(this);
			var $thisPollVoteContainer = $this.closest('.poll').find('.poll-vote-container');
			var $thisPollResultContainer = $this.closest('.poll').find('.poll-result-container');
			$thisPollVoteContainer.show();
			$thisPollResultContainer.hide();
		})

		// VOTE BTN
		$('body').on('click', '.poll-vote-btn', function(event) {
			event.preventDefault();
			var $this = $(this);
			var $thisPoll = $this.closest('.poll');
			var $thisPollVoteContainer = $thisPoll.find('.poll-vote-container');
			var $thisPollResultContainer = $thisPoll.find('.poll-result-container');
			var $thisRadioButtons = $thisPoll.find('.poll-answers input');
			var selectedRadioButtonIndex = "-1";

			// get radio button value
			$thisRadioButtons.each(function(index) {
				var $thisRadioButton = $(this);
				if ($thisRadioButton.is(':checked')) { selectedRadioButtonIndex = $thisRadioButton.attr('value'); }	
			});

			// bounce if no radio button is selected
			if (selectedRadioButtonIndex == "-1") { return;	}

			// get vars
			var id = $thisPoll.attr('data-id');
			var numVotes = $thisPoll.find('.poll-num-votes span').text();
			var nonce = $thisPoll.attr('data-nonce');

			$.ajax({
				type: 'post',
				url: extData.ajaxUrl,
				data: {
					action: 'vote_poll',
					id: id,
					selected_radio_button_index: selectedRadioButtonIndex,
					num_votes: numVotes,
					nonce: nonce
				},
				success: function(response) {
					if (response == "success") {

						// update total num votes
						numVotes++;
						var numVotesText = (numVotes === 1) ? "vote" : "votes";
						$thisPoll.find('.poll-num-votes').html("<span>" + numVotes + "</span> " + numVotesText);

						// update answer vote
						var $resultBars = $thisPoll.find('.result-bar-container .result-bar');
						var $votedForAnswer = $resultBars.eq(selectedRadioButtonIndex);
						var answerNumVotes = $votedForAnswer.attr('data-votes');
						answerNumVotes++;
						$votedForAnswer.attr('data-votes', answerNumVotes);


						// recalculate percentages and update
						$resultBars.each(function(index) {
							var $thisResultBar = $(this);
							var thisResultBarNumVotes = parseInt($thisResultBar.attr('data-votes'));
							if (numVotes > 0) {
								var thisResultBarPercent = Math.round((thisResultBarNumVotes/numVotes)*100);
								$thisResultBar.attr('data-percent', thisResultBarPercent);
								$thisResultBar.closest('li').find('.poll-result-text span').text(thisResultBarPercent + "%");
							}
						});

						// update bars
						updatePollResultBars();

						// hide  go to vote link
						$thisPoll.find('.poll-result-container .poll-result-footer .poll-go-vote-btn').hide();

						// change not-voted class to voted
						$thisPoll.removeClass('not-voted').addClass('voted');

						// show result hide vote
						$thisPollVoteContainer.hide();
						$thisPollResultContainer.show();

					}
				}
			}); //end ajax

		})


		function updatePollResultBars() {

			var $resultBars = $('.poll .result-bar-container .result-bar');
			$resultBars.each(function(index) {
				var $this = $(this);
				var percent = $this.attr('data-percent');
				$this.css('width', percent + "%");
			});

		}


	});


