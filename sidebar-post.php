<?php 

	$canon_options = get_option('canon_options');

	 //SETTTINGS
    $cmb_sidebar_id = get_post_meta($post->ID, 'cmb_sidebar_id', true);

    // FAILSAFE DEFAULT
    if (empty($cmb_sidebar_id)) { $cmb_sidebar_id = "canon_archive_sidebar_widget_area"; }

	$aside_class = ($canon_options['sidebars_alignment'] == 'left') ? 'left-aside fourth' : 'right-aside fourth last';

	$sidebar_name = $GLOBALS['wp_registered_sidebars'][$cmb_sidebar_id]['name'];

?>

				 <!-- Start Main Sidebar -->
				<aside class="<?php echo esc_attr($aside_class); ?>">

					<?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar($cmb_sidebar_id)) : ?>  
						
						<h4><?php echo esc_attr($sidebar_name); ?></h4>
                        <p><i><?php esc_html_e("Please login and add some widgets to this widget area.", "loc_canon_venuex"); ?></i></p> 
					
			        <?php endif; ?>  

				</aside>
				 <!-- Finish Sidebar -->