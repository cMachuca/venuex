<?php 

    // GET OPTIONS
    $canon_options_frame = get_option('canon_options_frame'); 
    
    $layout_array = explode("_", $canon_options_frame['widgetized_footer_layout']);

?>

            <!-- Start Widget Footer -->
        	<div class="outter-wrapper main-footer widget-footer main">

        		<div class="wrapper clearfix">
        			
                    <?php 

                        for ($i = 0; $i < count($layout_array); $i++) { 

                            $container_class = ($i == count($layout_array)-1) ? $layout_array[$i] . " last" : $layout_array[$i];

                            $this_sidebar_slug = $canon_options_frame['widget_footer_widget_area_' . ($i+1)];
                            $this_sidebar_name = $GLOBALS['wp_registered_sidebars'][$this_sidebar_slug]['name'];
                        ?>

                            <!-- FOOTER: WIDGET AREA -->
                            <div class="<?php echo esc_attr($container_class); ?>">

                                <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar($this_sidebar_slug) ) : ?>  
                                    
                                    <h4><?php echo esc_attr($this_sidebar_name); ?></h4>
                                    <p><i><?php esc_html_e("Please login and add some widgets to this widget area.", "loc_canon_venuex"); ?></i></p> 
                                
                                <?php endif; ?>  

                            </div>

                        <?php
                        }

                    ?>


        		</div>
        	</div>
        	<!-- End Widget Footer -->		

