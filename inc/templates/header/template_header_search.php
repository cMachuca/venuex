<?php 
	
	$canon_options = get_option('canon_options'); 
	$canon_options_post = get_option('canon_options_post'); 

	$autocomplete_string = $canon_options['autocomplete_words'];

	$autocomplete_explode_array = explode(",", $autocomplete_string);
	$autocomplete_array = array();

	foreach ($autocomplete_explode_array as $key => $value) {
		$value = trim($value);
		if (!empty($value)) { array_push($autocomplete_array, $value); }
	}

	wp_localize_script('canon-scripts','extDataAutocomplete', array(
		'autocompleteArray'			=> $autocomplete_array, 
	));        

    $layout_array = explode("_", $canon_options_post['widgetized_search_layout']);

?>


	<!-- SEARCH BOX -->

	    <!-- Start Outter Wrapper -->
	    <div class="outter-wrapper search-header-container" data-status="closed">
	        <!-- Start Main Navigation -->
	        <div class="wrapper">
	            <header class="clearfix">

	            	<div class="search-area">

		                <ul class="search_controls">
		                	<li class="search_control_search"><em class="fa fa-search"></em></li>
		                	<li class="search_control_close"><em class="fa fa-times"></em></li>
		                </ul>

		                <form role="search" method="get" id="searchform" action="<?php echo esc_url(home_url('/')); ?>">
		                    <input type="text" id="s" class="full" name="s" placeholder="<?php echo esc_attr($canon_options_post['search_box_text']); ?>" />
	                		<?php if (isset($_GET['lang'])) { printf("<input type='hidden' name='lang' value='%s' />", esc_attr($_GET['lang'])); } ?>
		                </form>

	            	</div>

	            	<div class="widgets-area">

                    <?php 

                        for ($i = 0; $i < count($layout_array); $i++) { 

                            $container_class = ($i == count($layout_array)-1) ? $layout_array[$i] . " last" : $layout_array[$i];

                            $this_sidebar_slug = $canon_options_post['search_widget_area_' . ($i+1)];
                            $this_sidebar_name = $GLOBALS['wp_registered_sidebars'][$this_sidebar_slug]['name'];
                        ?>

                            <!-- FOOTER: WIDGET AREA -->
                            <div class="<?php echo esc_attr($container_class); ?>">

                                <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar($this_sidebar_slug) ) : ?>  
                                    
                                    <h4><?php echo esc_attr($this_sidebar_name); ?></h4>
                                    <p><i><?php esc_html_e("Please login and add some widgets to this widget area.", "loc_canon_venuex"); ?></i></p> 
                                
                                <?php endif; ?>  

                            </div>

                        <?php
                        }

                    ?>

	            	</div>


	            </header>
	        </div>
	        <!-- End Main Navigation -->
	    </div>
	    <!-- End Outter Wrapper -->		        