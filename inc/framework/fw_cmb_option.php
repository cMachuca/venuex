<?php

/////////////////////////////////

// INDEX
//
// CHECKBOX
// CHECKBOX MULTIPLE
// TEXT
// SELECT
// SELECT SIDEBAR
// SELECT REVSLIDER
// TEXTAREA
// NUMBER

/////////////////////////////////





	function fw_cmb_option ($params) {

		extract($params);

		// general vars
		$id = $slug;
		$name = $slug;

		// check if slug is array
		if (strpos($slug, '[') !== FALSE) {
			$slug_array = explode('[', $slug);
			$option_array = get_post_meta($post_id, $slug_array[0], true);

			for ($i = 1; $i < count($slug_array); $i++) {  
				$slug_array[$i] = substr($slug_array[$i], 0, strlen($slug_array[$i])-1);
			}

			// 1 dimensional array
			if (count($slug_array) === 2) {
				$option = $option_array[$slug_array[1]];
			}

		} else {

			$option = get_post_meta($post_id, $slug, true);

		}

		// GET ARRAY OF REGISTERED SIDEBARS
		$registered_sidebars_array = array();
		foreach ($GLOBALS['wp_registered_sidebars'] as $key => $value) { array_push($registered_sidebars_array, $value); }

        // GET ARRAY OF SLIDERS
        if (class_exists('RevSlider')) {
	        $slider = new RevSlider();
	        $slider_aliases_array = $slider->getAllSliderAliases();
	        if (empty($slider_aliases_array)) { $slider_aliases_array = array('No sliders found!'); }
        } else {
        	$slider_aliases_array = array('Revolution Slider plugin not found!');	
        }


// CHECKBOX
//
// Usage:
//
// fw_cmb_option(array(
// 	'type'					=> 'checkbox',
// 	'title' 				=> esc_html__('Display quote as a tweet', 'loc_venuex_core_plugin'),
// 	'slug' 					=> 'cmb_quote_is_tweet',
// 	'post_id'				=> $post->ID,
// )); 


		if ($type == "checkbox") {
			?>

			<!-- FW CMB OPTION: CHECKBOX-->

				<div class="option_item">
					<input type="hidden" name="<?php echo esc_attr($name); ?>" value="unchecked" />
					<input type='checkbox' id='<?php echo esc_attr($id); ?>' name='<?php echo esc_attr($name); ?>' value='checked' <?php if(!empty($option)) { checked($option == "checked"); } ?>>
					<label for='<?php echo esc_attr($id); ?>'><?php echo wp_kses_post($title); ?></label><br>
				</div>

			<?php

			return true;		
				
		}


// CHECKBOX MULTIPLE
//
// Usage:
//
// fw_cmb_option(array(
// 	'type'					=> 'checkbox_multiple',
// 	'title' 				=> esc_html__('Display quote as a tweet', 'loc_venuex_core_plugin'),
// 	'slug' 					=> 'cmb_quote_is_tweet',
// 	'checkboxes'			=> array(
// 		'cmb_hide_from_archive'		=> esc_html__('Hide from blog', 'loc_venuex_core_plugin'),
// 		'cmb_hide_from_gallery'		=> esc_html__('Hide from gallery', 'loc_venuex_core_plugin'),
// 		'cmb_hide_from_popular'		=> esc_html__('Hide from popular lists', 'loc_venuex_core_plugin'),
// 	),
// 	'post_id'				=> $post->ID,
// )); 


		if ($type == "checkbox_multiple") {
			?>

			<!-- FW CMB OPTION: CHECKBOX-->

				<div class="option_item">

					<?php
						
						foreach($checkboxes as $key => $value) {
							$this_option = get_post_meta($post_id, $key, true);

						?>
							<input type="hidden" name="<?php echo esc_attr($key); ?>" value="unchecked" />
							<input type='checkbox' id='<?php echo esc_attr($key); ?>' name='<?php echo esc_attr($key); ?>' value='checked' <?php if(!empty($this_option)) { checked($this_option == "checked"); } ?>>
							<label for='<?php echo esc_attr($key); ?>'><?php echo wp_kses_post($value); ?></label><br>
						<?php	
						}
					
					?>
				</div>

			<?php

			return true;		
				
		}


// TEXT
//
// Usage:
//
// fw_cmb_option(array(
// 	'type'					=> 'text',
// 	'title' 				=> esc_html__('Use text as logo', 'loc_canon_venuex'),
// 	'slug' 					=> 'logo_text',
// 	'class'					=> 'widefat',
// 	'post_id'				=> $post->ID,
// )); 


		if ($type == "text") {

			// specific vars
			$default_class = "";	
			$final_class = (isset($class)) ? $class : $default_class;
			?>

			<!-- FW CMB OPTION: TEXT-->

				<div class="option_item">
					<label for='<?php echo esc_attr($id); ?>'><?php echo wp_kses_post($title); ?></label><br>
					<input type='text' id='<?php echo esc_attr($id); ?>' name='<?php echo esc_attr($name); ?>' class='<?php echo esc_attr($final_class); ?>' value="<?php if (!empty($option)) { echo htmlspecialchars($option); } ?>">
				</div>

			<?php

			return true;		
				
		}



// SELECT
//
// Usage:
//
// fw_cmb_option(array(
// 	'type'					=> 'select',
// 	'title' 				=> esc_html__('Post style', 'loc_venuex_core_plugin'),
// 	'slug' 					=> 'cmb_single_style',
// 	'select_options'		=> array(
// 		'full'				=> esc_html__('Featured full width (standard)', 'loc_canon_venuex'),
// 		'boxed'				=> esc_html__('Featured boxed', 'loc_canon_venuex'),
// 		'compact'			=> esc_html__('Featured compact', 'loc_canon_venuex'),
// 		'project'			=> esc_html__('Project post', 'loc_canon_venuex'),
// 		'multi'				=> esc_html__('Multi post', 'loc_canon_venuex'),
// 	),
// 	'post_id'				=> $post->ID,
// )); 




		if ($type == "select") {

			?>

			<!-- FW CMB OPTION: SELECT-->

				<div class="option_item">

					<label for='<?php echo esc_attr($id); ?>'><?php echo wp_kses_post($title); ?></label><br>
					<select id="<?php echo esc_attr($id); ?>" name="<?php echo esc_attr($name); ?>"> 
						<?php 

							foreach ($select_options as $key => $value) {
							?>
		     					<option value="<?php echo esc_attr($key); ?>" <?php if (!empty($option)) {if ($option == $key) echo "selected='selected'";} ?>><?php echo esc_attr($value); ?></option> 
							<?php		
							}

						?>
					</select> 

				</div>


			<?php

			return true;		
				
		}


// SELECT SIDEBAR
//
// Usage:
//
// fw_cmb_option(array(
// 	'type'					=> 'select_sidebar',
// 	'title' 				=> esc_html__('Select your sidebar', 'loc_venuex_core_plugin'),
// 	'slug' 					=> 'cmb_sidebar',
// 	'select_options'		=> array(
// 		'full'				=> esc_html__('Featured full width (standard)', 'loc_canon_venuex'),
// 		'boxed'				=> esc_html__('Featured boxed', 'loc_canon_venuex'),
// 		'compact'			=> esc_html__('Featured compact', 'loc_canon_venuex'),
// 		'project'			=> esc_html__('Project post', 'loc_canon_venuex'),
// 		'multi'				=> esc_html__('Multi post', 'loc_canon_venuex'),
// 	),
// 	'post_id'				=> $post->ID,
// )); 




		if ($type == "select_sidebar") {

			?>

			<!-- FW CMB OPTION: SELECT SIDEBAR-->

				<div class="option_item">

					<label for='<?php echo esc_attr($id); ?>'><?php echo wp_kses_post($title); ?></label><br>
					<select id="<?php echo esc_attr($id); ?>" name="<?php echo esc_attr($name); ?>"> 
						<?php 

							if (isset($select_options)) { foreach ($select_options as $key => $value) {
							?>
		     					<option value="<?php echo esc_attr($key); ?>" <?php if (isset($option)) {if ($option == $key) echo "selected='selected'";} ?>><?php echo esc_attr($value); ?></option> 
							<?php		
							}}

							foreach ($registered_sidebars_array as $key => $value) {
							?>
		     					<option value="<?php echo esc_attr($value['id']); ?>" <?php if (!empty($option)) {if ($option == $value['id']) echo "selected='selected'";} ?>><?php echo esc_attr($value['name']); ?></option> 
							<?php		
							}

						?>
					</select> 

				</div>


			<?php

			return true;		
				
		}

// SELECT REVSLIDER
//
// Usage:
//
// fw_cmb_option(array(
// 	'type'					=> 'select_revslider',
// 	'title' 				=> esc_html__('Select your sidebar', 'loc_venuex_core_plugin'),
// 	'slug' 					=> 'cmb_sidebar',
// 	'select_options'		=> array(
// 		'full'				=> esc_html__('Featured full width (standard)', 'loc_canon_venuex'),
// 		'boxed'				=> esc_html__('Featured boxed', 'loc_canon_venuex'),
// 		'compact'			=> esc_html__('Featured compact', 'loc_canon_venuex'),
// 		'project'			=> esc_html__('Project post', 'loc_canon_venuex'),
// 		'multi'				=> esc_html__('Multi post', 'loc_canon_venuex'),
// 	),
// 	'post_id'				=> $post->ID,
// )); 




		if ($type == "select_revslider") {

			?>

			<!-- FW CMB OPTION: SELECT SIDEBAR-->

				<div class="option_item">

					<label for='<?php echo esc_attr($id); ?>'><?php echo wp_kses_post($title); ?></label><br>
					<select id="<?php echo esc_attr($id); ?>" name="<?php echo esc_attr($name); ?>"> 
						<?php 

							if (isset($select_options)) { foreach ($select_options as $key => $value) {
							?>
		     					<option value="<?php echo esc_attr($key); ?>" <?php if (isset($option)) {if ($option == $key) echo "selected='selected'";} ?>><?php echo esc_attr($value); ?></option> 
							<?php		
							}}

							foreach ($slider_aliases_array as $key => $value) {
							?>
		     					<option value="<?php echo esc_attr($value); ?>" <?php if (!empty($option)) {if ($option == $value) echo "selected='selected'";} ?>><?php echo esc_attr($value); ?></option> 
							<?php		
							}

						?>
					</select> 

				</div>


			<?php

			return true;		
				
		}



// TEXTAREA
//
// Usage:
//
// fw_cmb_option(array(
// 	'type'					=> 'textarea',
// 	'title' 				=> esc_html__('Footer text', 'loc_canon_venuex'),
// 	'slug' 					=> 'footer_text',
// 	'cols'					=> '100',
// 	'rows'					=> '5',
//	'hint'					=> esc_html__('Optional. HTML allowed.', 'loc_canon_venuex'), ,
// 	'class'					=> 'widefat',
// 	'post_id'				=> $post->ID,
// )); 



		if ($type == "textarea") {

			// specific vars
			$default_class = "";	
			$final_class = (isset($class)) ? $class : $default_class;
			?>

			<!-- FW CMB OPTION: TEXTAREA-->

				<div class="option_item clearfix">
					<label for='<?php echo esc_attr($id); ?>'><?php echo wp_kses_post($title); ?></label><br>
					<textarea 
						id='<?php echo esc_attr($id); ?>' 
						name='<?php echo esc_attr($name); ?>' 
						class="<?php echo esc_attr($final_class); ?>" 
						<?php if (isset($cols)) { echo "cols=" . $cols; } ?>
						<?php if (isset($rows)) { echo "rows=" . $rows; } ?>
					><?php if (!empty($option)) echo esc_attr($option); ?></textarea>
					<?php if (isset($hint)) { printf('<span class="item_hint float_right">%s</span>', esc_attr($hint)); } ?>
				</div>

			<?php

			return true;		
				
		}


// NUMBER
//
// Usage:
//
// fw_cmb_option(array(
// 	'type'					=> 'number',
// 	'title' 				=> esc_html__('Posts per page', 'loc_canon_venuex'),
// 	'slug' 					=> 'cmb_timeline_posts_per_page',
// 	'min'					=> '1',										// optional
// 	'max'					=> '10000',									// optional
// 	'step'					=> '1',										// optional
// 	'width_px'				=> '60',									// optional
// 	'post_id'				=> $post->ID,
// )); 


		if ($type == "number") {

			// specific vars
			if (isset($width_px)) { $style_width = 'width:' . $width_px . 'px;'; };
			?>

			<!-- FW OPTION: NUMBER-->

				<div class="option_item">

					<input 
						type='number' 
						id= <?php echo esc_attr($id); ?>
						name= <?php echo esc_attr($name); ?>
						<?php if (isset($min)) { echo "min=" . $min; } ?>
						<?php if (isset($max)) { echo "max=" . $max; } ?>
						<?php if (isset($step)) { echo "step=" . $step; } ?>
						<?php if (isset($width_px)) { echo "style=" . $style_width; } ?>
						value='<?php if (isset($option)) echo esc_attr($option); ?>'
					>
					<?php echo wp_kses_post($title); ?>

				</div>

			<?php

			return true;		
				
		}













		return false;

	} // end function fw_cmb_option



