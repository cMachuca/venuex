	<div class="wrap">

		<div id="icon-themes" class="icon32"></div>

		<h2><?php printf( "%s %s - %s", esc_attr(wp_get_theme()->Name), esc_html__("Settings", "loc_canon_venuex"), esc_html__("Posts & Pages", "loc_canon_venuex") ); ?></h2>

		<?php 
			//delete_option('canon_options_post');

			// GET VARS
			$canon_options_post = get_option('canon_options_post'); 
			$canon_theme_name = wp_get_theme()->Name;

			// GET ARRAY OF REGISTERED SIDEBARS
			$registered_sidebars_array = array();
			foreach ($GLOBALS['wp_registered_sidebars'] as $key => $value) { array_push($registered_sidebars_array, $value); }

			// var_dump($canon_options_post);
		?>

		<br>
		
		<div class="options_wrapper canon-options">
		
			<div class="table_container">

				<form method="post" action="options.php" enctype="multipart/form-data">
					<?php settings_fields('group_canon_options_post'); ?>				<!-- very important to add these two functions as they mediate what wordpress generates automatically from the functions.php -->
					<?php do_settings_sections('handle_canon_options_post'); ?>		


					<?php submit_button(); ?>


					<!-- 

						INDEX

						SINGLE PAGE
						SINGLE POST
						SINGLE PERSON POST
						META INFO
						BLOG
						SEARCH 
						404
						LISTINGS
						REVOLUTION SLIDER
						WOOCOMMERCE
						BUDDYPRESS
						BBPRESS
						THE EVENTS CALENDAR BY TRIBE
					
					-->


					<!-- 
					--------------------------------------------------------------------------
						SINGLE PAGE
				    -------------------------------------------------------------------------- 
					-->

						<h3><?php esc_html_e("Single Page", "loc_canon_venuex"); ?> <img src="<?php echo get_template_directory_uri() . '/img/help.png' ?>"></h3>

						<div class='contextual-help'>
							<?php 

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Show comments', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Displays comments and comment reply form.', 'loc_canon_venuex'),
									),
								)); 

							 ?>		

						</div>

						<table class='form-table'>

							<?php 

								fw_option(array(
									'type'					=> 'checkbox',
									'title' 				=> esc_html__('Show comments', 'loc_canon_venuex'),
									'slug' 					=> 'page_show_comments',
									'options_name'			=> 'canon_options_post',
								)); 

							 ?>	

						</table>


					<!-- 
					--------------------------------------------------------------------------
						SINGLE POST
				    -------------------------------------------------------------------------- 
					-->

						<h3><?php esc_html_e("Single Post", "loc_canon_venuex"); ?> <img src="<?php echo get_template_directory_uri() . '/img/help.png' ?>"></h3>

						<div class='contextual-help'>
							<?php 

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Show tags', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Display tags associated with your post.', 'loc_canon_venuex'),
									),
								)); 

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Show comments', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Displays comments and comment reply form.', 'loc_canon_venuex'),
									),
								)); 

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Show post navigation', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Adds post navigation to posts. Use this to navigate between previous and next post relative to the current post.', 'loc_canon_venuex'),
									),
								)); 

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Post navigate only same category posts', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('The prev/next post navigation only navigates posts from the same category as the current post.', 'loc_canon_venuex'),
									),
								)); 

							 ?>		

						</div>

						<table class='form-table'>

							<?php 

								fw_option(array(
									'type'					=> 'checkbox',
									'title' 				=> esc_html__('Show tags', 'loc_canon_venuex'),
									'slug' 					=> 'show_tags',
									'options_name'			=> 'canon_options_post',
								)); 

								fw_option(array(
									'type'					=> 'checkbox',
									'title' 				=> esc_html__('Show comments', 'loc_canon_venuex'),
									'slug' 					=> 'show_comments',
									'options_name'			=> 'canon_options_post',
								)); 

								fw_option(array(
									'type'					=> 'checkbox',
									'title' 				=> esc_html__('Show post navigation', 'loc_canon_venuex'),
									'slug' 					=> 'show_post_nav',
									'options_name'			=> 'canon_options_post',
								)); 

								fw_option(array(
									'type'					=> 'checkbox',
									'title' 				=> esc_html__('Post navigate only same category posts', 'loc_canon_venuex'),
									'slug' 					=> 'post_nav_same_cat',
									'options_name'			=> 'canon_options_post',
								)); 

							 ?>	

						</table>

					<!-- 
					--------------------------------------------------------------------------
						SINGLE PERSON POST
				    -------------------------------------------------------------------------- 
					-->

						<h3><?php esc_html_e("Single Person Post", "loc_canon_venuex"); ?> <img src="<?php echo get_template_directory_uri() . '/img/help.png' ?>"></h3>

						<div class='contextual-help'>
							<?php 

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Show title/position', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Show title/position meta info on single person page.', 'loc_canon_venuex'),
									),
								)); 

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Show info', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Show info on single person page.', 'loc_canon_venuex'),
									),
								)); 

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Show post navigation', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Show post navigation on single person page.', 'loc_canon_venuex'),
									),
								)); 

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Post navigate only same category posts', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('The prev/next post navigation only navigates posts from the same people category as the current post.', 'loc_canon_venuex'),
									),
								)); 

							 ?>		

						</div>

						<table class='form-table'>

							<?php 

								fw_option(array(
									'type'					=> 'checkbox',
									'title' 				=> esc_html__('Show title/position', 'loc_canon_venuex'),
									'slug' 					=> 'show_person_position',
									'options_name'			=> 'canon_options_post',
								)); 

								fw_option(array(
									'type'					=> 'checkbox',
									'title' 				=> esc_html__('Show info', 'loc_canon_venuex'),
									'slug' 					=> 'show_person_info',
									'options_name'			=> 'canon_options_post',
								)); 

								fw_option(array(
									'type'					=> 'checkbox',
									'title' 				=> esc_html__('Show post navigation', 'loc_canon_venuex'),
									'slug' 					=> 'show_person_nav',
									'options_name'			=> 'canon_options_post',
								)); 

								fw_option(array(
									'type'					=> 'checkbox',
									'title' 				=> esc_html__('Post navigate only same category posts', 'loc_canon_venuex'),
									'slug' 					=> 'person_nav_same_cat',
									'options_name'			=> 'canon_options_post',
								)); 

							 ?>	

						</table>

					<!-- 
					--------------------------------------------------------------------------
						META INFO
				    -------------------------------------------------------------------------- 
					-->

						<h3><?php esc_html_e("Meta info", "loc_canon_venuex"); ?> <img src="<?php echo get_template_directory_uri() . '/img/help.png' ?>"></h3>

						<div class='contextual-help'>
							<?php 

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Show meta info', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Choose what meta info to display in posts.', 'loc_canon_venuex'),
									),
								)); 

							 ?>		

						</div>

						<table class='form-table'>

							<?php 

								fw_option(array(
									'type'					=> 'checkbox',
									'title' 				=> esc_html__('Show meta info: author', 'loc_canon_venuex'),
									'slug' 					=> 'show_meta_author',
									'options_name'			=> 'canon_options_post',
								)); 

								fw_option(array(
									'type'					=> 'checkbox',
									'title' 				=> esc_html__('Show meta info: publish date', 'loc_canon_venuex'),
									'slug' 					=> 'show_meta_date',
									'options_name'			=> 'canon_options_post',
								)); 

								fw_option(array(
									'type'					=> 'checkbox',
									'title' 				=> esc_html__('Show meta info: comments count', 'loc_canon_venuex'),
									'slug' 					=> 'show_meta_comments',
									'options_name'			=> 'canon_options_post',
								)); 

								fw_option(array(
									'type'					=> 'checkbox',
									'title' 				=> esc_html__('Show meta info: categories', 'loc_canon_venuex'),
									'slug' 					=> 'show_meta_categories',
									'options_name'			=> 'canon_options_post',
								)); 

							 ?>	

						</table>


					<!-- 
					--------------------------------------------------------------------------
						BLOG
				    -------------------------------------------------------------------------- 
					-->

						<h3><?php esc_html_e("Blog", "loc_canon_venuex"); ?> <img src="<?php echo get_template_directory_uri() . '/img/help.png' ?>"></h3>

						<div class='contextual-help'>
							
							<?php 

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Layout', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Choose between full width or sidebar layout.', 'loc_canon_venuex'),
									),
								)); 

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Sidebar', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Select what widget area to use in sidebar if sidebar layout is selected.', 'loc_canon_venuex'),
									),
								)); 

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Excerpt length', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Set the excerpt length in aprox. number of characters before cut-off.', 'loc_canon_venuex'),
									),
								)); 

							?>

						</div>

						<table class='form-table blog-section'>

							<?php 

								fw_option(array(
									'type'					=> 'select',
									'title' 				=> esc_html__('Blog Layout', 'loc_canon_venuex'),
									'slug' 					=> 'blog_layout',
									'select_options'		=> array(
										'full'				=> esc_html__('Full width', 'loc_canon_venuex'),
										'sidebar'			=> esc_html__('Sidebar', 'loc_canon_venuex'),
									),
									'options_name'			=> 'canon_options_post',
								)); 

							?>


							<tr valign='top' class="dynamic_option" data-listen_to="#blog_layout" data-listen_for="sidebar">

								<th scope='row'><?php esc_html_e("Sidebar for blog pages", "loc_canon_venuex"); ?></th>
								<td>
									<select name="canon_options_post[blog_sidebar]">
										<?php 
											for ($i = 0; $i < count($registered_sidebars_array); $i++) { 
											?>
							     				<option value="<?php echo esc_attr($registered_sidebars_array[$i]['id']); ?>" <?php if (isset($canon_options_post['blog_sidebar'])) {if ($canon_options_post['blog_sidebar'] ==  $registered_sidebars_array[$i]['id']) echo "selected='selected'";} ?>><?php echo  $registered_sidebars_array[$i]['name']; ?></option> 
											<?php
											}
										?>
									</select> 
								</td>

							</tr>


							<?php

								fw_option(array(
									'type'					=> 'number',
									'title' 				=> esc_html__('Excerpt length', 'loc_canon_venuex'),
									'slug' 					=> 'blog_excerpt_length',
									'min'					=> '1',									// optional
									'max'					=> '1000',								// optional
									'step'					=> '1',									// optional
									'width_px'				=> '60',								// optional
									'postfix'				=> '(characters)',
									'options_name'			=> 'canon_options_post',
								)); 

							?>

							<?php 



							?>




						</table>

					<!-- 
					--------------------------------------------------------------------------
						CATEGORY
				    -------------------------------------------------------------------------- 
					-->

						<h3><?php esc_html_e("Category", "loc_canon_venuex"); ?> <img src="<?php echo get_template_directory_uri() . '/img/help.png' ?>"></h3>

						<div class='contextual-help'>
							
							<?php 

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Layout', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Choose between full width or sidebar layout.', 'loc_canon_venuex'),
									),
								)); 

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Sidebar', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Select what widget area to use in sidebar if sidebar layout is selected.', 'loc_canon_venuex'),
									),
								)); 

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Excerpt length', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Set the excerpt length in aprox. number of characters before cut-off.', 'loc_canon_venuex'),
									),
								)); 

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Show category description', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Choose to display the category description at the top of category pages.', 'loc_canon_venuex'),
										wp_kses_post(__('You can set the category description at <i>Posts > Categories > Your category > Description</i>.', 'loc_canon_venuex')),
									),
								)); 

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Category pages', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Category pages will display posts within a certain category.', 'loc_canon_venuex'),
										wp_kses_post(__('To add a category page to your site go to <i>Appearance > Menus > Categories</i>. Select a category and click the Add to Menu button. Drag and drop the new menu item to the desired location in the menu.', 'loc_canon_venuex')),
									),
								)); 

							?>

						</div>

						<table class='form-table cat-section'>

							<?php 

								fw_option(array(
									'type'					=> 'select',
									'title' 				=> esc_html__('Category Layout', 'loc_canon_venuex'),
									'slug' 					=> 'cat_layout',
									'select_options'		=> array(
										'full'				=> esc_html__('Full width', 'loc_canon_venuex'),
										'sidebar'			=> esc_html__('Sidebar', 'loc_canon_venuex'),
									),
									'options_name'			=> 'canon_options_post',
								)); 

							?>
								

							<tr valign='top' class="dynamic_option" data-listen_to="#cat_layout" data-listen_for="sidebar">

								<th scope='row'><?php esc_html_e("Sidebar for category pages", "loc_canon_venuex"); ?></th>
								<td>
									<select name="canon_options_post[cat_sidebar]">
										<?php 
											for ($i = 0; $i < count($registered_sidebars_array); $i++) { 
											?>
							     				<option value="<?php echo esc_attr($registered_sidebars_array[$i]['id']); ?>" <?php if (isset($canon_options_post['cat_sidebar'])) {if ($canon_options_post['cat_sidebar'] ==  $registered_sidebars_array[$i]['id']) echo "selected='selected'";} ?>><?php echo  $registered_sidebars_array[$i]['name']; ?></option> 
											<?php
											}
										?>
									</select> 
								</td>

							</tr>


							<?php

								fw_option(array(
									'type'					=> 'number',
									'title' 				=> esc_html__('Excerpt length', 'loc_canon_venuex'),
									'slug' 					=> 'cat_excerpt_length',
									'min'					=> '1',									// optional
									'max'					=> '1000',								// optional
									'step'					=> '1',									// optional
									'width_px'				=> '60',								// optional
									'postfix'				=> '(characters)',
									'options_name'			=> 'canon_options_post',
								)); 

								fw_option(array(
									'type'					=> 'checkbox',
									'title' 				=> esc_html__('Show category title', 'loc_canon_venuex'),
									'slug' 					=> 'show_cat_title',
									'options_name'			=> 'canon_options_post',
								)); 

								fw_option(array(
									'type'					=> 'checkbox',
									'title' 				=> esc_html__('Show category description', 'loc_canon_venuex'),
									'slug' 					=> 'show_cat_description',
									'options_name'			=> 'canon_options_post',
								)); 

							?>



						</table>

					<!-- 
					--------------------------------------------------------------------------
						OTHER ARCHIVE PAGES
				    -------------------------------------------------------------------------- 
					-->

						<h3><?php esc_html_e("Other archive pages", "loc_canon_venuex"); ?> <img src="<?php echo get_template_directory_uri() . '/img/help.png' ?>"></h3>

						<div class='contextual-help'>
							
							<?php 

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Layout', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Choose between full width or sidebar layout.', 'loc_canon_venuex'),
									),
								)); 

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Sidebar', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Select what widget area to use in sidebar if sidebar layout is selected.', 'loc_canon_venuex'),
									),
								)); 

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Excerpt length', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Set the excerpt length in aprox. number of characters before cut-off.', 'loc_canon_venuex'),
									),
								)); 
							?>

						</div>

						<table class='form-table archive-section'>

							<?php 

								fw_option(array(
									'type'					=> 'select',
									'title' 				=> esc_html__('Archive Layout', 'loc_canon_venuex'),
									'slug' 					=> 'archive_layout',
									'select_options'		=> array(
										'full'				=> esc_html__('Full width', 'loc_canon_venuex'),
										'sidebar'			=> esc_html__('Sidebar', 'loc_canon_venuex'),
									),
									'options_name'			=> 'canon_options_post',
								)); 

							?>
								

							<tr valign='top' class="dynamic_option" data-listen_to="#archive_layout" data-listen_for="sidebar">

								<th scope='row'><?php esc_html_e("Sidebar for archive pages", "loc_canon_venuex"); ?></th>
								<td>
									<select name="canon_options_post[archive_sidebar]">
										<?php 
											for ($i = 0; $i < count($registered_sidebars_array); $i++) { 
											?>
							     				<option value="<?php echo esc_attr($registered_sidebars_array[$i]['id']); ?>" <?php if (isset($canon_options_post['archive_sidebar'])) {if ($canon_options_post['archive_sidebar'] ==  $registered_sidebars_array[$i]['id']) echo "selected='selected'";} ?>><?php echo  $registered_sidebars_array[$i]['name']; ?></option> 
											<?php
											}
										?>
									</select> 
								</td>

							</tr>


							<?php

								fw_option(array(
									'type'					=> 'number',
									'title' 				=> esc_html__('Excerpt length', 'loc_canon_venuex'),
									'slug' 					=> 'archive_excerpt_length',
									'min'					=> '1',									// optional
									'max'					=> '1000',								// optional
									'step'					=> '1',									// optional
									'width_px'				=> '60',								// optional
									'postfix'				=> '(characters)',
									'options_name'			=> 'canon_options_post',
								)); 

							?>



						</table>


					<!-- 
					--------------------------------------------------------------------------
						SEARCH 
				    -------------------------------------------------------------------------- 
					-->

						<h3><?php esc_html_e("Search", "loc_canon_venuex"); ?> <img src="<?php echo get_template_directory_uri() . '/img/help.png' ?>"></h3>

						<div class='contextual-help'>
							<?php 

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Search box text', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('The text that displays inside the search box.', 'loc_canon_venuex'),
									),
								)); 

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Search post types', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Select what post types to include in search. Notice that deselecting all post types will result in no filters being applied to search (default WordPress behaviour) and all post types containing the search term will be returned on the search results page. This may not always be what you want as a lot of custom post types are for internal theme/plugin use only and are not meant to be viewed as regular posts. Correct styling and functionality of search results can only be guaranteed for posts and pages. Including custom post types in search is to be viewed as "experimental" and is "use-at-own-risk".', 'loc_canon_venuex'),
									),
								)); 

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Custom post types', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('What custom post types to include in search when Search custom post types has been selected. Separate with commas. Notice that you need to put in the custom post type slug. If you are unsure what the slug of a certain custom post type is please consult the plugin documentation or the plugin author.', 'loc_canon_venuex'),
									),
								)); 

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Search window widgetized area layout', 'loc_canon_venuex'),
									'content' 				=> array(
										wp_kses_post(__('Search buttons like those that can be added to the header (<i>Header & Footer > General Settings > Add search button to primary/secondary menu</i>) will activate a full screen search window. This window has a search area as well as a widgetized area. Select a layout for the widgetized area.', 'loc_canon_venuex')),
										'',
										wp_kses_post(__('<b>Example</b>: The layout "third + two-thirds" is a three column layout meaning that the widgetized area will be divided into three columns. The first widget area will occupy the first column and the second widget area will span the last two columns.', 'loc_canon_venuex')),
									),
								)); 

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Search widget areas', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Select what widget areas to display in the search window widgetized area.', 'loc_canon_venuex'),
									),
								)); 

							?>

						</div>

						<table class='form-table'>

							<?php
								
								fw_option(array(
									'type'					=> 'text',
									'title' 				=> esc_html__('Search box text', 'loc_canon_venuex'),
									'slug' 					=> 'search_box_text',
									'class'					=> 'widefat',
									'options_name'			=> 'canon_options_post',
								)); 
							
								fw_option(array(
									'type'					=> 'checkbox',
									'title' 				=> esc_html__('Search posts', 'loc_canon_venuex'),
									'slug' 					=> 'search_posts',
									'options_name'			=> 'canon_options_post',
								)); 

								fw_option(array(
									'type'					=> 'checkbox',
									'title' 				=> esc_html__('Search pages', 'loc_canon_venuex'),
									'slug' 					=> 'search_pages',
									'options_name'			=> 'canon_options_post',
								)); 

								fw_option(array(
									'type'					=> 'checkbox',
									'title' 				=> esc_html__('Search custom post types', 'loc_canon_venuex'),
									'slug' 					=> 'search_cpt',
									'options_name'			=> 'canon_options_post',
								)); 

								fw_option(array(
									'type'					=> 'text',
									'title' 				=> esc_html__('Custom post types', 'loc_canon_venuex'),
									'slug' 					=> 'search_cpt_source',
									'class'					=> 'widefat',
									'options_name'			=> 'canon_options_post',
								)); 
							
								fw_option(array(
									'type'					=> 'select_reverse',
									'title' 				=> esc_html__('Search window widgetized area layout', 'loc_canon_venuex'),
									'slug' 					=> 'widgetized_search_layout',
									'select_options'		=> array(
										esc_html__('TWO COLUMN LAYOUT', 'loc_canon_venuex')							=> 'half_half',
										esc_html__('half + half', 'loc_canon_venuex')									=> 'half_half',
										''																=> 'third_third_third',
										esc_html__('THREE COLUMN LAYOUTS', 'loc_canon_venuex')							=> 'third_third_third',
										esc_html__('third + third + third', 'loc_canon_venuex')						=> 'third_third_third',
										esc_html__('two-thirds + third', 'loc_canon_venuex')							=> 'two-thirds_third',
										esc_html__('third + two-thirds', 'loc_canon_venuex')							=> 'third_two-thirds',
										' '																=> 'fourth_fourth_fourth_fourth',
										esc_html__('FOUR COLUMN LAYOUTS', 'loc_canon_venuex')							=> 'fourth_fourth_fourth_fourth',
										esc_html__('fourth + fourth + fourth + fourth', 'loc_canon_venuex')			=> 'fourth_fourth_fourth_fourth',
										esc_html__('fourth + half + fourth', 'loc_canon_venuex')						=> 'fourth_half_fourth',
										esc_html__('fourth + fourth + half', 'loc_canon_venuex')						=> 'fourth_fourth_half',
										'  '															=> 'fifth_fifth_fifth_fifth_fifth',
										esc_html__('FIVE COLUMN LAYOUTS', 'loc_canon_venuex')							=> 'fifth_fifth_fifth_fifth_fifth',
										esc_html__('fifth + fifth + fifth + fifth + fifth', 'loc_canon_venuex')		=> 'fifth_fifth_fifth_fifth_fifth',
									),
									'options_name'			=> 'canon_options_post',
								)); 

								for ($i = 1; $i < 6; $i++) {  

									fw_option(array(
										'type'					=> 'select_sidebar',
										'title' 				=> esc_html__('Search Widget Area ', 'loc_canon_venuex') . $i,
										'slug' 					=> 'search_widget_area_' .$i,
										'options_name'			=> 'canon_options_post',
									)); 

								}
							?>			

						</table>

					<!-- 
					--------------------------------------------------------------------------
						404
				    -------------------------------------------------------------------------- 
					-->

						<h3>404 <img src="<?php echo get_template_directory_uri() . '/img/help.png' ?>"></h3>

						<div class='contextual-help'>
							<?php 

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('404 layout', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Choose between full width or sidebar layout.', 'loc_canon_venuex'),
									),
								)); 

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('404 title', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Title that displays on the 404-page.', 'loc_canon_venuex'),
									),
								)); 

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('404 message', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Message to display on the 404-page.', 'loc_canon_venuex'),
									),
								)); 

							?>
						</div>

						<table class='form-table'>

							<?php 

								fw_option(array(
									'type'					=> 'select',
									'title' 				=> esc_html__('404 Layout', 'loc_canon_venuex'),
									'slug' 					=> '404_layout',
									'select_options'		=> array(
										'full'				=> esc_html__('Full width', 'loc_canon_venuex'),
										'sidebar'			=> esc_html__('Sidebar', 'loc_canon_venuex'),
									),
									'options_name'			=> 'canon_options_post',
								)); 

							?>
								

							<tr valign='top' class="dynamic_option" data-listen_to="#404_layout" data-listen_for="sidebar">

								<th scope='row'><?php esc_html_e("Sidebar for 404 page", "loc_canon_venuex"); ?></th>
								<td>
									<select name="canon_options_post[404_sidebar]">
										<?php 
											for ($i = 0; $i < count($registered_sidebars_array); $i++) { 
											?>
							     				<option value="<?php echo esc_attr($registered_sidebars_array[$i]['id']); ?>" <?php if (isset($canon_options_post['404_sidebar'])) {if ($canon_options_post['404_sidebar'] ==  $registered_sidebars_array[$i]['id']) echo "selected='selected'";} ?>><?php echo  $registered_sidebars_array[$i]['name']; ?></option> 
											<?php
											}
										?>
									</select> 
								</td>

							</tr>

							<?php
								
								fw_option(array(
									'type'					=> 'text',
									'title' 				=> esc_html__('404 title', 'loc_canon_venuex'),
									'slug' 					=> '404_title',
									'class'					=> 'widefat',
									'options_name'			=> 'canon_options_post',
								)); 

								fw_option(array(
									'type'					=> 'textarea',
									'title' 				=> esc_html__('404 message', 'loc_canon_venuex'),
									'slug' 					=> '404_msg',
									'cols'					=> '100',
									'rows'					=> '5',
									'options_name'			=> 'canon_options_post',
								)); 
							
							?>			

						</table>
						
						
					<!-- 
					--------------------------------------------------------------------------
						LISTINGS
				    -------------------------------------------------------------------------- 
					-->

						<h3><?php esc_html_e("Listings", "loc_canon_venuex"); ?> <img src="<?php echo get_template_directory_uri() . '/img/help.png' ?>"></h3>

						<div class='contextual-help'>
							<?php 

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Currency symbol', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Choose which currency symbol to use. Can be left empty for no currency symbol.', 'loc_canon_venuex'),
									),
								)); 

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Currency symbol position', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Choose whether the currency symbol should be prepended or appended the price.', 'loc_canon_venuex'),
									),
								)); 

							?>
						</div>

						<table class='form-table'>

							<?php
								
								fw_option(array(
									'type'					=> 'text',
									'title' 				=> esc_html__('Currency symbol', 'loc_canon_venuex'),
									'slug' 					=> 'currency_symbol',
									'class'					=> 'widefat',
									'options_name'			=> 'canon_options_post',
								)); 

								fw_option(array(
									'type'					=> 'select',
									'title' 				=> esc_html__('Currency symbol position', 'loc_canon_venuex'),
									'slug' 					=> 'currency_symbol_pos',
									'select_options'		=> array(
										'prepend'				=> esc_html__('Prepend', 'loc_canon_venuex'),
										'append'				=> esc_html__('Append', 'loc_canon_venuex'),
									),
									'options_name'			=> 'canon_options_post',
								)); 

							
							?>			

						</table>
						

					<?php 

						if (class_exists('RevSlider')) {
						?>

					<!-- 
					--------------------------------------------------------------------------
						REVOLUTION SLIDER
				    -------------------------------------------------------------------------- 
					-->

							<h3>Revolution Slider <img src="<?php echo get_template_directory_uri() . '/img/help.png' ?>"></h3>

							<div class='contextual-help'>
								<?php 

									fw_option_help(array(
										'type'					=> 'standard',
										'title' 				=> esc_html__('Clean UI', 'loc_canon_venuex'),
										'content' 				=> array(
											esc_html__('By default the Revolution Slider (AKA Slider Revolution) interface contains practical information that is not relevant in themes where the Revolution Slider plugin comes bundled with the theme. This option will let you hide this information for a cleaner user interface.', 'loc_canon_venuex'),
										),
									)); 

									fw_option_help(array(
										'type'					=> 'standard',
										'title' 				=> esc_html__('A note on bundled plugins', 'loc_canon_venuex'),
										'content' 				=> array(
											esc_html__('This theme comes bundled with a free (fully featured) version of the Revolution Slider plugin. Please be aware that the free bundled version may not always be the latest version of the plugin. We update the bundled version regularly but not necessarily at the same time or every time a new version is released as we need time to test the updated plugin with our theme to ensure optimal compatibility. This is the only limitation of the free plugin that comes bundled with our theme. If you want to activate automatic updates for the Revolution Slider plugin or you urgently need the latest version of the plugin you will have to buy a regular license for the Revolution Slider plugin.', 'loc_canon_venuex'),
										),
									)); 


								?>

							</div>

							<table class='form-table'>

								<?php
								
									fw_option(array(
										'type'					=> 'checkbox',
										'title' 				=> esc_html__('Clean user interface', 'loc_canon_venuex'),
										'slug' 					=> 'revslider_clean_ui',
										'options_name'			=> 'canon_options_post',
									)); 

								?>

							</table>

						 		
						<?php	
						}
					?>


					<?php 

						if (is_plugin_active('woocommerce/woocommerce.php')) {
						?>

					<!-- 
					--------------------------------------------------------------------------
						WOOCOMMERCE
				    -------------------------------------------------------------------------- 
					-->

							<h3>WooCommerce <img src="<?php echo get_template_directory_uri() . '/img/help.png' ?>"></h3>

							<div class='contextual-help'>
								<?php 

									fw_option_help(array(
										'type'					=> 'standard',
										'title' 				=> esc_html__('Sidebar on shop and product pages', 'loc_canon_venuex'),
										'content' 				=> array(
											esc_html__('Choose to have a sidebar displayed on your shop and product pages.', 'loc_canon_venuex'),
										),
									)); 

									fw_option_help(array(
										'type'					=> 'standard',
										'title' 				=> esc_html__('What about the other WooCommerce pages?', 'loc_canon_venuex'),
										'content' 				=> array(
											esc_html__('Other WooCommerce pages use ordinary page templates. You can change which template to use for each of the WooCommerce pages (sidebar or full width page templates).', 'loc_canon_venuex'),
										),
									)); 

									fw_option_help(array(
										'type'					=> 'standard',
										'title' 				=> esc_html__('Sidebar for WooCommerce pages', 'loc_canon_venuex'),
										'content' 				=> array(
											esc_html__('Choose which sidebar to use for your WooCommerce pages. This will be the same across all WooCommerce pages that have a sidebar.', 'loc_canon_venuex'),
										),
									)); 

								?>

							</div>

							<table class='form-table'>

								<?php
								
									fw_option(array(
										'type'					=> 'checkbox',
										'title' 				=> esc_html__('Sidebar on shop and product pages', 'loc_canon_venuex'),
										'slug' 					=> 'use_woocommerce_sidebar',
										'options_name'			=> 'canon_options_post',
									)); 

									fw_option(array(
										'type'					=> 'select_sidebar',
										'title' 				=> esc_html__('Sidebar for WooCommerce pages', 'loc_canon_venuex'),
										'slug' 					=> 'woocommerce_sidebar',
										'options_name'			=> 'canon_options_post',
									)); 

								?>

							</table>

						 		
						<?php	
						}
					?>



					<?php 

						if (function_exists('bp_is_active')) {
						?>

					<!-- 
					--------------------------------------------------------------------------
						BUDDYPRESS
				    -------------------------------------------------------------------------- 
					-->

							<h3>BuddyPress <img src="<?php echo get_template_directory_uri() . '/img/help.png' ?>"></h3>

							<div class='contextual-help'>
								<?php 

									fw_option_help(array(
										'type'					=> 'standard',
										'title' 				=> esc_html__('Sidebar on BuddyPress pages', 'loc_canon_venuex'),
										'content' 				=> array(
											esc_html__('Choose to have a sidebar displayed on your BuddyPress pages.', 'loc_canon_venuex'),
										),
									)); 

									fw_option_help(array(
										'type'					=> 'standard',
										'title' 				=> esc_html__('Sidebar for BuddyPress pages', 'loc_canon_venuex'),
										'content' 				=> array(
											esc_html__('Choose which sidebar to use for your BuddyPress pages.', 'loc_canon_venuex'),
										),
									)); 

								?>

							</div>

							<table class='form-table'>

								<?php
								
									fw_option(array(
										'type'					=> 'checkbox',
										'title' 				=> esc_html__('Sidebar on BuddyPress pages', 'loc_canon_venuex'),
										'slug' 					=> 'use_buddypress_sidebar',
										'options_name'			=> 'canon_options_post',
									)); 

									fw_option(array(
										'type'					=> 'select_sidebar',
										'title' 				=> esc_html__('Sidebar for BuddyPress pages', 'loc_canon_venuex'),
										'slug' 					=> 'buddypress_sidebar',
										'options_name'			=> 'canon_options_post',
									)); 

								?>

							</table>

						 		
						<?php	
						}
					?>


					<?php 

						if (class_exists('bbPress')) {
						?>

					<!-- 
					--------------------------------------------------------------------------
						BBPRESS
				    -------------------------------------------------------------------------- 
					-->

							<h3>bbPress <img src="<?php echo get_template_directory_uri() . '/img/help.png' ?>"></h3>

							<div class='contextual-help'>
								<?php 

									fw_option_help(array(
										'type'					=> 'standard',
										'title' 				=> esc_html__('Sidebar on bbPress pages', 'loc_canon_venuex'),
										'content' 				=> array(
											esc_html__('Choose to have a sidebar displayed on your bbPress pages.', 'loc_canon_venuex'),
										),
									)); 

									fw_option_help(array(
										'type'					=> 'standard',
										'title' 				=> esc_html__('Sidebar for bbPress pages', 'loc_canon_venuex'),
										'content' 				=> array(
											esc_html__('Choose which sidebar to use for your bbPress pages.', 'loc_canon_venuex'),
										),
									)); 

								?>

							</div>

							<table class='form-table'>

								<?php
								
									fw_option(array(
										'type'					=> 'checkbox',
										'title' 				=> esc_html__('Sidebar on bbPress pages', 'loc_canon_venuex'),
										'slug' 					=> 'use_bbpress_sidebar',
										'options_name'			=> 'canon_options_post',
									)); 

									fw_option(array(
										'type'					=> 'select_sidebar',
										'title' 				=> esc_html__('Sidebar for bbPress pages', 'loc_canon_venuex'),
										'slug' 					=> 'bbpress_sidebar',
										'options_name'			=> 'canon_options_post',
									)); 

								?>

							</table>

						 		
						<?php	
						}
					?>
					


					<?php 

						if (class_exists('Tribe__Events__Main')) {
						?>

					<!-- 
					--------------------------------------------------------------------------
						THE EVENTS CALENDAR BY TRIBE
				    -------------------------------------------------------------------------- 
					-->

							<h3>The Events Calendar <img src="<?php echo get_template_directory_uri() . '/img/help.png' ?>"></h3>

							<div class='contextual-help'>
								<?php 

									fw_option_help(array(
										'type'					=> 'standard',
										'title' 				=> esc_html__('Sidebar on Events pages', 'loc_canon_venuex'),
										'content' 				=> array(
											esc_html__('Choose to have a sidebar displayed on your Events pages. Make sure you are using the Default Events Template (Events > Settings > Display > Events Template).', 'loc_canon_venuex'),
										),
									)); 

									fw_option_help(array(
										'type'					=> 'standard',
										'title' 				=> esc_html__('Sidebar for Events pages', 'loc_canon_venuex'),
										'content' 				=> array(
											esc_html__('Choose which sidebar to use for your Events pages by default.', 'loc_canon_venuex'),
										),
									)); 

									fw_option_help(array(
										'type'					=> 'standard',
										'title' 				=> esc_html__('Slider for Events pages', 'loc_canon_venuex'),
										'content' 				=> array(
											esc_html__('Choose which slider to use for your Events pages by default.', 'loc_canon_venuex'),
										),
									)); 

									fw_option_help(array(
										'type'					=> 'standard',
										'title' 				=> esc_html__('Optimize Events settings', 'loc_canon_venuex'),
										'content' 				=> array(
											esc_html__('Checking this option will change certain settings in The Events Calendar settings menu. This will ensure that the plugin content is displayed as intended by the theme.', 'loc_canon_venuex'),
											'',
											esc_html__('These specific settings are set:', 'loc_canon_venuex'),
											wp_kses_post(__('<strong>Default stylesheet used for events templates</strong> is set to <i>Skeleton Styles</i>.', 'loc_canon_venuex')),
											wp_kses_post(__('<strong>Events template</strong> is set to <i>Default Events Template</i>.', 'loc_canon_venuex')),
										),
									)); 

								?>

							</div>

							<table class='form-table'>

								<?php
								
									fw_option(array(
										'type'					=> 'checkbox',
										'title' 				=> esc_html__('Sidebar on Events pages', 'loc_canon_venuex'),
										'slug' 					=> 'use_events_sidebar',
										'options_name'			=> 'canon_options_post',
									)); 

									fw_option(array(
										'type'					=> 'select_sidebar',
										'title' 				=> esc_html__('Sidebar for Events pages', 'loc_canon_venuex'),
										'slug' 					=> 'events_sidebar',
										'options_name'			=> 'canon_options_post',
									)); 

									fw_option(array(
										'type'					=> 'select_revslider',
										'title' 				=> esc_html__('Slider for Events pages', 'loc_canon_venuex'),
										'select_options'		=> array(
											'no_slider'				=> esc_html__('No slider', 'loc_canon_venuex'),
											''						=> esc_html__('---', 'loc_canon_venuex')
										),
										'slug' 					=> 'events_slider',
										'options_name'			=> 'canon_options_post',
									)); 

									fw_option(array(
										'type'					=> 'checkbox',
										'title' 				=> esc_html__('Optimize Events settings', 'loc_canon_venuex'),
										'slug' 					=> 'optimize_events_settings',
										'options_name'			=> 'canon_options_post',
									)); 

								?>

							</table>

						 		
						<?php	
						}
					?>
					


					<!-- END OPTIONS AND WRAP UP FILE -->

					<?php submit_button(); ?>

				</form>
			</div> <!-- end table container -->	

	
		</div>

	</div>

