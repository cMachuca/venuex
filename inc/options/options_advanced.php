	<div class="wrap">

		<div id="icon-themes" class="icon32"></div>

		<h2><?php printf( "%s %s - %s", esc_attr(wp_get_theme()->Name), esc_html__("Settings", "loc_canon_venuex"), esc_html__("Advanced", "loc_canon_venuex") ); ?></h2>

		<?php 
			$canon_options_advanced = get_option('canon_options_advanced'); 

			//LOAD OPTIONS
			$canon_options = get_option('canon_options'); 
			$canon_options_frame = get_option('canon_options_frame'); 
			$canon_options_post = get_option('canon_options_post'); 
			$canon_options_appearance = get_option('canon_options_appearance');
			$canon_options_advanced = get_option('canon_options_advanced'); 


		////////////////////////////////////////////////
		// IMPORT/EXPORT SETTINGS
		////////////////////////////////////////////////

			//MAKE SUPERARRAY AND ENCODE
			$canon_options_superarray = array(
				'canon_options' => $canon_options,
				'canon_options_frame' => $canon_options_frame,
				'canon_options_post' => $canon_options_post,
				'canon_options_appearance' => $canon_options_appearance,
				'canon_options_advanced' => $canon_options_advanced,

			);
			$encoded_serialized_options_data = mb_filter_sensitive_input(json_encode($canon_options_superarray));

			//IF IMPORT DATA WAS CLICKED
			if ( (isset($canon_options_advanced['import_data'])) && (isset($canon_options_advanced['canon_options_data'])) )  {
				if ($canon_options_advanced['import_data'] == 'IMPORT') {
					
					//get import data (returns false if improper structured data sent)
					$import_superarray = @json_decode(mb_filter_sensitive_output($canon_options_advanced['canon_options_data']), true);

					//only proceed if unserialize succeeded
					if ($import_superarray) {
						//replace old data with new data
						$canon_options = mb_array_replace($canon_options, $import_superarray['canon_options']);
						$canon_options_frame = mb_array_replace($canon_options_frame, $import_superarray['canon_options_frame']);
						$canon_options_post = mb_array_replace($canon_options_post, $import_superarray['canon_options_post']);
						$canon_options_appearance = mb_array_replace($canon_options_appearance, $import_superarray['canon_options_appearance']);
						$canon_options_advanced = mb_array_replace($canon_options_advanced, $import_superarray['canon_options_advanced']);

						//update data to database
						update_option('canon_options', $canon_options);
						update_option('canon_options_frame', $canon_options_frame);
						update_option('canon_options_post', $canon_options_post);
						update_option('canon_options_appearance', $canon_options_appearance);
						update_option('canon_options_advanced', $canon_options_advanced);

						//get data from database (is this not superfluous?)
						$canon_options = get_option('canon_options'); 
						$canon_options_frame = get_option('canon_options_frame'); 
						$canon_options_post = get_option('canon_options_post'); 
						$canon_options_appearance = get_option('canon_options_appearance');
						$canon_options_advanced = get_option('canon_options_advanced'); 

						//display success notice:
						echo '<div class="updated"><p>Settings successfully imported!</p></div>';

					} else {
							
						//display fail notice:
						echo '<div class="error"><p>Import failed!</p></div>';

					}

				}
					
			}


		////////////////////////////////////////////////
		// IMPORT/EXPORT WIDGETS
		////////////////////////////////////////////////

			// MAKE WIDGETS SUPERARRAY
			$canon_widgets_superarray = array();

			// GET AND ADD WIDGET AREAS SUBARRAY
			$widget_areas = get_option('sidebars_widgets');
			$canon_widgets_superarray['widget_areas'] = $widget_areas;

			// CREATE AND ADD ACTIVE WIDGETS SUBARRAY
			$active_widgets = array();
			foreach ($widget_areas as $area_slug => $area_content) {			// first we create an array of active widget slugs
				if (is_array($area_content) && !empty($area_content)) {
					foreach ($area_content as $key => $widget_name) {
						// grab and delete postfix
						$widget_name_explode_array = explode('-', $widget_name);
						$last_index = count($widget_name_explode_array)-1;
						$postfix = "-" . $widget_name_explode_array[$last_index];
						$widget_name = str_replace($postfix, "", $widget_name);
						array_push($active_widgets, $widget_name);
					}
				}
			}
			$active_widgets = array_unique($active_widgets);
			foreach ($active_widgets as $key => $widget_slug) {					// then we convert the array of active widget slugs to an assoc array of active widget slugs and their settings
				$widget_settings_array = get_option('widget_' . $widget_slug);
				$active_widgets[$widget_slug] = $widget_settings_array;
				unset($active_widgets[$key]);

			}
			$canon_widgets_superarray['active_widgets'] = $active_widgets;
			$encoded_serialized_widgets_data = mb_filter_sensitive_input(json_encode($canon_widgets_superarray));

			//IF IMPORT WIDGETS DATA WAS CLICKED
			if ( (isset($canon_options_advanced['import_widgets_data'])) && (isset($canon_options_advanced['canon_widgets_data'])) )  {
				if ($canon_options_advanced['import_widgets_data'] == 'IMPORT') {
					
					//get import data (returns false if improper structured data sent)
					$import_widgets_superarray = @json_decode(mb_filter_sensitive_output($canon_options_advanced['canon_widgets_data']), true);

					//only proceed if unserialize succeeded
					if ($import_widgets_superarray) {

						// first replace widget areas
						update_option('sidebars_widgets', $import_widgets_superarray['widget_areas']);

						// next replace active widget settings
						foreach ($import_widgets_superarray['active_widgets'] as $widget_slug => $widget_content) {
							update_option('widget_' . $widget_slug, $widget_content);
						}

						// update data to database
						unset($canon_options_advanced['import_widgets_data']);
						unset($canon_options_advanced['canon_widgets_data']);
						update_option('canon_options_advanced', $canon_options_advanced);

						// get data from database (is this not superfluous?)
						$canon_options_advanced = get_option('canon_options_advanced'); 

						//display success notice:
						echo '<div class="updated"><p>Widgets successfully imported!</p></div>';

					} else {
							
						//display fail notice:
						echo '<div class="error"><p>Import failed!</p></div>';

					}

				}
					
			}



		////////////////////////////////////////////////
		// RESET SETTINGS
		////////////////////////////////////////////////

			//RESET BASIC
			if ($canon_options_advanced['reset_basic'] == 'RESET') {
				delete_option('canon_options');
				delete_option('canon_options_frame');
				delete_option('canon_options_post');
				delete_option('canon_options_appearance');

				// clear reset_basic var
				$canon_options_advanced['reset_basic'] = "";
				update_option('canon_options_advanced', $canon_options_advanced);

				// output response
				echo "<script>alert('Basic theme settings have been reset!'); window.location.reload();</script>";
			}


			//RESET ALL
			if ($canon_options_advanced['reset_all'] == 'RESET') {
				delete_option('canon_options');
				delete_option('canon_options_frame');
				delete_option('canon_options_post');
				delete_option('canon_options_appearance');
				delete_option('canon_options_advanced');


				// output response
				echo "<script>alert('All theme settings have been reset!'); window.location.reload();</script>";
			}


		////////////////////////////////////////////////
		// MISC
		////////////////////////////////////////////////

			// remove template + remove duplicate custom widget areas and rearrange keys
			if (isset($canon_options_advanced['custom_widget_areas'][9999])) { unset($canon_options_advanced['custom_widget_areas'][9999]); }
            $canon_options_advanced['custom_widget_areas'] = array_values($canon_options_advanced['custom_widget_areas']);

			// delete_option('canon_options_advanced');
			// var_dump($canon_options_advanced);

		?>

		<br>
		
		<div class="options_wrapper canon-options">
		
			<div class="table_container">

				<form method="post" action="options.php" enctype="multipart/form-data">
					<?php settings_fields('group_canon_options_advanced'); ?>				<!-- very important to add these two functions as they mediate what wordpress generates automatically from the functions.php -->
					<?php do_settings_sections('handle_canon_options_advanced'); ?>		

					<?php submit_button(); ?>
					
					<!-- 

						INDEX

						CUSTOM WIDGET AREAS (CWA)
						FINAL CALL CSS
						IMPORT/EXPORT SETTINGS
						IMPORT/EXPORT WIDGETS
						INSTAGRAM AUTHORIZATION
					
					-->

					<!-- 
					--------------------------------------------------------------------------
						CUSTOM WIDGET AREAS (CWA)
				    -------------------------------------------------------------------------- 
					-->

						<h3><?php esc_html_e("Widget Areas Manager", "loc_canon_venuex"); ?> <img src="<?php echo get_template_directory_uri() . '/img/help.png' ?>"></h3>

						<div class='contextual-help'>
							<?php
								
								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Widget Areas Manager', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Here you can create new custom widget areas. Give each widget area a unique name.', 'loc_canon_venuex'),
										esc_html__('You can drag and drop to decide the order of which the widget areas will display in the widgets section.', 'loc_canon_venuex'),
										wp_kses_post(__('To add widgets to your custom widget areas go to <i>WordPress Appearance > Widgets</i>.', 'loc_canon_venuex')),
										esc_html__('To display your custom widget areas go to Pagebuilder and add a Widgets block to one of your pages.', 'loc_canon_venuex'),
									),
								)); 

							?>

						</div>

						<table class='form-table'>

							<tr>

								<th scope='row'></th>
								<td>
									<ul id="cwa_template">

												<!-- TEMPLATE: C/P LI -->
												<?php $i=9999; ?>

												<li>
													<span><?php esc_html_e("Custom Widget Area Name", "loc_canon_venuex"); ?>:<span>
													<span class="cwa_del"><a href="#"><?php esc_html_e("Delete", "loc_canon_venuex"); ?></a></span>
													<input class='cwa_option' type='text' name='canon_options_advanced[custom_widget_areas][<?php echo esc_attr($i); ?>][name]' value="<?php if (isset($canon_options_advanced['custom_widget_areas'][$i]['name'])) echo htmlspecialchars($canon_options_advanced['custom_widget_areas'][$i]['name']); ?>">
												</li>


									</ul>
								</td>
							</tr>

							<tr>
								<th scope='row'><?php esc_html_e("Custom Widget Areas", "loc_canon_venuex"); ?></th>
								<td>
									<ul id="cwa_list" class="cwa_sortable">

										<?php 

											if (isset($canon_options_advanced['custom_widget_areas'])) {

												for ($i = 0; $i < count($canon_options_advanced['custom_widget_areas']); $i++) {  
												?>

												<li>
													<span><?php esc_html_e("Custom Widget Area Name", "loc_canon_venuex"); ?>:<span>
													<span class="cwa_del"><a href="#"><?php esc_html_e("Delete", "loc_canon_venuex"); ?></a></span>
													<input class='cwa_option' type='text' name='canon_options_advanced[custom_widget_areas][<?php echo esc_attr($i); ?>][name]' value="<?php if (isset($canon_options_advanced['custom_widget_areas'][$i]['name'])) echo htmlspecialchars($canon_options_advanced['custom_widget_areas'][$i]['name']); ?>">
												</li>

												<?php
												}

											}

										?>

									</ul>
								</td>
							</tr>

							<tr valign='top'>
								<th scope='row'></th>
								<td>
									<input type="button" class="button button_add_cwa" value="<?php esc_html_e("Create new custom widget area", "loc_canon_venuex"); ?>" />
									<br><br>
								</td>
							</tr>




						</table>


					<!-- 
					--------------------------------------------------------------------------
						FINAL CALL CSS
				    -------------------------------------------------------------------------- 
					-->

						<h3><?php esc_html_e("Final Call CSS", "loc_canon_venuex"); ?> <img src="<?php echo get_template_directory_uri() . '/img/help.png' ?>"></h3>

						<div class='contextual-help'>

							<?php
								
								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Final call CSS', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Put your own CSS code here. This CSS will be called last and overwrites all theme CSS.', 'loc_canon_venuex'),
										esc_html__('Final call CSS will be exported/imported along with all other theme settings when using the Import/Export option.', 'loc_canon_venuex'),
									),
								)); 

							?>

						</div>

						<table class='form-table'>

							<?php 

								fw_option(array(
									'type'					=> 'checkbox',
									'title' 				=> esc_html__('Use final call CSS', 'loc_canon_venuex'),
									'slug' 					=> 'use_final_call_css',
									'options_name'			=> 'canon_options_advanced',
								)); 

							?>


							<tr valign='top'>
								<th></th>
								<td colspan="2">
									<textarea id='final_call_css' name='canon_options_advanced[final_call_css]' rows='20' cols='100'><?php if (isset($canon_options_advanced['final_call_css'])) echo htmlentities($canon_options_advanced['final_call_css']); ?></textarea>

								</td>
							</tr>

						</table>




						<table class='form-table'>

						</table>


					<!-- 
					--------------------------------------------------------------------------
						IMPORT/EXPORT SETTINGS
				    -------------------------------------------------------------------------- 
					-->

						<h3><?php esc_html_e("Import/Export Settings", "loc_canon_venuex"); ?> <img src="<?php echo get_template_directory_uri() . '/img/help.png' ?>"></h3>

						<div class='contextual-help'>
							<?php 

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Import/Export settings', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Use this section to import/export your settings.', 'loc_canon_venuex'),
										wp_kses_post(__('<strong>WARNING</strong>: Settings may be overwritten/deleted/replaced. ', 'loc_canon_venuex')),
									),
								)); 

								fw_option_help(array(
									'type'					=> 'ul',
									'title' 				=> esc_html__('Generate settings data', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Clicking this button will generate settings data. You can copy this data from the settings data window.', 'loc_canon_venuex'),
										esc_html__('Clicking the window will select all text.', 'loc_canon_venuex'),
										esc_html__('Press CTRL+C on your keyboard or right click selected text and select copy.', 'loc_canon_venuex'),
										esc_html__('Once you have copied the data you can either save it to a text document/file (safest) or simply keep the data in your copy/paste clipboard (not safe).', 'loc_canon_venuex'),
									),
								)); 

								fw_option_help(array(
									'type'					=> 'ul',
									'title' 				=> esc_html__('Import settings data', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Clicking this button will import your settings data from the data string supplied in the settings data window.', 'loc_canon_venuex'),
										esc_html__('Make sure you paste all of the data into the settings data textarea/window. If part of the code is altered or left out import will fail.', 'loc_canon_venuex'),
										esc_html__('Click the "Import settings data" button.', 'loc_canon_venuex'),
										esc_html__('Your setting have now been imported.', 'loc_canon_venuex'),
									),
								)); 

								fw_option_help(array(
									'type'					=> 'ul',
									'title' 				=> esc_html__('Load predefined settings data', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Use this select to load predefined settings data into the data window.', 'loc_canon_venuex'),
										esc_html__('Click the "Import settings data" button.', 'loc_canon_venuex'),
										esc_html__('The predefined settings have now been imported.', 'loc_canon_venuex'),
									),
								)); 

							?>
						</div>

						<table class='form-table import-export'>

							<tr valign='top'>
								<th scope='row'><?php esc_html_e("Settings data", "loc_canon_venuex"); ?></th>
								<td colspan="2">
									<textarea id='canon_options_data' class='canon_export_data' name='canon_options_advanced[canon_options_data]' rows='5' cols='100'></textarea>
								</td>
							</tr>

							<tr valign='top'>
								<th scope='row'></th>
								<td>
									<input type="hidden" id="import_data" name="canon_options_advanced[import_data]" value="">

									<input type="button" class="button button_generate_data" value="Generate settings data" data-export_data='<?php echo esc_attr($encoded_serialized_options_data); ?>' />
									<button id="button_import_data" name="button_import_data" class="button-secondary"><?php esc_html_e("Import settings data", "loc_canon_venuex"); ?></button>
								</td>

								<td class="float-right">
									<select class="predefined-data-select">
							     		<option value="" selected='selected'><?php esc_html_e('Load predefined settings data...', 'loc_canon_venuex'); ?></option> 
							     		
							     		
							     		<option value='{¤(dq)¤canon_options¤(dq)¤:{¤(dq)¤reset_all¤(dq)¤:¤(dq)¤¤(dq)¤,¤(dq)¤reset_basic¤(dq)¤:¤(dq)¤¤(dq)¤,¤(dq)¤use_responsive_design¤(dq)¤:¤(dq)¤checked¤(dq)¤,¤(dq)¤use_boxed_design¤(dq)¤:¤(dq)¤unchecked¤(dq)¤,¤(dq)¤use_construction_mode¤(dq)¤:¤(dq)¤unchecked¤(dq)¤,¤(dq)¤construction_msg¤(dq)¤:¤(dq)¤This site is under construction!¤(dq)¤,¤(dq)¤sidebars_alignment¤(dq)¤:¤(dq)¤right¤(dq)¤,¤(dq)¤read_more_text¤(dq)¤:¤(dq)¤Read More¤(dq)¤,¤(dq)¤overlay_header¤(dq)¤:¤(dq)¤checked¤(dq)¤,¤(dq)¤overlay_header_turn_off_width¤(dq)¤:¤(dq)¤768¤(dq)¤,¤(dq)¤dev_mode¤(dq)¤:¤(dq)¤unchecked¤(dq)¤,¤(dq)¤autocomplete_words¤(dq)¤:¤(dq)¤c++, jquery, I like jQuery, java, php, coldfusion, javascript, asp, ruby¤(dq)¤,¤(dq)¤hide_theme_meta_description¤(dq)¤:¤(dq)¤unchecked¤(dq)¤,¤(dq)¤hide_theme_og¤(dq)¤:¤(dq)¤unchecked¤(dq)¤,¤(dq)¤fontface_fix¤(dq)¤:¤(dq)¤unchecked¤(dq)¤,¤(dq)¤favicon_url¤(dq)¤:¤(dq)¤¤(dq)¤},¤(dq)¤canon_options_frame¤(dq)¤:{¤(dq)¤use_boxed_header¤(dq)¤:¤(dq)¤checked¤(dq)¤,¤(dq)¤use_sticky_preheader¤(dq)¤:¤(dq)¤unchecked¤(dq)¤,¤(dq)¤use_sticky_header¤(dq)¤:¤(dq)¤checked¤(dq)¤,¤(dq)¤use_sticky_postheader¤(dq)¤:¤(dq)¤unchecked¤(dq)¤,¤(dq)¤preheader_opacity¤(dq)¤:¤(dq)¤1¤(dq)¤,¤(dq)¤header_opacity¤(dq)¤:¤(dq)¤0¤(dq)¤,¤(dq)¤postheader_opacity¤(dq)¤:¤(dq)¤0.15¤(dq)¤,¤(dq)¤use_sticky_shadow¤(dq)¤:¤(dq)¤checked¤(dq)¤,¤(dq)¤sticky_turn_off_width¤(dq)¤:¤(dq)¤768¤(dq)¤,¤(dq)¤add_search_btn_to_primary¤(dq)¤:¤(dq)¤checked¤(dq)¤,¤(dq)¤add_search_btn_to_secondary¤(dq)¤:¤(dq)¤unchecked¤(dq)¤,¤(dq)¤status_of_scroll_to_menu_items¤(dq)¤:¤(dq)¤auto¤(dq)¤,¤(dq)¤pre_header_layout¤(dq)¤:¤(dq)¤pre_custom_left_right¤(dq)¤,¤(dq)¤pre_custom_center¤(dq)¤:¤(dq)¤off¤(dq)¤,¤(dq)¤pre_custom_left¤(dq)¤:¤(dq)¤text¤(dq)¤,¤(dq)¤pre_custom_right¤(dq)¤:¤(dq)¤secondary¤(dq)¤,¤(dq)¤main_header_layout¤(dq)¤:¤(dq)¤main_custom_left_right¤(dq)¤,¤(dq)¤main_custom_center¤(dq)¤:¤(dq)¤off¤(dq)¤,¤(dq)¤main_custom_left¤(dq)¤:¤(dq)¤logo¤(dq)¤,¤(dq)¤main_custom_right¤(dq)¤:¤(dq)¤primary¤(dq)¤,¤(dq)¤post_header_layout¤(dq)¤:¤(dq)¤off¤(dq)¤,¤(dq)¤post_custom_center¤(dq)¤:¤(dq)¤off¤(dq)¤,¤(dq)¤post_custom_left¤(dq)¤:¤(dq)¤social¤(dq)¤,¤(dq)¤post_custom_right¤(dq)¤:¤(dq)¤text¤(dq)¤,¤(dq)¤header_padding_top¤(dq)¤:¤(dq)¤12¤(dq)¤,¤(dq)¤header_padding_bottom¤(dq)¤:¤(dq)¤8¤(dq)¤,¤(dq)¤pos_left_element_top¤(dq)¤:¤(dq)¤0¤(dq)¤,¤(dq)¤pos_left_element_left¤(dq)¤:¤(dq)¤0¤(dq)¤,¤(dq)¤pos_right_element_top¤(dq)¤:¤(dq)¤30¤(dq)¤,¤(dq)¤pos_right_element_right¤(dq)¤:¤(dq)¤0¤(dq)¤,¤(dq)¤logo_url¤(dq)¤:¤(dq)¤http:¤(bs)¤/¤(bs)¤/themecanon.com¤(bs)¤/venuex¤(bs)¤/wp-content¤(bs)¤/uploads¤(bs)¤/2016¤(bs)¤/01¤(bs)¤/logo.png¤(dq)¤,¤(dq)¤logo_text¤(dq)¤:¤(dq)¤¤(dq)¤,¤(dq)¤logo_text_size¤(dq)¤:¤(dq)¤28¤(dq)¤,¤(dq)¤logo_max_width¤(dq)¤:¤(dq)¤100¤(dq)¤,¤(dq)¤header_img_homepage_only¤(dq)¤:¤(dq)¤unchecked¤(dq)¤,¤(dq)¤header_img_url¤(dq)¤:¤(dq)¤¤(dq)¤,¤(dq)¤header_img_bg_color¤(dq)¤:¤(dq)¤#141312¤(dq)¤,¤(dq)¤header_img_height¤(dq)¤:¤(dq)¤300¤(dq)¤,¤(dq)¤header_img_use_parallax¤(dq)¤:¤(dq)¤checked¤(dq)¤,¤(dq)¤header_img_parallax_ratio¤(dq)¤:¤(dq)¤0.5¤(dq)¤,¤(dq)¤header_img_text¤(dq)¤:¤(dq)¤¤(lt)¤h3¤(gt)¤Header Image With Parallax Scrolling - What¤(sq)¤s Not To Like!¤(lt)¤¤(bs)¤/h3¤(gt)¤[button]Buy VenueX Today[¤(bs)¤/button]¤(dq)¤,¤(dq)¤header_img_text_alignment¤(dq)¤:¤(dq)¤centered¤(dq)¤,¤(dq)¤header_img_text_margin_top¤(dq)¤:¤(dq)¤150¤(dq)¤,¤(dq)¤header_banner_code¤(dq)¤:¤(dq)¤¤(lt)¤a href=¤(sq)¤http:¤(bs)¤/¤(bs)¤/www.themeforest.com¤(bs)¤/?ref=themecanon¤(sq)¤ target=¤(sq)¤_blank¤(sq)¤¤(gt)¤¤(lt)¤img src=¤(sq)¤http:¤(bs)¤/¤(bs)¤/localhost:8888¤(bs)¤/20160104-venuex¤(bs)¤/wp-content¤(bs)¤/themes¤(bs)¤/venuex¤(bs)¤/img¤(bs)¤/banner_468x60.gif¤(sq)¤¤(gt)¤¤(lt)¤¤(bs)¤/a¤(gt)¤¤(dq)¤,¤(dq)¤header_text¤(dq)¤:¤(dq)¤¤(lt)¤a href=¤(bs)¤¤(dq)¤http:¤(bs)¤/¤(bs)¤/themecanon.com¤(bs)¤/venuex¤(bs)¤/event¤(bs)¤/the-culprits¤(bs)¤/¤(bs)¤¤(dq)¤¤(gt)¤Get Tickets ¤(lt)¤em class=¤(bs)¤¤(dq)¤fa fa-ticket¤(bs)¤¤(dq)¤¤(gt)¤¤(lt)¤¤(bs)¤/em¤(gt)¤¤(lt)¤¤(bs)¤/a¤(gt)¤¤(dq)¤,¤(dq)¤show_social_icons¤(dq)¤:¤(dq)¤checked¤(dq)¤,¤(dq)¤social_in_new¤(dq)¤:¤(dq)¤checked¤(dq)¤,¤(dq)¤social_links¤(dq)¤:[[¤(dq)¤fa-facebook-square¤(dq)¤,¤(dq)¤https:¤(bs)¤/¤(bs)¤/www.facebook.com¤(bs)¤/themecanon¤(dq)¤],[¤(dq)¤fa-twitter-square¤(dq)¤,¤(dq)¤https:¤(bs)¤/¤(bs)¤/twitter.com¤(bs)¤/ThemeCanon¤(dq)¤],[¤(dq)¤fa-rss-square¤(dq)¤,¤(dq)¤http:¤(bs)¤/¤(bs)¤/localhost:8888¤(bs)¤/20160104-venuex¤(bs)¤/feed¤(bs)¤/¤(dq)¤]],¤(dq)¤toolbar_search_button¤(dq)¤:¤(dq)¤checked¤(dq)¤,¤(dq)¤countdown_datetime_string¤(dq)¤:¤(dq)¤December 31, 2023 23:59:59¤(dq)¤,¤(dq)¤countdown_gmt_offset¤(dq)¤:¤(dq)¤+10¤(dq)¤,¤(dq)¤countdown_description¤(dq)¤:¤(dq)¤Next Event: ¤(dq)¤,¤(dq)¤show_pre_footer¤(dq)¤:¤(dq)¤unchecked¤(dq)¤,¤(dq)¤show_main_footer¤(dq)¤:¤(dq)¤checked¤(dq)¤,¤(dq)¤show_post_footer¤(dq)¤:¤(dq)¤checked¤(dq)¤,¤(dq)¤footer_text¤(dq)¤:¤(dq)¤¤(bs)¤u00a9 Copyright VenueX by ¤(lt)¤a href=¤(bs)¤¤(dq)¤http:¤(bs)¤/¤(bs)¤/www.themecanon.com¤(bs)¤¤(dq)¤ target=¤(bs)¤¤(dq)¤_blank¤(bs)¤¤(dq)¤¤(gt)¤Theme Canon¤(lt)¤¤(bs)¤/a¤(gt)¤¤(dq)¤,¤(dq)¤widgetized_footer_layout¤(dq)¤:¤(dq)¤fourth_fourth_fourth_fourth¤(dq)¤,¤(dq)¤widget_footer_widget_area_1¤(dq)¤:¤(dq)¤canon_cwa_footer-widget-area-1¤(dq)¤,¤(dq)¤widget_footer_widget_area_2¤(dq)¤:¤(dq)¤canon_cwa_footer-widget-area-2¤(dq)¤,¤(dq)¤widget_footer_widget_area_3¤(dq)¤:¤(dq)¤canon_cwa_footer-widget-area-3¤(dq)¤,¤(dq)¤widget_footer_widget_area_4¤(dq)¤:¤(dq)¤canon_cwa_footer-widget-area-4¤(dq)¤,¤(dq)¤widget_footer_widget_area_5¤(dq)¤:¤(dq)¤canon_cwa_footer-widget-area-5¤(dq)¤},¤(dq)¤canon_options_post¤(dq)¤:{¤(dq)¤page_show_comments¤(dq)¤:¤(dq)¤unchecked¤(dq)¤,¤(dq)¤show_tags¤(dq)¤:¤(dq)¤checked¤(dq)¤,¤(dq)¤show_comments¤(dq)¤:¤(dq)¤checked¤(dq)¤,¤(dq)¤show_post_nav¤(dq)¤:¤(dq)¤checked¤(dq)¤,¤(dq)¤post_nav_same_cat¤(dq)¤:¤(dq)¤checked¤(dq)¤,¤(dq)¤show_person_position¤(dq)¤:¤(dq)¤checked¤(dq)¤,¤(dq)¤show_person_info¤(dq)¤:¤(dq)¤checked¤(dq)¤,¤(dq)¤show_person_nav¤(dq)¤:¤(dq)¤checked¤(dq)¤,¤(dq)¤person_nav_same_cat¤(dq)¤:¤(dq)¤unchecked¤(dq)¤,¤(dq)¤show_meta_author¤(dq)¤:¤(dq)¤checked¤(dq)¤,¤(dq)¤show_meta_date¤(dq)¤:¤(dq)¤checked¤(dq)¤,¤(dq)¤show_meta_comments¤(dq)¤:¤(dq)¤checked¤(dq)¤,¤(dq)¤show_meta_categories¤(dq)¤:¤(dq)¤checked¤(dq)¤,¤(dq)¤blog_layout¤(dq)¤:¤(dq)¤sidebar¤(dq)¤,¤(dq)¤blog_sidebar¤(dq)¤:¤(dq)¤canon_archive_sidebar_widget_area¤(dq)¤,¤(dq)¤blog_excerpt_length¤(dq)¤:¤(dq)¤345¤(dq)¤,¤(dq)¤cat_layout¤(dq)¤:¤(dq)¤sidebar¤(dq)¤,¤(dq)¤cat_sidebar¤(dq)¤:¤(dq)¤canon_archive_sidebar_widget_area¤(dq)¤,¤(dq)¤cat_excerpt_length¤(dq)¤:¤(dq)¤345¤(dq)¤,¤(dq)¤show_cat_title¤(dq)¤:¤(dq)¤checked¤(dq)¤,¤(dq)¤show_cat_description¤(dq)¤:¤(dq)¤checked¤(dq)¤,¤(dq)¤archive_layout¤(dq)¤:¤(dq)¤sidebar¤(dq)¤,¤(dq)¤archive_sidebar¤(dq)¤:¤(dq)¤canon_archive_sidebar_widget_area¤(dq)¤,¤(dq)¤archive_excerpt_length¤(dq)¤:¤(dq)¤330¤(dq)¤,¤(dq)¤search_box_text¤(dq)¤:¤(dq)¤What are you looking for?¤(dq)¤,¤(dq)¤search_posts¤(dq)¤:¤(dq)¤checked¤(dq)¤,¤(dq)¤search_pages¤(dq)¤:¤(dq)¤checked¤(dq)¤,¤(dq)¤search_cpt¤(dq)¤:¤(dq)¤checked¤(dq)¤,¤(dq)¤search_cpt_source¤(dq)¤:¤(dq)¤cpt_people, cpt_project¤(dq)¤,¤(dq)¤widgetized_search_layout¤(dq)¤:¤(dq)¤third_third_third¤(dq)¤,¤(dq)¤search_widget_area_1¤(dq)¤:¤(dq)¤canon_cwa_search-3¤(dq)¤,¤(dq)¤search_widget_area_2¤(dq)¤:¤(dq)¤canon_cwa_search-1¤(dq)¤,¤(dq)¤search_widget_area_3¤(dq)¤:¤(dq)¤canon_cwa_search-2¤(dq)¤,¤(dq)¤search_widget_area_4¤(dq)¤:¤(dq)¤canon_cwa_custom-widget-area-4¤(dq)¤,¤(dq)¤search_widget_area_5¤(dq)¤:¤(dq)¤canon_cwa_custom-widget-area-5¤(dq)¤,¤(dq)¤404_layout¤(dq)¤:¤(dq)¤full¤(dq)¤,¤(dq)¤404_sidebar¤(dq)¤:¤(dq)¤canon_page_sidebar_widget_area¤(dq)¤,¤(dq)¤404_title¤(dq)¤:¤(dq)¤Page not found¤(dq)¤,¤(dq)¤404_msg¤(dq)¤:¤(dq)¤Sorry, you¤(sq)¤re lost my friend, the page you¤(sq)¤re looking for does not exist anymore. Take your luck at searching for a new one.¤(dq)¤,¤(dq)¤currency_symbol¤(dq)¤:¤(dq)¤$¤(dq)¤,¤(dq)¤currency_symbol_pos¤(dq)¤:¤(dq)¤prepend¤(dq)¤,¤(dq)¤revslider_clean_ui¤(dq)¤:¤(dq)¤checked¤(dq)¤,¤(dq)¤use_woocommerce_sidebar¤(dq)¤:¤(dq)¤checked¤(dq)¤,¤(dq)¤woocommerce_sidebar¤(dq)¤:¤(dq)¤canon_page_sidebar_widget_area¤(dq)¤,¤(dq)¤use_events_sidebar¤(dq)¤:¤(dq)¤unchecked¤(dq)¤,¤(dq)¤events_sidebar¤(dq)¤:¤(dq)¤canon_page_sidebar_widget_area¤(dq)¤,¤(dq)¤events_slider¤(dq)¤:¤(dq)¤feature-strip¤(dq)¤,¤(dq)¤optimize_events_settings¤(dq)¤:¤(dq)¤checked¤(dq)¤},¤(dq)¤canon_options_appearance¤(dq)¤:{¤(dq)¤body_skin_class¤(dq)¤:¤(dq)¤tc-venuex-1¤(dq)¤,¤(dq)¤color_body¤(dq)¤:¤(dq)¤#f9f9f9¤(dq)¤,¤(dq)¤color_plate¤(dq)¤:¤(dq)¤#ffffff¤(dq)¤,¤(dq)¤color_general_text¤(dq)¤:¤(dq)¤#222425¤(dq)¤,¤(dq)¤color_links¤(dq)¤:¤(dq)¤#222425¤(dq)¤,¤(dq)¤color_links_hover¤(dq)¤:¤(dq)¤#ff4200¤(dq)¤,¤(dq)¤color_headings¤(dq)¤:¤(dq)¤#222425¤(dq)¤,¤(dq)¤color_text_meta¤(dq)¤:¤(dq)¤#c7c7c7¤(dq)¤,¤(dq)¤color_text_logo¤(dq)¤:¤(dq)¤#ffffff¤(dq)¤,¤(dq)¤color_feat_text_1¤(dq)¤:¤(dq)¤#ff4200¤(dq)¤,¤(dq)¤color_feat_text_2¤(dq)¤:¤(dq)¤#ff4200¤(dq)¤,¤(dq)¤color_white_text¤(dq)¤:¤(dq)¤#ffffff¤(dq)¤,¤(dq)¤color_preheader_bg¤(dq)¤:¤(dq)¤#000000¤(dq)¤,¤(dq)¤color_preheader¤(dq)¤:¤(dq)¤#ffffff¤(dq)¤,¤(dq)¤color_preheader_hover¤(dq)¤:¤(dq)¤#ff4200¤(dq)¤,¤(dq)¤color_header_bg¤(dq)¤:¤(dq)¤#23282d¤(dq)¤,¤(dq)¤color_header¤(dq)¤:¤(dq)¤#ffffff¤(dq)¤,¤(dq)¤color_header_hover¤(dq)¤:¤(dq)¤#ff4200¤(dq)¤,¤(dq)¤color_postheader_bg¤(dq)¤:¤(dq)¤#000000¤(dq)¤,¤(dq)¤color_postheader¤(dq)¤:¤(dq)¤#ffffff¤(dq)¤,¤(dq)¤color_postheader_hover¤(dq)¤:¤(dq)¤#ff4200¤(dq)¤,¤(dq)¤color_third_prenav¤(dq)¤:¤(dq)¤#111111¤(dq)¤,¤(dq)¤color_third_nav¤(dq)¤:¤(dq)¤#111111¤(dq)¤,¤(dq)¤color_third_postnav¤(dq)¤:¤(dq)¤#111111¤(dq)¤,¤(dq)¤color_search_bg¤(dq)¤:¤(dq)¤#1f2327¤(dq)¤,¤(dq)¤color_search_text¤(dq)¤:¤(dq)¤#ffffff¤(dq)¤,¤(dq)¤color_search_text_hover¤(dq)¤:¤(dq)¤#ff4200¤(dq)¤,¤(dq)¤color_search_line¤(dq)¤:¤(dq)¤#464d51¤(dq)¤,¤(dq)¤color_sidr_bg¤(dq)¤:¤(dq)¤#111111¤(dq)¤,¤(dq)¤color_sidr¤(dq)¤:¤(dq)¤#ffffff¤(dq)¤,¤(dq)¤color_sidr_hover¤(dq)¤:¤(dq)¤#ff4200¤(dq)¤,¤(dq)¤color_sidr_line¤(dq)¤:¤(dq)¤#343434¤(dq)¤,¤(dq)¤color_btn¤(dq)¤:¤(dq)¤#ff4200¤(dq)¤,¤(dq)¤color_btn_hover¤(dq)¤:¤(dq)¤#c73300¤(dq)¤,¤(dq)¤color_btn_text¤(dq)¤:¤(dq)¤#ffffff¤(dq)¤,¤(dq)¤color_btn_text_hover¤(dq)¤:¤(dq)¤#ffffff¤(dq)¤,¤(dq)¤color_btn_2¤(dq)¤:¤(dq)¤#ff4200¤(dq)¤,¤(dq)¤color_btn_2_hover¤(dq)¤:¤(dq)¤#e5e5e5¤(dq)¤,¤(dq)¤color_btn_2_text¤(dq)¤:¤(dq)¤#ffffff¤(dq)¤,¤(dq)¤color_btn_2_text_hover¤(dq)¤:¤(dq)¤#000000¤(dq)¤,¤(dq)¤color_feat_block_1¤(dq)¤:¤(dq)¤#efefef¤(dq)¤,¤(dq)¤color_lite_block¤(dq)¤:¤(dq)¤#f2f2f2¤(dq)¤,¤(dq)¤color_form_fields_bg¤(dq)¤:¤(dq)¤#f6f6f6¤(dq)¤,¤(dq)¤color_form_fields_text¤(dq)¤:¤(dq)¤#666666¤(dq)¤,¤(dq)¤color_lines¤(dq)¤:¤(dq)¤#e3e3e3¤(dq)¤,¤(dq)¤color_footer_block¤(dq)¤:¤(dq)¤#212425¤(dq)¤,¤(dq)¤color_footer_headings¤(dq)¤:¤(dq)¤#ffffff¤(dq)¤,¤(dq)¤color_footer_text¤(dq)¤:¤(dq)¤#ebebeb¤(dq)¤,¤(dq)¤color_footer_text_hover¤(dq)¤:¤(dq)¤#ff4200¤(dq)¤,¤(dq)¤color_footlines¤(dq)¤:¤(dq)¤#373b3c¤(dq)¤,¤(dq)¤color_footer_button¤(dq)¤:¤(dq)¤#ff4200¤(dq)¤,¤(dq)¤color_footer_form_fields_bg¤(dq)¤:¤(dq)¤#191b1c¤(dq)¤,¤(dq)¤color_footer_form_fields_text¤(dq)¤:¤(dq)¤#bebebe¤(dq)¤,¤(dq)¤color_footer_alt_block¤(dq)¤:¤(dq)¤#232627¤(dq)¤,¤(dq)¤color_footer_base¤(dq)¤:¤(dq)¤#000000¤(dq)¤,¤(dq)¤color_footer_base_text¤(dq)¤:¤(dq)¤#ffffff¤(dq)¤,¤(dq)¤color_footer_base_text_hover¤(dq)¤:¤(dq)¤#ff4200¤(dq)¤,¤(dq)¤bg_img_url¤(dq)¤:¤(dq)¤¤(dq)¤,¤(dq)¤bg_link¤(dq)¤:¤(dq)¤¤(dq)¤,¤(dq)¤bg_repeat¤(dq)¤:¤(dq)¤repeat¤(dq)¤,¤(dq)¤bg_attachment¤(dq)¤:¤(dq)¤fixed¤(dq)¤,¤(dq)¤lightbox_overlay_color¤(dq)¤:¤(dq)¤#000000¤(dq)¤,¤(dq)¤lightbox_overlay_opacity¤(dq)¤:¤(dq)¤0.7¤(dq)¤,¤(dq)¤font_main¤(dq)¤:[¤(dq)¤canon_default¤(dq)¤,¤(dq)¤¤(dq)¤,¤(dq)¤¤(dq)¤],¤(dq)¤font_meta¤(dq)¤:[¤(dq)¤canon_default¤(dq)¤,¤(dq)¤¤(dq)¤,¤(dq)¤¤(dq)¤],¤(dq)¤font_quote¤(dq)¤:[¤(dq)¤canon_default¤(dq)¤,¤(dq)¤¤(dq)¤,¤(dq)¤¤(dq)¤],¤(dq)¤font_lead¤(dq)¤:[¤(dq)¤canon_default¤(dq)¤,¤(dq)¤¤(dq)¤,¤(dq)¤¤(dq)¤],¤(dq)¤font_logotext¤(dq)¤:[¤(dq)¤canon_default¤(dq)¤,¤(dq)¤¤(dq)¤,¤(dq)¤¤(dq)¤],¤(dq)¤font_bold¤(dq)¤:[¤(dq)¤canon_default¤(dq)¤,¤(dq)¤¤(dq)¤,¤(dq)¤¤(dq)¤],¤(dq)¤font_button¤(dq)¤:[¤(dq)¤canon_default¤(dq)¤,¤(dq)¤¤(dq)¤,¤(dq)¤¤(dq)¤],¤(dq)¤font_italic¤(dq)¤:[¤(dq)¤canon_default¤(dq)¤,¤(dq)¤¤(dq)¤,¤(dq)¤¤(dq)¤],¤(dq)¤font_heading¤(dq)¤:[¤(dq)¤canon_default¤(dq)¤,¤(dq)¤¤(dq)¤,¤(dq)¤¤(dq)¤],¤(dq)¤font_heading2¤(dq)¤:[¤(dq)¤canon_default¤(dq)¤,¤(dq)¤¤(dq)¤,¤(dq)¤¤(dq)¤],¤(dq)¤font_nav¤(dq)¤:[¤(dq)¤canon_default¤(dq)¤,¤(dq)¤¤(dq)¤,¤(dq)¤¤(dq)¤],¤(dq)¤font_widget_footer¤(dq)¤:[¤(dq)¤canon_default¤(dq)¤,¤(dq)¤¤(dq)¤,¤(dq)¤¤(dq)¤],¤(dq)¤font_size_root¤(dq)¤:100,¤(dq)¤anim_img_slider_slideshow¤(dq)¤:¤(dq)¤unchecked¤(dq)¤,¤(dq)¤anim_img_slider_delay¤(dq)¤:¤(dq)¤5000¤(dq)¤,¤(dq)¤anim_img_slider_anim_duration¤(dq)¤:¤(dq)¤800¤(dq)¤,¤(dq)¤anim_quote_slider_slideshow¤(dq)¤:¤(dq)¤unchecked¤(dq)¤,¤(dq)¤anim_quote_slider_delay¤(dq)¤:¤(dq)¤5000¤(dq)¤,¤(dq)¤anim_quote_slider_anim_duration¤(dq)¤:¤(dq)¤800¤(dq)¤,¤(dq)¤anim_menu_slider_slideshow¤(dq)¤:¤(dq)¤unchecked¤(dq)¤,¤(dq)¤anim_menu_slider_delay¤(dq)¤:¤(dq)¤5000¤(dq)¤,¤(dq)¤anim_menu_slider_anim_duration¤(dq)¤:¤(dq)¤800¤(dq)¤,¤(dq)¤lazy_load_on_pagebuilder_blocks¤(dq)¤:¤(dq)¤unchecked¤(dq)¤,¤(dq)¤lazy_load_on_blog¤(dq)¤:¤(dq)¤unchecked¤(dq)¤,¤(dq)¤lazy_load_on_widgets¤(dq)¤:¤(dq)¤unchecked¤(dq)¤,¤(dq)¤lazy_load_after¤(dq)¤:0.3,¤(dq)¤lazy_load_enter¤(dq)¤:¤(dq)¤bottom¤(dq)¤,¤(dq)¤lazy_load_move¤(dq)¤:24,¤(dq)¤lazy_load_over¤(dq)¤:0.56,¤(dq)¤lazy_load_viewport_factor¤(dq)¤:0.2,¤(dq)¤anim_menus¤(dq)¤:¤(dq)¤anim_menus_off¤(dq)¤,¤(dq)¤anim_menus_enter¤(dq)¤:¤(dq)¤left¤(dq)¤,¤(dq)¤anim_menus_move¤(dq)¤:40,¤(dq)¤anim_menus_duration¤(dq)¤:600,¤(dq)¤anim_menus_delay¤(dq)¤:150},¤(dq)¤canon_options_advanced¤(dq)¤:{¤(dq)¤custom_widget_areas¤(dq)¤:{¤(dq)¤9999¤(dq)¤:{¤(dq)¤name¤(dq)¤:¤(dq)¤¤(dq)¤},¤(dq)¤0¤(dq)¤:{¤(dq)¤name¤(dq)¤:¤(dq)¤Custom Widget Area 1¤(dq)¤},¤(dq)¤1¤(dq)¤:{¤(dq)¤name¤(dq)¤:¤(dq)¤Custom Widget Area 2¤(dq)¤},¤(dq)¤2¤(dq)¤:{¤(dq)¤name¤(dq)¤:¤(dq)¤Custom Widget Area 3¤(dq)¤},¤(dq)¤3¤(dq)¤:{¤(dq)¤name¤(dq)¤:¤(dq)¤Custom Widget Area 4¤(dq)¤},¤(dq)¤4¤(dq)¤:{¤(dq)¤name¤(dq)¤:¤(dq)¤Custom Widget Area 5¤(dq)¤},¤(dq)¤5¤(dq)¤:{¤(dq)¤name¤(dq)¤:¤(dq)¤Footer Widget Area 1¤(dq)¤},¤(dq)¤6¤(dq)¤:{¤(dq)¤name¤(dq)¤:¤(dq)¤Footer Widget Area 2¤(dq)¤},¤(dq)¤7¤(dq)¤:{¤(dq)¤name¤(dq)¤:¤(dq)¤Footer Widget Area 3¤(dq)¤},¤(dq)¤8¤(dq)¤:{¤(dq)¤name¤(dq)¤:¤(dq)¤Footer Widget Area 4¤(dq)¤},¤(dq)¤9¤(dq)¤:{¤(dq)¤name¤(dq)¤:¤(dq)¤Footer Widget Area 5¤(dq)¤},¤(dq)¤10¤(dq)¤:{¤(dq)¤name¤(dq)¤:¤(dq)¤Search 1¤(dq)¤},¤(dq)¤11¤(dq)¤:{¤(dq)¤name¤(dq)¤:¤(dq)¤Search 2¤(dq)¤},¤(dq)¤12¤(dq)¤:{¤(dq)¤name¤(dq)¤:¤(dq)¤Search 3¤(dq)¤},¤(dq)¤13¤(dq)¤:{¤(dq)¤name¤(dq)¤:¤(dq)¤About Sidebar¤(dq)¤}},¤(dq)¤use_final_call_css¤(dq)¤:¤(dq)¤checked¤(dq)¤,¤(dq)¤final_call_css¤(dq)¤:¤(dq)¤.tribe-events-list h2.tribe-events-page-title{¤(bs)¤r¤(bs)¤n    display: none;¤(bs)¤r¤(bs)¤n}¤(dq)¤,¤(dq)¤canon_options_data¤(dq)¤:¤(dq)¤¤(dq)¤,¤(dq)¤import_data¤(dq)¤:¤(dq)¤¤(dq)¤,¤(dq)¤canon_widgets_data¤(dq)¤:¤(dq)¤¤(dq)¤,¤(dq)¤import_widgets_data¤(dq)¤:¤(dq)¤¤(dq)¤,¤(dq)¤reset_basic¤(dq)¤:¤(dq)¤¤(dq)¤,¤(dq)¤reset_all¤(dq)¤:¤(dq)¤¤(dq)¤}}'>
							     		  	<?php esc_html_e('VenueX Demo Settings', 'loc_canon_venuex'); ?></option>
									</select> 
								</td>
							</tr>

						</table>


					<!-- 
					--------------------------------------------------------------------------
						IMPORT/EXPORT WIDGETS
				    -------------------------------------------------------------------------- 
					-->

						<h3><?php esc_html_e("Import/Export Widgets", "loc_canon_venuex"); ?> <img src="<?php echo get_template_directory_uri() . '/img/help.png' ?>"></h3>

						<div class='contextual-help'>
							<?php 

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Import/Export widgets', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Use this section to import/export your widgets.', 'loc_canon_venuex'),
										' ',
										wp_kses_post(__('<strong>WARNING</strong>: Existing widgets and widget settings will be lost! ', 'loc_canon_venuex')),
										' ',
										esc_html__('The Widget Areas Manager which is used to create custom widget areas is part of the theme settings so please notice that custom widget areas are imported/exported along with the rest of the theme settings and NOT as part of the widgets import/export function. As widgets can only be imported into custom widget areas that already exist you may want to import your theme settings first to make sure that the required custom widget areas exist when importing your widgets.', 'loc_canon_venuex'),
									),
								)); 

								fw_option_help(array(
									'type'					=> 'ul',
									'title' 				=> esc_html__('Generate widgets data', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Clicking this button will generate widgets data. You can copy this data from the widgets data window.', 'loc_canon_venuex'),
										esc_html__('Clicking the window will select all text.', 'loc_canon_venuex'),
										esc_html__('Press CTRL+C on your keyboard or right click selected text and select copy.', 'loc_canon_venuex'),
										esc_html__('Once you have copied the data you can either save it to a text document/file (safest) or simply keep the data in your copy/paste clipboard (not safe).', 'loc_canon_venuex'),
									),
								)); 

								fw_option_help(array(
									'type'					=> 'ul',
									'title' 				=> esc_html__('Import widgets data', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Clicking this button will import your widgets data from the data string supplied in the widgets data window.', 'loc_canon_venuex'),
										esc_html__('Make sure you paste all of the data into the widgets data textarea/window. If part of the code is altered or left out import will fail.', 'loc_canon_venuex'),
										esc_html__('Click the "Import widgets data" button.', 'loc_canon_venuex'),
										esc_html__('Your widgets have now been imported.', 'loc_canon_venuex'),
									),
								)); 

								fw_option_help(array(
									'type'					=> 'ul',
									'title' 				=> esc_html__('Load predefined widgets data', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Use this select to load predefined widgets data into the data window.', 'loc_canon_venuex'),
										esc_html__('Click the "Import widgets data" button.', 'loc_canon_venuex'),
										esc_html__('The predefined widgets have now been imported.', 'loc_canon_venuex'),
									),
								)); 

							?>
						</div>

						<table class='form-table'>

							<tr valign='top'>
								<th scope='row'><?php esc_html_e("Widgets data", "loc_canon_venuex"); ?></th>
								<td colspan="2">
									<textarea id='canon_widgets_data' class='canon_export_data' name='canon_options_advanced[canon_widgets_data]' rows='5' cols='100'></textarea>
								</td>
							</tr>

							<tr valign='top'>
								<th scope='row'></th>
								<td>
									<input type="hidden" id="import_widgets_data" name="canon_options_advanced[import_widgets_data]" value="">

									<input type="button" class="button button_generate_data" value="Generate widgets data" data-export_data='<?php echo esc_attr($encoded_serialized_widgets_data); ?>' />
									<button id="button_import_widgets_data" name="button_import_widgets_data" class="button-secondary"><?php esc_html_e("Import widgets data", "loc_canon_venuex"); ?></button>
								</td>
								<td class="float-right">
									<select class="predefined-data-select">
							     		<option value="" selected='selected'><?php esc_html_e('Load predefined widgets data...', 'loc_canon_venuex'); ?></option> 
							     		
							     		
							     		<option value='{¤(dq)¤widget_areas¤(dq)¤:{¤(dq)¤wp_inactive_widgets¤(dq)¤:[],¤(dq)¤canon_archive_sidebar_widget_area¤(dq)¤:[¤(dq)¤venuex_social_links-2¤(dq)¤,¤(dq)¤venuex_more_posts-4¤(dq)¤,¤(dq)¤widget_venuex_facebook-2¤(dq)¤,¤(dq)¤tribe-events-list-widget-2¤(dq)¤,¤(dq)¤recent-comments-2¤(dq)¤],¤(dq)¤canon_page_sidebar_widget_area¤(dq)¤:[¤(dq)¤venuex_social_links-3¤(dq)¤,¤(dq)¤venuex_contact_list-3¤(dq)¤],¤(dq)¤canon_cwa_custom-widget-area-1¤(dq)¤:[],¤(dq)¤canon_cwa_custom-widget-area-2¤(dq)¤:[],¤(dq)¤canon_cwa_custom-widget-area-3¤(dq)¤:[],¤(dq)¤canon_cwa_custom-widget-area-4¤(dq)¤:[],¤(dq)¤canon_cwa_custom-widget-area-5¤(dq)¤:[],¤(dq)¤canon_cwa_footer-widget-area-1¤(dq)¤:[¤(dq)¤text-3¤(dq)¤],¤(dq)¤canon_cwa_footer-widget-area-2¤(dq)¤:[¤(dq)¤nav_menu-2¤(dq)¤],¤(dq)¤canon_cwa_footer-widget-area-3¤(dq)¤:[¤(dq)¤venuex_twitter-2¤(dq)¤],¤(dq)¤canon_cwa_footer-widget-area-4¤(dq)¤:[¤(dq)¤venuex_more_posts-3¤(dq)¤],¤(dq)¤canon_cwa_footer-widget-area-5¤(dq)¤:[],¤(dq)¤canon_cwa_search-1¤(dq)¤:[¤(dq)¤venuex_more_posts-6¤(dq)¤],¤(dq)¤canon_cwa_search-2¤(dq)¤:[¤(dq)¤venuex_twitter-3¤(dq)¤],¤(dq)¤canon_cwa_search-3¤(dq)¤:[¤(dq)¤tribe-events-list-widget-4¤(dq)¤],¤(dq)¤canon_cwa_about-sidebar¤(dq)¤:[¤(dq)¤venuex_social_links-4¤(dq)¤,¤(dq)¤venuex_more_posts-5¤(dq)¤,¤(dq)¤tribe-events-list-widget-3¤(dq)¤,¤(dq)¤widget_venuex_facebook-3¤(dq)¤],¤(dq)¤array_version¤(dq)¤:3},¤(dq)¤active_widgets¤(dq)¤:{¤(dq)¤venuex_social_links¤(dq)¤:{¤(dq)¤2¤(dq)¤:{¤(dq)¤title¤(dq)¤:¤(dq)¤Follow Us¤(dq)¤,¤(dq)¤display_style¤(dq)¤:¤(dq)¤square¤(dq)¤,¤(dq)¤open_in_new¤(dq)¤:¤(dq)¤checked¤(dq)¤,¤(dq)¤social_links¤(dq)¤:[[¤(dq)¤fa-facebook¤(dq)¤,¤(dq)¤https:¤(bs)¤/¤(bs)¤/www.facebook.com¤(bs)¤/themecanon¤(dq)¤],[¤(dq)¤fa-twitter¤(dq)¤,¤(dq)¤https:¤(bs)¤/¤(bs)¤/twitter.com¤(bs)¤/ThemeCanon¤(dq)¤],[¤(dq)¤fa-google-plus¤(dq)¤,¤(dq)¤#¤(dq)¤],[¤(dq)¤fa-youtube¤(dq)¤,¤(dq)¤#¤(dq)¤],[¤(dq)¤fa-rss¤(dq)¤,¤(dq)¤http:¤(bs)¤/¤(bs)¤/themecanon.com¤(bs)¤/venuex¤(bs)¤/feed¤(bs)¤/¤(dq)¤]]},¤(dq)¤3¤(dq)¤:{¤(dq)¤title¤(dq)¤:¤(dq)¤Follow Us¤(dq)¤,¤(dq)¤display_style¤(dq)¤:¤(dq)¤square¤(dq)¤,¤(dq)¤open_in_new¤(dq)¤:¤(dq)¤checked¤(dq)¤,¤(dq)¤social_links¤(dq)¤:[[¤(dq)¤fa-facebook¤(dq)¤,¤(dq)¤https:¤(bs)¤/¤(bs)¤/www.facebook.com¤(bs)¤/themecanon¤(dq)¤],[¤(dq)¤fa-twitter¤(dq)¤,¤(dq)¤https:¤(bs)¤/¤(bs)¤/twitter.com¤(bs)¤/ThemeCanon¤(dq)¤],[¤(dq)¤fa-google-plus¤(dq)¤,¤(dq)¤#¤(dq)¤],[¤(dq)¤fa-youtube¤(dq)¤,¤(dq)¤#¤(dq)¤],[¤(dq)¤fa-rss¤(dq)¤,¤(dq)¤http:¤(bs)¤/¤(bs)¤/themecanon.com¤(bs)¤/venuex¤(bs)¤/feed¤(bs)¤/¤(dq)¤]]},¤(dq)¤4¤(dq)¤:{¤(dq)¤title¤(dq)¤:¤(dq)¤Social Links¤(dq)¤,¤(dq)¤display_style¤(dq)¤:¤(dq)¤square¤(dq)¤,¤(dq)¤open_in_new¤(dq)¤:¤(dq)¤checked¤(dq)¤,¤(dq)¤social_links¤(dq)¤:[[¤(dq)¤fa-facebook¤(dq)¤,¤(dq)¤https:¤(bs)¤/¤(bs)¤/www.facebook.com¤(bs)¤/themecanon¤(dq)¤],[¤(dq)¤fa-twitter¤(dq)¤,¤(dq)¤https:¤(bs)¤/¤(bs)¤/twitter.com¤(bs)¤/ThemeCanon¤(dq)¤],[¤(dq)¤fa-google-plus¤(dq)¤,¤(dq)¤#¤(dq)¤],[¤(dq)¤fa-youtube¤(dq)¤,¤(dq)¤#¤(dq)¤],[¤(dq)¤fa-rss¤(dq)¤,¤(dq)¤http:¤(bs)¤/¤(bs)¤/themecanon.com¤(bs)¤/venuex¤(bs)¤/feed¤(bs)¤/¤(dq)¤]]},¤(dq)¤_multiwidget¤(dq)¤:1},¤(dq)¤venuex_more_posts¤(dq)¤:{¤(dq)¤3¤(dq)¤:{¤(dq)¤title¤(dq)¤:¤(dq)¤Quick Pics¤(dq)¤,¤(dq)¤show¤(dq)¤:¤(dq)¤latest_posts¤(dq)¤,¤(dq)¤display_style¤(dq)¤:¤(dq)¤images_to_lightbox¤(dq)¤,¤(dq)¤num_posts¤(dq)¤:¤(dq)¤6¤(dq)¤,¤(dq)¤num_columns¤(dq)¤:¤(dq)¤3¤(dq)¤},¤(dq)¤4¤(dq)¤:{¤(dq)¤title¤(dq)¤:¤(dq)¤Recent Posts¤(dq)¤,¤(dq)¤show¤(dq)¤:¤(dq)¤latest_posts¤(dq)¤,¤(dq)¤display_style¤(dq)¤:¤(dq)¤thumbnails_list¤(dq)¤,¤(dq)¤num_posts¤(dq)¤:¤(dq)¤3¤(dq)¤,¤(dq)¤num_columns¤(dq)¤:¤(dq)¤2¤(dq)¤},¤(dq)¤5¤(dq)¤:{¤(dq)¤title¤(dq)¤:¤(dq)¤Quick Pics¤(dq)¤,¤(dq)¤show¤(dq)¤:¤(dq)¤latest_posts¤(dq)¤,¤(dq)¤display_style¤(dq)¤:¤(dq)¤images_to_lightbox¤(dq)¤,¤(dq)¤num_posts¤(dq)¤:¤(dq)¤6¤(dq)¤,¤(dq)¤num_columns¤(dq)¤:¤(dq)¤2¤(dq)¤},¤(dq)¤6¤(dq)¤:{¤(dq)¤title¤(dq)¤:¤(dq)¤Recent Posts¤(dq)¤,¤(dq)¤show¤(dq)¤:¤(dq)¤latest_posts¤(dq)¤,¤(dq)¤display_style¤(dq)¤:¤(dq)¤thumbnails_list¤(dq)¤,¤(dq)¤num_posts¤(dq)¤:¤(dq)¤3¤(dq)¤,¤(dq)¤num_columns¤(dq)¤:¤(dq)¤2¤(dq)¤},¤(dq)¤_multiwidget¤(dq)¤:1},¤(dq)¤widget_venuex_facebook¤(dq)¤:{¤(dq)¤2¤(dq)¤:{¤(dq)¤title¤(dq)¤:¤(dq)¤Facebook Feed¤(dq)¤,¤(dq)¤fb_page¤(dq)¤:¤(dq)¤https:¤(bs)¤/¤(bs)¤/www.facebook.com¤(bs)¤/themecanon¤(dq)¤,¤(dq)¤fb_width¤(dq)¤:¤(dq)¤600¤(dq)¤,¤(dq)¤fb_height¤(dq)¤:¤(dq)¤360¤(dq)¤,¤(dq)¤fb_cover¤(dq)¤:¤(dq)¤checked¤(dq)¤,¤(dq)¤fb_faces¤(dq)¤:¤(dq)¤checked¤(dq)¤,¤(dq)¤fb_posts¤(dq)¤:¤(dq)¤checked¤(dq)¤,¤(dq)¤fb_small_header¤(dq)¤:¤(dq)¤checked¤(dq)¤},¤(dq)¤3¤(dq)¤:{¤(dq)¤title¤(dq)¤:¤(dq)¤Facebook¤(dq)¤,¤(dq)¤fb_page¤(dq)¤:¤(dq)¤https:¤(bs)¤/¤(bs)¤/www.facebook.com¤(bs)¤/themecanon¤(dq)¤,¤(dq)¤fb_width¤(dq)¤:¤(dq)¤600¤(dq)¤,¤(dq)¤fb_height¤(dq)¤:¤(dq)¤360¤(dq)¤,¤(dq)¤fb_cover¤(dq)¤:¤(dq)¤checked¤(dq)¤,¤(dq)¤fb_faces¤(dq)¤:¤(dq)¤checked¤(dq)¤,¤(dq)¤fb_posts¤(dq)¤:¤(dq)¤checked¤(dq)¤,¤(dq)¤fb_small_header¤(dq)¤:¤(dq)¤checked¤(dq)¤},¤(dq)¤_multiwidget¤(dq)¤:1},¤(dq)¤tribe-events-list-widget¤(dq)¤:{¤(dq)¤2¤(dq)¤:{¤(dq)¤title¤(dq)¤:¤(dq)¤Upcoming Events¤(dq)¤,¤(dq)¤limit¤(dq)¤:¤(dq)¤3¤(dq)¤,¤(dq)¤no_upcoming_events¤(dq)¤:false},¤(dq)¤3¤(dq)¤:{¤(dq)¤title¤(dq)¤:¤(dq)¤Upcoming Events¤(dq)¤,¤(dq)¤limit¤(dq)¤:¤(dq)¤3¤(dq)¤,¤(dq)¤no_upcoming_events¤(dq)¤:false},¤(dq)¤4¤(dq)¤:{¤(dq)¤title¤(dq)¤:¤(dq)¤Upcoming Events¤(dq)¤,¤(dq)¤limit¤(dq)¤:¤(dq)¤2¤(dq)¤,¤(dq)¤no_upcoming_events¤(dq)¤:false},¤(dq)¤_multiwidget¤(dq)¤:1},¤(dq)¤recent-comments¤(dq)¤:{¤(dq)¤2¤(dq)¤:{¤(dq)¤title¤(dq)¤:¤(dq)¤¤(dq)¤,¤(dq)¤number¤(dq)¤:5},¤(dq)¤_multiwidget¤(dq)¤:1},¤(dq)¤venuex_contact_list¤(dq)¤:{¤(dq)¤3¤(dq)¤:{¤(dq)¤title¤(dq)¤:¤(dq)¤Find us at¤(dq)¤,¤(dq)¤content¤(dq)¤:¤(dq)¤¤(lt)¤ul¤(gt)¤¤(bs)¤r¤(bs)¤n    ¤(lt)¤li¤(gt)¤¤(lt)¤a href=¤(bs)¤¤(dq)¤#¤(bs)¤¤(dq)¤¤(gt)¤Facebook.com¤(bs)¤/venuex¤(lt)¤¤(bs)¤/a¤(gt)¤¤(lt)¤¤(bs)¤/li¤(gt)¤¤(bs)¤r¤(bs)¤n    ¤(lt)¤li¤(gt)¤¤(lt)¤a href=¤(bs)¤¤(dq)¤#¤(bs)¤¤(dq)¤¤(gt)¤Google.com¤(bs)¤/venuex¤(lt)¤¤(bs)¤/a¤(gt)¤¤(lt)¤¤(bs)¤/li¤(gt)¤¤(bs)¤r¤(bs)¤n    ¤(lt)¤li¤(gt)¤¤(lt)¤a href=¤(bs)¤¤(dq)¤#¤(bs)¤¤(dq)¤¤(gt)¤Twitter.com¤(bs)¤/venuex¤(lt)¤¤(bs)¤/a¤(gt)¤¤(lt)¤¤(bs)¤/li¤(gt)¤¤(bs)¤r¤(bs)¤n    ¤(lt)¤li¤(gt)¤PO Box 4356, Melbourne 4000¤(bs)¤r¤(bs)¤n    Victoria, Australia¤(lt)¤¤(bs)¤/li¤(gt)¤¤(bs)¤r¤(bs)¤n¤(lt)¤¤(bs)¤/ul¤(gt)¤ ¤(dq)¤},¤(dq)¤_multiwidget¤(dq)¤:1},¤(dq)¤text¤(dq)¤:{¤(dq)¤3¤(dq)¤:{¤(dq)¤title¤(dq)¤:¤(dq)¤¤(dq)¤,¤(dq)¤text¤(dq)¤:¤(dq)¤¤(lt)¤img class=¤(bs)¤¤(dq)¤wp-image-108¤(bs)¤¤(dq)¤ src=¤(bs)¤¤(dq)¤http:¤(bs)¤/¤(bs)¤/themecanon.com¤(bs)¤/venuex¤(bs)¤/wp-content¤(bs)¤/uploads¤(bs)¤/2016¤(bs)¤/01¤(bs)¤/foot-logo.png¤(bs)¤¤(dq)¤ alt=¤(bs)¤¤(dq)¤foot-logo¤(bs)¤¤(dq)¤ width=¤(bs)¤¤(dq)¤130¤(bs)¤¤(dq)¤ height=¤(bs)¤¤(dq)¤43¤(bs)¤¤(dq)¤ ¤(bs)¤/¤(gt)¤¤(bs)¤r¤(bs)¤n¤(bs)¤r¤(bs)¤n¤(lt)¤p¤(gt)¤Concert Hall One, 3005 Laneway Drive, Riverton, Brisbane, 40567, Australia.¤(lt)¤¤(bs)¤/p¤(gt)¤¤(bs)¤r¤(bs)¤n¤(bs)¤r¤(bs)¤n¤(lt)¤ul¤(gt)¤ ¤(bs)¤r¤(bs)¤n¤(lt)¤li¤(gt)¤¤(lt)¤em class=¤(bs)¤¤(dq)¤fa fa-phone¤(bs)¤¤(dq)¤¤(gt)¤¤(lt)¤¤(bs)¤/em¤(gt)¤ +61 4321 1234¤(lt)¤¤(bs)¤/li¤(gt)¤¤(bs)¤r¤(bs)¤n¤(lt)¤li¤(gt)¤¤(lt)¤em class=¤(bs)¤¤(dq)¤fa fa-envelope-o¤(bs)¤¤(dq)¤¤(gt)¤¤(lt)¤¤(bs)¤/em¤(gt)¤ info@venue.com¤(lt)¤¤(bs)¤/li¤(gt)¤¤(bs)¤r¤(bs)¤n¤(lt)¤¤(bs)¤/ul¤(gt)¤¤(dq)¤,¤(dq)¤filter¤(dq)¤:false},¤(dq)¤_multiwidget¤(dq)¤:1},¤(dq)¤nav_menu¤(dq)¤:{¤(dq)¤2¤(dq)¤:{¤(dq)¤title¤(dq)¤:¤(dq)¤Navigation¤(dq)¤,¤(dq)¤nav_menu¤(dq)¤:9},¤(dq)¤_multiwidget¤(dq)¤:1},¤(dq)¤venuex_twitter¤(dq)¤:{¤(dq)¤2¤(dq)¤:{¤(dq)¤title¤(dq)¤:¤(dq)¤Latest Tweet¤(dq)¤,¤(dq)¤twitter_widget_code¤(dq)¤:¤(dq)¤            ¤(lt)¤a class=¤(bs)¤¤(dq)¤twitter-timeline¤(bs)¤¤(dq)¤  href=¤(bs)¤¤(dq)¤https:¤(bs)¤/¤(bs)¤/twitter.com¤(bs)¤/makelemonadeco¤(bs)¤¤(dq)¤ data-widget-id=¤(bs)¤¤(dq)¤334632933006655488¤(bs)¤¤(dq)¤¤(gt)¤Tweets by @makelemonadeco¤(lt)¤¤(bs)¤/a¤(gt)¤¤(bs)¤r¤(bs)¤n            ¤(lt)¤script¤(gt)¤!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=¤(bs)¤/^http:¤(bs)¤/.test(d.location)?¤(sq)¤http¤(sq)¤:¤(sq)¤https¤(sq)¤;if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+¤(bs)¤¤(dq)¤:¤(bs)¤/¤(bs)¤/platform.twitter.com¤(bs)¤/widgets.js¤(bs)¤¤(dq)¤;fjs.parentNode.insertBefore(js,fjs);}}(document,¤(bs)¤¤(dq)¤script¤(bs)¤¤(dq)¤,¤(bs)¤¤(dq)¤twitter-wjs¤(bs)¤¤(dq)¤);¤(lt)¤¤(bs)¤/script¤(gt)¤¤(bs)¤r¤(bs)¤n          ¤(dq)¤,¤(dq)¤use_theme_design¤(dq)¤:¤(dq)¤checked¤(dq)¤,¤(dq)¤twitter_num_tweets¤(dq)¤:¤(dq)¤1¤(dq)¤},¤(dq)¤3¤(dq)¤:{¤(dq)¤title¤(dq)¤:¤(dq)¤Latest tweet¤(dq)¤,¤(dq)¤twitter_widget_code¤(dq)¤:¤(dq)¤            ¤(lt)¤a class=¤(bs)¤¤(dq)¤twitter-timeline¤(bs)¤¤(dq)¤  href=¤(bs)¤¤(dq)¤https:¤(bs)¤/¤(bs)¤/twitter.com¤(bs)¤/makelemonadeco¤(bs)¤¤(dq)¤ data-widget-id=¤(bs)¤¤(dq)¤334632933006655488¤(bs)¤¤(dq)¤¤(gt)¤Tweets by @makelemonadeco¤(lt)¤¤(bs)¤/a¤(gt)¤¤(bs)¤r¤(bs)¤n            ¤(lt)¤script¤(gt)¤!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=¤(bs)¤/^http:¤(bs)¤/.test(d.location)?¤(sq)¤http¤(sq)¤:¤(sq)¤https¤(sq)¤;if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+¤(bs)¤¤(dq)¤:¤(bs)¤/¤(bs)¤/platform.twitter.com¤(bs)¤/widgets.js¤(bs)¤¤(dq)¤;fjs.parentNode.insertBefore(js,fjs);}}(document,¤(bs)¤¤(dq)¤script¤(bs)¤¤(dq)¤,¤(bs)¤¤(dq)¤twitter-wjs¤(bs)¤¤(dq)¤);¤(lt)¤¤(bs)¤/script¤(gt)¤¤(bs)¤r¤(bs)¤n          ¤(dq)¤,¤(dq)¤use_theme_design¤(dq)¤:¤(dq)¤checked¤(dq)¤,¤(dq)¤twitter_num_tweets¤(dq)¤:¤(dq)¤1¤(dq)¤},¤(dq)¤_multiwidget¤(dq)¤:1}}}'>
							     			<?php esc_html_e('VenueX Demo Widgets', 'loc_canon_venuex'); ?></option>

									</select> 
								</td>

							</tr>

						</table>




					<!-- BOTTOM BUTTONS -->

					<br><br>
					
					<div class="save_submit"><?php submit_button(); ?></div>

					<input type="hidden" id="reset_basic" name="canon_options_advanced[reset_basic]" value="">
					<button id="reset_basic_button" class="button-primary reset_button"><?php esc_html_e("Reset basic settings ..", "loc_canon_venuex"); ?></button>

					<input type="hidden" id="reset_all" name="canon_options_advanced[reset_all]" value="">
					<button id="reset_all_button" class="button-primary reset_button"><?php esc_html_e("Reset all settings ..", "loc_canon_venuex"); ?></button>

				</form>
			</div> <!-- end table container -->	

	
		</div>

	</div>

