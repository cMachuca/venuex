<?php

/****************************************************
DESCRIPTION: 	POST & PAGE OPTIONS
OPTION HANDLE: 	canon_options_post
****************************************************/


	/****************************************************
	REGISTER MENU
	****************************************************/

	add_action('admin_menu', 'register_canon_options_post');

	function register_canon_options_post () {
		global $screen_handle_canon_options_post;	  						//this is the SCREEN handle used to identify the new admin menu page (notice: different than the add_menu_page handle)

		// Use this instad if submenu
		$screen_handle_canon_options_post = add_submenu_page(
			'handle_canon_options',											//the handle of the parent options page. 
			esc_html__('Posts & Pages Settings', 'loc_canon_venuex'),	//the submenu title that will appear in browser title area.
			esc_html__('Posts & Pages', 'loc_canon_venuex'),			//the on screen name of the submenu
			'manage_options',									//privileges check
			'handle_canon_options_post',						//the handle of this submenu
			'display_canon_options_post'						//the callback function to display the actual submenu page content.
		);

		//changing the name of the first submenu which has duplicate name (there are global variables $menu and $submenu which can be used. var_dump them to see content)
		// Put this in the submenu controller. NB: Not in the first add_menu_page controller, but in the first submenu controller with add_submenu_page. It is not defined until then. 
		// global $submenu;	
		// $submenu['handle_canon_options'][0][0] = "General";

	}

	/****************************************************
	INITIALIZE MENU
	****************************************************/

	add_action('admin_init', 'init_canon_options_post');	
	
	function init_canon_options_post () {
		register_setting(
			'group_canon_options_post',							//group name. The group for the fields custom_options_group
			'canon_options_post',								//the options variabel. THIS IS WEHERE YOUR OPTIONS ARE STORED.
			'validate_canon_options_post'						//optional 3rd param. Callback /function to sanitize and validate
		);
	}

	/****************************************************
	SET DEFAULTS
	****************************************************/

	add_action('after_setup_theme', 'default_canon_options_post');	

	function default_canon_options_post () {

	 	// SET DEFAULTS
	 	$default_options = array (

 			'page_show_comments' 		=> 'checked',

 			'show_tags' 				=> 'checked',
 			'show_post_nav' 			=> 'checked',
 			'post_nav_same_cat' 		=> 'unchecked',
 			'show_comments' 			=> 'checked',
 			
 			'show_person_position' 		=> 'checked',
 			'show_person_info' 			=> 'checked',
 			'show_person_nav' 			=> 'checked',
 			'person_nav_same_cat' 		=> 'unchecked',
 			
 			'show_meta_author' 			=> 'checked',
 			'show_meta_date' 			=> 'checked',
 			'show_meta_comments' 		=> 'checked',
 			'show_meta_categories' 		=> 'checked',


 			'homepage_blog_style'		=> 'full',
 			'blog_style'				=> 'full',
 			'cat_style'					=> 'full',
 			'archive_excerpt_length'	=>	325,

 			'blog_layout'				=> 'sidebar',
			'blog_sidebar'				=> 'canon_archive_sidebar_widget_area',
 			'blog_excerpt_length'		=> 345,

 			'cat_layout'				=> 'sidebar',
			'cat_sidebar'				=> 'canon_archive_sidebar_widget_area',
 			'cat_excerpt_length'		=> 345,
 			'show_cat_title'			=> 'checked',
 			'show_cat_description'		=> 'checked',

 			'archive_layout'				=> 'sidebar',
			'archive_sidebar'				=> 'canon_archive_sidebar_widget_area',
 			'archive_excerpt_length'		=> 330,

	 		'search_box_text'			=> esc_html__('What are you looking for?', "loc_canon_venuex"),
	 		'search_posts'				=> 'checked',
	 		'search_pages'				=> 'unchecked',
	 		'search_cpt'				=> 'unchecked',
	 		'search_cpt_source'			=> 'cpt_people, cpt_project',
			'widgetized_search_layout'	=> 'third_third_third',
			'search_widget_area_1'		=> 'canon_cwa_custom-widget-area-1',
			'search_widget_area_2'		=> 'canon_cwa_custom-widget-area-2',
			'search_widget_area_3'		=> 'canon_cwa_custom-widget-area-3',
			'search_widget_area_4'		=> 'canon_cwa_custom-widget-area-4',
			'search_widget_area_5'		=> 'canon_cwa_custom-widget-area-5',

 			'404_layout'				=> 'full',
			'404_sidebar'				=> 'canon_page_sidebar_widget_area',
	 		'404_title'					=> esc_html__('Page not found', "loc_canon_venuex"),
	 		'404_msg'					=> esc_html__("Sorry, you're lost my friend, the page you're looking for does not exist anymore. Take your luck at searching for a new one.", "loc_canon_venuex"),
			
	 		'currency_symbol'			=> '$',
	 		'currency_symbol_pos'		=> 'prepend',

			'revslider_clean_ui'		=> 'checked',

			'use_woocommerce_sidebar'	=> 'checked',
			'woocommerce_sidebar'		=> 'canon_page_sidebar_widget_area',

			'use_buddypress_sidebar'	=> 'checked',
			'buddypress_sidebar'		=> 'canon_page_sidebar_widget_area',

			'use_bbpress_sidebar'		=> 'checked',
			'bbpress_sidebar'			=> 'canon_page_sidebar_widget_area',

			'use_events_sidebar'		=> 'checked',
			'events_sidebar'			=> 'canon_page_sidebar_widget_area',
			'events_slider'				=> 'no_slider',
			'optimize_events_settings'	=> 'checked',

		);

		// GET EXISTING OPTIONS IF ANY
		$canon_options_post = (get_option('canon_options_post')) ? get_option('canon_options_post') : array();

		// MERGE ARRAYS. EXISTING OPTIONS WILL OVERWRITE DEFAULTS.
		$canon_options_post = array_merge($default_options, $canon_options_post);

		// SAVE OPTIONS
		update_option('canon_options_post', $canon_options_post);

	}


	/****************************************************
	VALIDATE INPUT AND DISPLAY MENU
	****************************************************/

	//remember this will strip all html php tags, strip slashes and convert quotation marks. This is not always what you want (maybe you want a field for HTML?) why you might want to modify this part.	
	function validate_canon_options_post ($new_instance) {				
		return $new_instance;
	}

	//display the menus
	function display_canon_options_post () {
		require_once get_template_directory() . "/inc/options/options_post.php";
	}