	<div class="wrap">

		<div id="icon-themes" class="icon32"></div>

		<h2><?php printf( "%s %s - %s", esc_attr(wp_get_theme()->Name), esc_html__("Settings", "loc_canon_venuex"), esc_html__("Appearance", "loc_canon_venuex") ); ?></h2>

		<?php 
			//delete_option('canon_options_appearance');
			$canon_options_appearance = get_option('canon_options_appearance'); 

			// var_dump($canon_options_appearance);

		?>

		<br>
		
		<div class="options_wrapper canon-options">
		
			<div class="table_container">

				<form method="post" action="options.php" enctype="multipart/form-data">
					<?php settings_fields('group_canon_options_appearance'); ?>				<!-- very important to add these two functions as they mediate what wordpress generates automatically from the functions.php -->
					<?php do_settings_sections('handle_canon_options_appearance'); ?>		


					<?php submit_button(); ?>
					
					<!-- 

						INDEX

						SKINS
						COLOR SETTINGS
						BACKGROUND
						GOOGLE WEBFONTS
						RELATIVE FONT SIZE
						LIGHTBOX SETTINGS
						ANIMATION: IMG SLIDERS
						ANIMATION: QUOTE SLIDERS
						ANIMATION: REVIEW SLIDERS
						ANIMATION: LAZY LOAD EFFECT
						ANIMATION: MENUS

					-->

					<!-- 
					--------------------------------------------------------------------------
						SKINS
				    -------------------------------------------------------------------------- 
					-->

						<h3><?php esc_html_e("Skins", "loc_canon_venuex"); ?> <img src="<?php echo get_template_directory_uri() . '/img/help.png' ?>"></h3>

						<div class='contextual-help'>

							<?php
								
								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Skins', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Click a skin-button to change the colour-scheme of the whole theme.', 'loc_canon_venuex'),
									),
								)); 

							?>			

						</div>

						<table class='form-table' id="skins">

							<?php
								
								fw_option(array(
									'type'					=> 'hidden',
									'slug' 					=> 'body_skin_class',
									'options_name'			=> 'canon_options_appearance',
								)); 
							
							?>

							<tr valign='top'>
								<td>
									<img src="<?php echo get_template_directory_uri() ?>/img/skin_btn_1.png" alt="" 

										data-body_skin_class					="tc-venuex-1"
										
										data-color_body							="#f9f9f9"
										data-color_plate						="#ffffff"
										data-color_general_text					="#222425"
										data-color_links						="#222425"
										data-color_links_hover					="#ff4200"
										data-color_headings						="#222425"
										data-color_text_meta					="#c7c7c7"
										data-color_text_logo					="#ffffff"
										data-color_feat_text_1					="#ff4200"
										data-color_feat_text_2					="#ff4200"
										data-color_white_text					="#ffffff"
										data-color_preheader_bg					="#000000"
										data-color_preheader					="#ffffff"
										data-color_preheader_hover				="#ff4200"
										data-color_header_bg					="#23282d"
										data-color_header						="#ffffff"
										data-color_header_hover					="#ff4200"
										data-color_postheader_bg				="#000000"
										data-color_postheader					="#ffffff"
										data-color_postheader_hover				="#ff4200"
										data-color_third_prenav					="#111111"
										data-color_third_nav					="#111111"
										data-color_third_postnav				="#111111"
										data-color_search_bg					="#1f2327"
										data-color_search_text					="#ffffff"
										data-color_search_text_hover			="#ff4200"
										data-color_search_line					="#464d51"
										data-color_sidr_bg						="#111111"
										data-color_sidr							="#ffffff"
										data-color_sidr_hover					="#ff4200"
										data-color_sidr_line					="#343434"
										data-color_btn							="#ff4200"
										data-color_btn_hover					="#c73300"
										data-color_btn_text						="#ffffff"
										data-color_btn_text_hover				="#ffffff"
										data-color_btn_2						="#ff4200"
										data-color_btn_2_hover					="#c73300"
										data-color_btn_2_text					="#ffffff"
										data-color_btn_2_text_hover				="#ffffff"
										data-color_feat_block_1					="#efefef"
										data-color_lite_block					="#f2f2f2"
										data-color_form_fields_bg				="#f6f6f6"
										data-color_form_fields_text				="#666666"
										data-color_lines						="#e3e3e3"
										data-color_footer_block					="#212425"
										data-color_footer_headings				="#ffffff"
										data-color_footer_text					="#ebebeb"
										data-color_footer_text_hover			="#ff4200"
										data-color_footlines					="#373b3c"
										data-color_footer_button				="#ff4200"
										data-color_footer_form_fields_bg		="#191b1c"
										data-color_footer_form_fields_text		="#bebebe"
										data-color_footer_alt_block				="#232627"
										data-color_footer_base					="#000000"
										data-color_footer_base_text				="#ffffff"
										data-color_footer_base_text_hover		="#ff4200"

									/>
									
									
									
								
									
									
									
									
									
								</td>
							</tr>

						</table>

					<!-- 
					--------------------------------------------------------------------------
						COLOR SETTINGS
				    -------------------------------------------------------------------------- 
					-->

						<h3><?php esc_html_e("Color settings", "loc_canon_venuex"); ?> <img src="<?php echo get_template_directory_uri() . '/img/help.png' ?>"></h3>

						<div class='contextual-help'>

							<?php
								
								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Colors', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Change the colours of the theme. Remember to save your changes.', 'loc_canon_venuex'),
									),
								)); 

							?>			

						</div>

						<table class='form-table'>

							<?php
								
								
								fw_option(array(
									'type'					=> 'color',
									'title' 				=> esc_html__('Body Background', 'loc_canon_venuex'),
									'slug' 					=> 'color_body',
									'options_name'			=> 'canon_options_appearance',
								)); 
								
								fw_option(array(
									'type'					=> 'color',
									'title' 				=> esc_html__('Main Plate Background', 'loc_canon_venuex'),
									'slug' 					=> 'color_plate',
									'options_name'			=> 'canon_options_appearance',
								)); 
								
								fw_option(array(
									'type'					=> 'color',
									'title' 				=> esc_html__('General Text', 'loc_canon_venuex'),
									'slug' 					=> 'color_general_text',
									'options_name'			=> 'canon_options_appearance',
								));
								
								fw_option(array(
									'type'					=> 'color',
									'title' 				=> esc_html__('Link Text', 'loc_canon_venuex'),
									'slug' 					=> 'color_links',
									'options_name'			=> 'canon_options_appearance',
								));
								
								fw_option(array(
									'type'					=> 'color',
									'title' 				=> esc_html__('Link Text Hover', 'loc_canon_venuex'),
									'slug' 					=> 'color_links_hover',
									'options_name'			=> 'canon_options_appearance',
								));  
								
								fw_option(array(
									'type'					=> 'color',
									'title' 				=> esc_html__('Primary Headings', 'loc_canon_venuex'),
									'slug' 					=> 'color_headings',
									'options_name'			=> 'canon_options_appearance',
								)); 
								
								fw_option(array(
									'type'					=> 'color',
									'title' 				=> esc_html__('Meta Text', 'loc_canon_venuex'),
									'slug' 					=> 'color_text_meta',
									'options_name'			=> 'canon_options_appearance',
								));
								
								fw_option(array(
									'type'					=> 'color',
									'title' 				=> esc_html__('Logo As Text', 'loc_canon_venuex'),
									'slug' 					=> 'color_text_logo',
									'options_name'			=> 'canon_options_appearance',
								));
								
								fw_option(array(
									'type'					=> 'color',
									'title' 				=> esc_html__('Primary Feature Text', 'loc_canon_venuex'),
									'slug' 					=> 'color_feat_text_1',
									'options_name'			=> 'canon_options_appearance',
								));    
								
								fw_option(array(
									'type'					=> 'color',
									'title' 				=> esc_html__('Secondary Feature Text', 'loc_canon_venuex'),
									'slug' 					=> 'color_feat_text_2',
									'options_name'			=> 'canon_options_appearance',
								)); 
								
								fw_option(array(
									'type'					=> 'color',
									'title' 				=> esc_html__('White Text', 'loc_canon_venuex'),
									'slug' 					=> 'color_white_text',
									'options_name'			=> 'canon_options_appearance',
								)); 
								
								fw_option(array(
									'type'					=> 'color',
									'title' 				=> esc_html__('Pre Header Background', 'loc_canon_venuex'),
									'slug' 					=> 'color_preheader_bg',
									'options_name'			=> 'canon_options_appearance',
								));
								
								fw_option(array(
									'type'					=> 'color',
									'title' 				=> esc_html__('Pre Header Text', 'loc_canon_venuex'),
									'slug' 					=> 'color_preheader',
									'options_name'			=> 'canon_options_appearance',
								));
								
								fw_option(array(
									'type'					=> 'color',
									'title' 				=> esc_html__('Pre Header Text Hover', 'loc_canon_venuex'),
									'slug' 					=> 'color_preheader_hover',
									'options_name'			=> 'canon_options_appearance',
								));
								
								fw_option(array(
									'type'					=> 'color',
									'title' 				=> esc_html__('Header Background', 'loc_canon_venuex'),
									'slug' 					=> 'color_header_bg',
									'options_name'			=> 'canon_options_appearance',
								));
								
								fw_option(array(
									'type'					=> 'color',
									'title' 				=> esc_html__('Header Text', 'loc_canon_venuex'),
									'slug' 					=> 'color_header',
									'options_name'			=> 'canon_options_appearance',
								));
								
								fw_option(array(
									'type'					=> 'color',
									'title' 				=> esc_html__('Header Text hover', 'loc_canon_venuex'),
									'slug' 					=> 'color_header_hover',
									'options_name'			=> 'canon_options_appearance',
								));
								
								fw_option(array(
									'type'					=> 'color',
									'title' 				=> esc_html__('Post Header Background', 'loc_canon_venuex'),
									'slug' 					=> 'color_postheader_bg',
									'options_name'			=> 'canon_options_appearance',
								));
								
								fw_option(array(
									'type'					=> 'color',
									'title' 				=> esc_html__('Post Header Text', 'loc_canon_venuex'),
									'slug' 					=> 'color_postheader',
									'options_name'			=> 'canon_options_appearance',
								));
								
								fw_option(array(
									'type'					=> 'color',
									'title' 				=> esc_html__('Post Header Text Hover', 'loc_canon_venuex'),
									'slug' 					=> 'color_postheader_hover',
									'options_name'			=> 'canon_options_appearance',
								));
								
								fw_option(array(
									'type'					=> 'color',
									'title' 				=> esc_html__('Pre Header Tertiary Menu Background', 'loc_canon_venuex'),
									'slug' 					=> 'color_third_prenav',
									'options_name'			=> 'canon_options_appearance',
								));
								
								fw_option(array(
									'type'					=> 'color',
									'title' 				=> esc_html__('Header Tertiary Menu Background', 'loc_canon_venuex'),
									'slug' 					=> 'color_third_nav',
									'options_name'			=> 'canon_options_appearance',
								));
								
								fw_option(array(
									'type'					=> 'color',
									'title' 				=> esc_html__('Post Header Tertiary Menu Background', 'loc_canon_venuex'),
									'slug' 					=> 'color_third_postnav',
									'options_name'			=> 'canon_options_appearance',
								)); 
								
								fw_option(array(
									'type'					=> 'color',
									'title' 				=> esc_html__('Search Feature Background', 'loc_canon_venuex'),
									'slug' 					=> 'color_search_bg',
									'options_name'			=> 'canon_options_appearance',
								));
								
								fw_option(array(
									'type'					=> 'color',
									'title' 				=> esc_html__('Search feature Text', 'loc_canon_venuex'),
									'slug' 					=> 'color_search_text',
									'options_name'			=> 'canon_options_appearance',
								));
								
								fw_option(array(
									'type'					=> 'color',
									'title' 				=> esc_html__('Search feature Text Hover', 'loc_canon_venuex'),
									'slug' 					=> 'color_search_text_hover',
									'options_name'			=> 'canon_options_appearance',
								));
								
								fw_option(array(
									'type'					=> 'color',
									'title' 				=> esc_html__('Search feature Borders', 'loc_canon_venuex'),
									'slug' 					=> 'color_search_line',
									'options_name'			=> 'canon_options_appearance',
								));
								
								fw_option(array(
									'type'					=> 'color',
									'title' 				=> esc_html__('Responsive Menu Background', 'loc_canon_venuex'),
									'slug' 					=> 'color_sidr_bg',
									'options_name'			=> 'canon_options_appearance',
								)); 
								
								fw_option(array(
									'type'					=> 'color',
									'title' 				=> esc_html__('Responsive Menu Text', 'loc_canon_venuex'),
									'slug' 					=> 'color_sidr',
									'options_name'			=> 'canon_options_appearance',
								));
								
								fw_option(array(
									'type'					=> 'color',
									'title' 				=> esc_html__('Responsive Menu Text Hover', 'loc_canon_venuex'),
									'slug' 					=> 'color_sidr_hover',
									'options_name'			=> 'canon_options_appearance',
								));
								
								fw_option(array(
									'type'					=> 'color',
									'title' 				=> esc_html__('Responsive Menu Borders', 'loc_canon_venuex'),
									'slug' 					=> 'color_sidr_line',
									'options_name'			=> 'canon_options_appearance',
								));    
								
								fw_option(array(
									'type'					=> 'color',
									'title' 				=> esc_html__('Buttons', 'loc_canon_venuex'),
									'slug' 					=> 'color_btn',
									'options_name'			=> 'canon_options_appearance',
								));
								
								fw_option(array(
									'type'					=> 'color',
									'title' 				=> esc_html__('Buttons Hover', 'loc_canon_venuex'),
									'slug' 					=> 'color_btn_hover',
									'options_name'			=> 'canon_options_appearance',
								));
								
								fw_option(array(
									'type'					=> 'color',
									'title' 				=> esc_html__('Buttons Text', 'loc_canon_venuex'),
									'slug' 					=> 'color_btn_text',
									'options_name'			=> 'canon_options_appearance',
								));
								
								fw_option(array(
									'type'					=> 'color',
									'title' 				=> esc_html__('Buttons Text Hover', 'loc_canon_venuex'),
									'slug' 					=> 'color_btn_text_hover',
									'options_name'			=> 'canon_options_appearance',
								));
								
								fw_option(array(
									'type'					=> 'color',
									'title' 				=> esc_html__('Button 2 Background', 'loc_canon_venuex'),
									'slug' 					=> 'color_btn_2',
									'options_name'			=> 'canon_options_appearance',
								));
								
								fw_option(array(
									'type'					=> 'color',
									'title' 				=> esc_html__('Button 2 Hover Background', 'loc_canon_venuex'),
									'slug' 					=> 'color_btn_2_hover',
									'options_name'			=> 'canon_options_appearance',
								));
								
								fw_option(array(
									'type'					=> 'color',
									'title' 				=> esc_html__('Button 2 Text', 'loc_canon_venuex'),
									'slug' 					=> 'color_btn_2_text',
									'options_name'			=> 'canon_options_appearance',
								));
								
								fw_option(array(
									'type'					=> 'color',
									'title' 				=> esc_html__('Button 2 Text Hover', 'loc_canon_venuex'),
									'slug' 					=> 'color_btn_2_text_hover',
									'options_name'			=> 'canon_options_appearance',
								));
								
								fw_option(array(
									'type'					=> 'color',
									'title' 				=> esc_html__('Feature Block 1', 'loc_canon_venuex'),
									'slug' 					=> 'color_feat_block_1',
									'options_name'			=> 'canon_options_appearance',
								));
								
								fw_option(array(
									'type'					=> 'color',
									'title' 				=> esc_html__('Light Block Elements', 'loc_canon_venuex'),
									'slug' 					=> 'color_lite_block',
									'options_name'			=> 'canon_options_appearance',
								));
								
								fw_option(array(
									'type'					=> 'color',
									'title' 				=> esc_html__('Form Fields Background', 'loc_canon_venuex'),
									'slug' 					=> 'color_form_fields_bg',
									'options_name'			=> 'canon_options_appearance',
								)); 
							
								fw_option(array(
									'type'					=> 'color',
									'title' 				=> esc_html__('Form Fields Text', 'loc_canon_venuex'),
									'slug' 					=> 'color_form_fields_text',
									'options_name'			=> 'canon_options_appearance',
								)); 
								
								fw_option(array(
									'type'					=> 'color',
									'title' 				=> esc_html__('Border/Rulers Color', 'loc_canon_venuex'),
									'slug' 					=> 'color_lines',
									'options_name'			=> 'canon_options_appearance',
								));
								
								fw_option(array(
									'type'					=> 'color',
									'title' 				=> esc_html__('Footer Block', 'loc_canon_venuex'),
									'slug' 					=> 'color_footer_block',
									'options_name'			=> 'canon_options_appearance',
								));
								
								fw_option(array(
									'type'					=> 'color',
									'title' 				=> esc_html__('Footer Headings', 'loc_canon_venuex'),
									'slug' 					=> 'color_footer_headings',
									'options_name'			=> 'canon_options_appearance',
								)); 
								
								fw_option(array(
									'type'					=> 'color',
									'title' 				=> esc_html__('Footer Text', 'loc_canon_venuex'),
									'slug' 					=> 'color_footer_text',
									'options_name'			=> 'canon_options_appearance',
								));
								
								fw_option(array(
									'type'					=> 'color',
									'title' 				=> esc_html__('Footer Text Hover', 'loc_canon_venuex'),
									'slug' 					=> 'color_footer_text_hover',
									'options_name'			=> 'canon_options_appearance',
								)); 
								
								fw_option(array(
									'type'					=> 'color',
									'title' 				=> esc_html__('Footer Border/Rulers Color', 'loc_canon_venuex'),
									'slug' 					=> 'color_footlines',
									'options_name'			=> 'canon_options_appearance',
								));
								
								fw_option(array(
									'type'					=> 'color',
									'title' 				=> esc_html__('Footer Buttons', 'loc_canon_venuex'),
									'slug' 					=> 'color_footer_button',
									'options_name'			=> 'canon_options_appearance',
								)); 
								
								fw_option(array(
									'type'					=> 'color',
									'title' 				=> esc_html__('Footer Form Fields Background', 'loc_canon_venuex'),
									'slug' 					=> 'color_footer_form_fields_bg',
									'options_name'			=> 'canon_options_appearance',
								)); 
							
								fw_option(array(
									'type'					=> 'color',
									'title' 				=> esc_html__('Footer Form Fields Text', 'loc_canon_venuex'),
									'slug' 					=> 'color_footer_form_fields_text',
									'options_name'			=> 'canon_options_appearance',
								)); 
							
								fw_option(array(
									'type'					=> 'color',
									'title' 				=> esc_html__('Footer Alternate Block Color', 'loc_canon_venuex'),
									'slug' 					=> 'color_footer_alt_block',
									'options_name'			=> 'canon_options_appearance',
								)); 
								
								fw_option(array(
									'type'					=> 'color',
									'title' 				=> esc_html__('Footer Base', 'loc_canon_venuex'),
									'slug' 					=> 'color_footer_base',
									'options_name'			=> 'canon_options_appearance',
								));
								
								fw_option(array(
									'type'					=> 'color',
									'title' 				=> esc_html__('Footer Base Text', 'loc_canon_venuex'),
									'slug' 					=> 'color_footer_base_text',
									'options_name'			=> 'canon_options_appearance',
								));
								
								fw_option(array(
									'type'					=> 'color',
									'title' 				=> esc_html__('Footer Base Text Hover', 'loc_canon_venuex'),
									'slug' 					=> 'color_footer_base_text_hover',
									'options_name'			=> 'canon_options_appearance',
								));


							?>			


						</table>


					<!-- 
					--------------------------------------------------------------------------
						BACKGROUND
				    -------------------------------------------------------------------------- 
					-->

						<h3><?php esc_html_e("Background", "loc_canon_venuex"); ?> <img src="<?php echo get_template_directory_uri() . '/img/help.png' ?>"></h3>

						<div class='contextual-help'>
							<?php

								fw_option_help(array(
									'type'					=> 'ul',
									'title' 				=> esc_html__('Background image URL', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Enter a complete URL to the image you want to use or', 'loc_canon_venuex'),
										esc_html__('Click the "Upload" button, upload an image and make sure you click the "Use this image" button or', 'loc_canon_venuex'),
										esc_html__('Click the "Upload" button and choose an image from the media library tab. Make sure you click the "Use this image" button.', 'loc_canon_venuex'),
										esc_html__('Remember to save your changes.', 'loc_canon_venuex'),
										esc_html__('NB: the background image will be positioned top-center.', 'loc_canon_venuex'),
									),
								));

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Background link (optional)', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('If you insert a link here you background will automatically be made clickable. Clicking the background will open up your link in a new window. Great for take-over style ad-campaigns.', 'loc_canon_venuex'),
										esc_html__('NB: Only works with boxed design.', 'loc_canon_venuex'),
									),
								));

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Repeat', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('If set to repeat the background image will repeat vertically.', 'loc_canon_venuex'),
									),
								));

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Attachment', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('If set to fixed the background image will not scroll.', 'loc_canon_venuex'),
									),
								));

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Background pattern', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Click one of buttons to use that background pattern. Notice that the url of pattern image file will be automatically inserted into the Backgroun image URL input. Also notice that Repeat and attachment selects will be updated to recommended selections for use with pattern backgrounds (repeat fixed). Remember to save your changes.', 'loc_canon_venuex'),
									),
								));

							?> 

						</div>

						<table class='form-table' id="background_table">

							<?php

								fw_option(array(
									'type'					=> 'upload',
									'title' 				=> esc_html__('Background image URL', 'loc_canon_venuex'),
									'slug' 					=> 'bg_img_url',
									'btn_text'				=> esc_html__('Upload background image', 'loc_canon_venuex'),
									'options_name'			=> 'canon_options_appearance',
								)); 

								fw_option(array(
									'type'					=> 'text',
									'title' 				=> esc_html__('Background link (optional)', 'loc_canon_venuex'),
									'slug' 					=> 'bg_link',
									'class'					=> 'widefat',
									'options_name'			=> 'canon_options_appearance',
								)); 

								fw_option(array(
									'type'					=> 'select',
									'title' 				=> esc_html__('Repeat', 'loc_canon_venuex'),
									'slug' 					=> 'bg_repeat',
									'select_options'		=> array(
										'repeat'			=> esc_html__('Repeat', 'loc_canon_venuex'),
										'no-repeat'			=> esc_html__('No repeat', 'loc_canon_venuex')
									),
									'options_name'			=> 'canon_options_appearance',
								)); 

								fw_option(array(
									'type'					=> 'select',
									'title' 				=> esc_html__('Attachment', 'loc_canon_venuex'),
									'slug' 					=> 'bg_attachment',
									'select_options'		=> array(
										'fixed'				=> esc_html__('Fixed', 'loc_canon_venuex'),
										'scroll'			=> esc_html__('Scroll', 'loc_canon_venuex')
									),
									'options_name'			=> 'canon_options_appearance',
								)); 

							 ?>		

							<tr valign='top'>
								<th scope='row'><?php esc_html_e("Background pattern", "loc_canon_venuex"); ?></th>
								<td class="bg_pattern_picker">
									<img src="<?php echo get_template_directory_uri(); ?>/img/patterns/tile_btn.png" data-img_file="tile.png"> 
									<img src="<?php echo get_template_directory_uri(); ?>/img/patterns/tile2_btn.png" data-img_file="tile2.png"> 
									<img src="<?php echo get_template_directory_uri(); ?>/img/patterns/tile3_btn.png" data-img_file="tile3.png">
									<img src="<?php echo get_template_directory_uri(); ?>/img/patterns/tile4_btn.png" data-img_file="tile4.png"> 
									<img src="<?php echo get_template_directory_uri(); ?>/img/patterns/tile5_btn.png" data-img_file="tile5.png"> 
									<img src="<?php echo get_template_directory_uri(); ?>/img/patterns/tile6_btn.png" data-img_file="tile6.png">
									<img src="<?php echo get_template_directory_uri(); ?>/img/patterns/tile7_btn.png" data-img_file="tile7.png"> 
									<img src="<?php echo get_template_directory_uri(); ?>/img/patterns/tile8_btn.png" data-img_file="tile8.png"> 
									<img src="<?php echo get_template_directory_uri(); ?>/img/patterns/tile9_btn.png" data-img_file="tile9.png"> 
									<img src="<?php echo get_template_directory_uri(); ?>/img/patterns/tile10_btn.png" data-img_file="tile10.png">  
								</td>
							</tr>


						</table>


					<!-- 
					--------------------------------------------------------------------------
						GOOGLE WEBFONTS
				    -------------------------------------------------------------------------- 
					-->

						<h3><?php esc_html_e("Google Webfonts", "loc_canon_venuex"); ?> <img src="<?php echo get_template_directory_uri() . '/img/help.png' ?>"></h3>

						<div class='contextual-help'>
							<?php

								fw_option_help(array(
									'type'					=> 'ul',
									'title' 				=> esc_html__('Change fonts', 'loc_canon_venuex'),
									'content' 				=> array(
										wp_kses_post(__('<i>first select:</i> Font name.', 'loc_canon_venuex')),
										wp_kses_post(__('<i>middle select:</i> Font variants (will change automatically if available for the chosen font).', 'loc_canon_venuex')),
										wp_kses_post(__('<i>last select:</i> Font subset (will change automatically if available for the chosen font).', 'loc_canon_venuex')),
									),
								));

								fw_option_help(array(
									'type'					=> 'paragraphs',
									'title' 				=> esc_html__('More info', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Notice: You can only control the general fonts to be used. However, parameters like font size, styling, letter-spacing etc. are controlled by the theme itself.', 'loc_canon_venuex'),
										wp_kses_post(__('Go to <a href="http://www.google.com/webfonts" target="_blank">Google Webfonts</a> homepage to preview fonts.', 'loc_canon_venuex')),
									),
								));

							?> 

						</div>

						<table class='form-table'>

							<?php 

								fw_option(array(
									'type'					=> 'font',
									'title' 				=> esc_html__('Body text', 'loc_canon_venuex'),
									'slug' 					=> 'font_main',
									'options_name'			=> 'canon_options_appearance',
								)); 
								
								fw_option(array(
									'type'					=> 'font',
									'title' 				=> esc_html__('Meta text', 'loc_canon_venuex'),
									'slug' 					=> 'font_meta',
									'options_name'			=> 'canon_options_appearance',
								)); 

								fw_option(array(
									'type'					=> 'font',
									'title' 				=> esc_html__('Feature text', 'loc_canon_venuex'),
									'slug' 					=> 'font_quote',
									'options_name'			=> 'canon_options_appearance',
								));
								
								fw_option(array(
									'type'					=> 'font',
									'title' 				=> esc_html__('Lead in text', 'loc_canon_venuex'),
									'slug' 					=> 'font_lead',
									'options_name'			=> 'canon_options_appearance',
								)); 
								
								fw_option(array(
									'type'					=> 'font',
									'title' 				=> esc_html__('Logo text', 'loc_canon_venuex'),
									'slug' 					=> 'font_logotext',
									'options_name'			=> 'canon_options_appearance',
								)); 

								fw_option(array(
									'type'					=> 'font',
									'title' 				=> esc_html__('Bold text', 'loc_canon_venuex'),
									'slug' 					=> 'font_bold',
									'options_name'			=> 'canon_options_appearance',
								)); 

								fw_option(array(
									'type'					=> 'font',
									'title' 				=> esc_html__('Button text', 'loc_canon_venuex'),
									'slug' 					=> 'font_button',
									'options_name'			=> 'canon_options_appearance',
								)); 

								fw_option(array(
									'type'					=> 'font',
									'title' 				=> esc_html__('Italic text', 'loc_canon_venuex'),
									'slug' 					=> 'font_italic',
									'options_name'			=> 'canon_options_appearance',
								)); 

								fw_option(array(
									'type'					=> 'font',
									'title' 				=> esc_html__('Main headings text', 'loc_canon_venuex'),
									'slug' 					=> 'font_heading',
									'options_name'			=> 'canon_options_appearance',
								));
								
								fw_option(array(
									'type'					=> 'font',
									'title' 				=> esc_html__('Second headings text', 'loc_canon_venuex'),
									'slug' 					=> 'font_heading2',
									'options_name'			=> 'canon_options_appearance',
								));  

								fw_option(array(
									'type'					=> 'font',
									'title' 				=> esc_html__('Nav style text', 'loc_canon_venuex'),
									'slug' 					=> 'font_nav',
									'options_name'			=> 'canon_options_appearance',
								)); 

								fw_option(array(
									'type'					=> 'font',
									'title' 				=> esc_html__('Widget footer text', 'loc_canon_venuex'),
									'slug' 					=> 'font_widget_footer',
									'options_name'			=> 'canon_options_appearance',
								)); 

							?>

						</table>


					<!-- 
					--------------------------------------------------------------------------
						RELATIVE FONT SIZE
				    -------------------------------------------------------------------------- 
					-->


						<h3><?php esc_html_e("Relative Font Size", "loc_canon_venuex"); ?> <img src="<?php echo get_template_directory_uri() . '/img/help.png' ?>"></h3>

						<div class='contextual-help'>
							<?php

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Font size', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Adjust the relative size of all fonts.', 'loc_canon_venuex'),
									),
								));

							?> 

						</div>
						

						<table class='form-table'>

							<?php 

								fw_option(array(
									'type'					=> 'number',
									'title' 				=> esc_html__('Font size', 'loc_canon_venuex'),
									'slug' 					=> 'font_size_root',
									'min'					=> '0',
									'max'					=> '1000',
									'step'					=> '1',
									'width_px'				=> '60',
									'colspan'				=> '2',
									'postfix' 				=> '%',
									'options_name'			=> 'canon_options_appearance',
								)); 

							?>

						</table>


					<!-- 
					--------------------------------------------------------------------------
						LIGHTBOX SETTINGS
				    -------------------------------------------------------------------------- 
					-->


						<h3><?php esc_html_e("Lightbox settings", "loc_canon_venuex"); ?> <img src="<?php echo get_template_directory_uri() . '/img/help.png' ?>"></h3>

						<div class='contextual-help'>
							<?php

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Lightbox overlay color', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Select the color of the lightbox overlay.', 'loc_canon_venuex'),
									),
								));

								fw_option_help(array(
									'type'					=> 'ul',
									'title' 				=> esc_html__('Lightbox overlay opacity', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Select the opacity of the lightbox overlay.', 'loc_canon_venuex'),
										esc_html__('Choose a value between 0 and 1.', 'loc_canon_venuex'),
										esc_html__('0 is completely transparent.', 'loc_canon_venuex'),
										esc_html__('1 is compeltely solid.', 'loc_canon_venuex'),
									),
								));

							?> 

						</div>

						<table class='form-table'>

							<?php

								fw_option(array(
									'type'					=> 'color',
									'title' 				=> esc_html__('Lightbox overlay color', 'loc_canon_venuex'),
									'slug' 					=> 'lightbox_overlay_color',
									'options_name'			=> 'canon_options_appearance',
								)); 

								fw_option(array(
									'type'					=> 'number',
									'title' 				=> esc_html__('Lightbox overlay opacity', 'loc_canon_venuex'),
									'slug' 					=> 'lightbox_overlay_opacity',
									'min'					=> '0',
									'max'					=> '1',
									'step'					=> '0.1',
									'width_px'				=> '60',
									'colspan'				=> '2',
									'options_name'			=> 'canon_options_appearance',
								)); 

							?>

						</table>


					<!-- 
					--------------------------------------------------------------------------
						ANIMATION: IMG SLIDERS
				    -------------------------------------------------------------------------- 
					-->

						<h3><?php esc_html_e("Animation: Image Sliders", "loc_canon_venuex"); ?> <img src="<?php echo get_template_directory_uri() . '/img/help.png' ?>"></h3>

						<div class='contextual-help'>

							This controls general behavior of image flexsliders used in theme.

							<br><br>

							<?php

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Slideshow', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('If checked slides will change automatically.', 'loc_canon_venuex'),
									),
								));

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Slide delay', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Delay between each slide (in milliseconds).', 'loc_canon_venuex'),
									),
								));

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Animation duration', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Duration of transition animation (in milliseconds).', 'loc_canon_venuex'),
									),
								));

							?> 

						</div>

						<table class='form-table'>

							<?php 

								fw_option(array(
									'type'					=> 'checkbox',
									'title' 				=> esc_html__('Slideshow', 'loc_canon_venuex'),
									'slug' 					=> 'anim_img_slider_slideshow',
									'options_name'			=> 'canon_options_appearance',
								)); 

								fw_option(array(
									'type'					=> 'number',
									'title' 				=> esc_html__('Slide delay', 'loc_canon_venuex'),
									'slug' 					=> 'anim_img_slider_delay',
									'min'					=> '0',
									'max'					=> '100000',
									'step'					=> '10',
									'postfix'				=> '<i> (milliseconds)</i>',
									'width_px'				=> '60',
									'options_name'			=> 'canon_options_appearance',
								)); 

								fw_option(array(
									'type'					=> 'number',
									'title' 				=> esc_html__('Animation duration', 'loc_canon_venuex'),
									'slug' 					=> 'anim_img_slider_anim_duration',
									'min'					=> '0',
									'max'					=> '100000',
									'step'					=> '10',
									'postfix'				=> '<i> (milliseconds)</i>',
									'width_px'				=> '60',
									'options_name'			=> 'canon_options_appearance',
								)); 

							?>

						</table>


					<!-- 
					--------------------------------------------------------------------------
						ANIMATION: QUOTE SLIDERS
				    -------------------------------------------------------------------------- 
					-->

						<h3><?php esc_html_e("Animation: Quote Sliders", "loc_canon_venuex"); ?> <img src="<?php echo get_template_directory_uri() . '/img/help.png' ?>"></h3>

						<div class='contextual-help'>

							This controls general behavior of quote flexsliders used in theme.

							<br><br>

							<?php

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Slideshow', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('If checked slides will change automatically.', 'loc_canon_venuex'),
									),
								));

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Slide delay', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Delay between each slide (in milliseconds).', 'loc_canon_venuex'),
									),
								));

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Animation duration', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Duration of transition animation (in milliseconds).', 'loc_canon_venuex'),
									),
								));

							?> 

						</div>

						<table class='form-table'>

							<?php 

								fw_option(array(
									'type'					=> 'checkbox',
									'title' 				=> esc_html__('Slideshow', 'loc_canon_venuex'),
									'slug' 					=> 'anim_quote_slider_slideshow',
									'options_name'			=> 'canon_options_appearance',
								)); 

								fw_option(array(
									'type'					=> 'number',
									'title' 				=> esc_html__('Slide delay', 'loc_canon_venuex'),
									'slug' 					=> 'anim_quote_slider_delay',
									'min'					=> '0',
									'max'					=> '100000',
									'step'					=> '10',
									'postfix'				=> '<i> (milliseconds)</i>',
									'width_px'				=> '60',
									'options_name'			=> 'canon_options_appearance',
								)); 

								fw_option(array(
									'type'					=> 'number',
									'title' 				=> esc_html__('Animation duration', 'loc_canon_venuex'),
									'slug' 					=> 'anim_quote_slider_anim_duration',
									'min'					=> '0',
									'max'					=> '100000',
									'step'					=> '10',
									'postfix'				=> '<i> (milliseconds)</i>',
									'width_px'				=> '60',
									'options_name'			=> 'canon_options_appearance',
								)); 

							?>

						</table>

					<!-- 
					--------------------------------------------------------------------------
						ANIMATION: REVIEW SLIDERS
				    -------------------------------------------------------------------------- 
					-->

						<h3><?php esc_html_e("Animation: Review Sliders", "loc_canon_venuex"); ?> <img src="<?php echo get_template_directory_uri() . '/img/help.png' ?>"></h3>

						<div class='contextual-help'>

							<?php esc_html_e("This controls general behavior of review flexsliders used in theme.", "loc_canon_venuex"); ?>

							<br><br>

							<?php

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Slideshow', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('If checked slides will change automatically.', 'loc_canon_venuex'),
									),
								));

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Slide delay', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Delay between each slide (in milliseconds).', 'loc_canon_venuex'),
									),
								));

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Animation duration', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Duration of transition animation (in milliseconds).', 'loc_canon_venuex'),
									),
								));

							?> 

						</div>

						<table class='form-table'>

							<?php 

								fw_option(array(
									'type'					=> 'checkbox',
									'title' 				=> esc_html__('Slideshow', 'loc_canon_venuex'),
									'slug' 					=> 'anim_menu_slider_slideshow',
									'options_name'			=> 'canon_options_appearance',
								)); 

								fw_option(array(
									'type'					=> 'number',
									'title' 				=> esc_html__('Slide delay', 'loc_canon_venuex'),
									'slug' 					=> 'anim_menu_slider_delay',
									'min'					=> '0',
									'max'					=> '100000',
									'step'					=> '10',
									'postfix'				=> '<i> (milliseconds)</i>',
									'width_px'				=> '60',
									'options_name'			=> 'canon_options_appearance',
								)); 

								fw_option(array(
									'type'					=> 'number',
									'title' 				=> esc_html__('Animation duration', 'loc_canon_venuex'),
									'slug' 					=> 'anim_menu_slider_anim_duration',
									'min'					=> '0',
									'max'					=> '100000',
									'step'					=> '10',
									'postfix'				=> '<i> (milliseconds)</i>',
									'width_px'				=> '60',
									'options_name'			=> 'canon_options_appearance',
								)); 

							?>

						</table>

					<!-- 
					--------------------------------------------------------------------------
						ANIMATION: LAZY LOAD EFFECT
				    -------------------------------------------------------------------------- 
					-->

						<h3><?php esc_html_e("Animation: Lazy Load Effect", "loc_canon_venuex"); ?> <img src="<?php echo get_template_directory_uri() . '/img/help.png' ?>"></h3>

						<div class='contextual-help'>

							<?php

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Lazy Load Effect', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('When scrolling down elements fade in as they enter the viewport simulating lazy load.', 'loc_canon_venuex'),
									),
								));

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Start animation after', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Delay before animation starts (in seconds).', 'loc_canon_venuex'),
									),
								));

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Enter from', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Element moves in from this angle.', 'loc_canon_venuex'),
									),
								));

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Move', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('How much the element will move (in pixels). Can be 0 if you do not want the element to move at all.', 'loc_canon_venuex'),
									),
								));

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Animation duration', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('How long the fade-in animation lasts (in seconds).', 'loc_canon_venuex'),
									),
								));

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Viewport factor', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('How big a part of the element that must enter the viewport for the fade-in animation to trigger. 0 will trigger fade-in animation right when element enters viewport. 1 will require the whole element to enter viewport before triggering fade-in.', 'loc_canon_venuex'),
									),
								));

							?> 

						</div>

						<table class='form-table'>

							<?php 

								fw_option(array(
									'type'					=> 'checkbox',
									'title' 				=> esc_html__('Use on pagebuilder blocks', 'loc_canon_venuex'),
									'slug' 					=> 'lazy_load_on_pagebuilder_blocks',
									'options_name'			=> 'canon_options_appearance',
								)); 

								fw_option(array(
									'type'					=> 'checkbox',
									'title' 				=> esc_html__('Use on blog/archive posts', 'loc_canon_venuex'),
									'slug' 					=> 'lazy_load_on_blog',
									'options_name'			=> 'canon_options_appearance',
								)); 

								fw_option(array(
									'type'					=> 'checkbox',
									'title' 				=> esc_html__('Use on widgets', 'loc_canon_venuex'),
									'slug' 					=> 'lazy_load_on_widgets',
									'options_name'			=> 'canon_options_appearance',
								)); 

								fw_option(array(
									'type'					=> 'number',
									'title' 				=> esc_html__('Start animation after', 'loc_canon_venuex'),
									'slug' 					=> 'lazy_load_after',
									'min'					=> '0',
									'max'					=> '100',
									'step'					=> '0.01',
									'postfix'				=> '<i> (seconds)</i>',
									'width_px'				=> '60',
									'options_name'			=> 'canon_options_appearance',
								)); 

								fw_option(array(
									'type'					=> 'select',
									'title' 				=> esc_html__('Enter from', 'loc_canon_venuex'),
									'slug' 					=> 'lazy_load_enter',
									'select_options'		=> array(
										'top'				=> esc_html__('Top', 'loc_canon_venuex'),
										'right'				=> esc_html__('Right', 'loc_canon_venuex'),
										'bottom'			=> esc_html__('Bottom', 'loc_canon_venuex'),
										'left'				=> esc_html__('Left', 'loc_canon_venuex'),
									),
									'options_name'			=> 'canon_options_appearance',
								)); 


								fw_option(array(
									'type'					=> 'number',
									'title' 				=> esc_html__('Move', 'loc_canon_venuex'),
									'slug' 					=> 'lazy_load_move',
									'min'					=> '0',
									'max'					=> '1000',
									'step'					=> '1',
									'postfix'				=> '<i> (pixels)</i>',
									'width_px'				=> '60',
									'options_name'			=> 'canon_options_appearance',
								)); 

								fw_option(array(
									'type'					=> 'number',
									'title' 				=> esc_html__('Animation duration', 'loc_canon_venuex'),
									'slug' 					=> 'lazy_load_over',
									'min'					=> '0',
									'max'					=> '100',
									'step'					=> '0.01',
									'postfix'				=> '<i> (seconds)</i>',
									'width_px'				=> '60',
									'options_name'			=> 'canon_options_appearance',
								)); 

								fw_option(array(
									'type'					=> 'number',
									'title' 				=> esc_html__('Viewport factor', 'loc_canon_venuex'),
									'slug' 					=> 'lazy_load_viewport_factor',
									'min'					=> '0',
									'max'					=> '1',
									'step'					=> '0.01',
									'postfix'				=> '<i> (ratio)</i>',
									'width_px'				=> '60',
									'options_name'			=> 'canon_options_appearance',
								)); 

							?>

						</table>


					<!-- 
					--------------------------------------------------------------------------
						ANIMATION: MENUS
				    -------------------------------------------------------------------------- 
					-->

						<h3><?php esc_html_e("Animation: Menus", "loc_canon_venuex"); ?> <img src="<?php echo get_template_directory_uri() . '/img/help.png' ?>"></h3>

						<div class='contextual-help'>

							<?php

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Animate menus', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Select which menus to animate - or turn off menu animation altogether.', 'loc_canon_venuex'),
									),
								));

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Enter from', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Element moves in from this angle.', 'loc_canon_venuex'),
									),
								));

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Move distance', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('How much the element will move (in pixels). Can be 0 if you do not want the element to move at all.', 'loc_canon_venuex'),
									),
								));

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Animation duration', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Duration of the menu animation.', 'loc_canon_venuex'),
									),
								));

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Delay between elements', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Delay in milliseconds between each menu item starts to appear.', 'loc_canon_venuex'),
									),
								));

							?> 

						</div>

						<table class='form-table'>

							<?php 

								fw_option(array(
									'type'					=> 'select',
									'title' 				=> esc_html__('Animate menus', 'loc_canon_venuex'),
									'slug' 					=> 'anim_menus',
									'select_options'		=> array(
										'anim_menus_off'		=> esc_html__('Off', 'loc_canon_venuex'),
										'.primary_menu'			=> esc_html__('Primary menu', 'loc_canon_venuex'),
										'.secondary_menu'		=> esc_html__('Secondary menu', 'loc_canon_venuex'),
										'.nav'					=> esc_html__('Primary + Secondary menu', 'loc_canon_venuex'),
									),
									'options_name'			=> 'canon_options_appearance',
								)); 

								fw_option(array(
									'type'					=> 'select',
									'title' 				=> esc_html__('Enter from', 'loc_canon_venuex'),
									'slug' 					=> 'anim_menus_enter',
									'select_options'		=> array(
										'bottom'			=> esc_html__('Top', 'loc_canon_venuex'),
										'left'				=> esc_html__('Right', 'loc_canon_venuex'),
										'top'				=> esc_html__('Bottom', 'loc_canon_venuex'),
										'right'				=> esc_html__('Left', 'loc_canon_venuex'),
									),
									'options_name'			=> 'canon_options_appearance',
								)); 

								fw_option(array(
									'type'					=> 'number',
									'title' 				=> esc_html__('Move distance', 'loc_canon_venuex'),
									'slug' 					=> 'anim_menus_move',
									'min'					=> '0',
									'max'					=> '10000',
									'step'					=> '1',
									'postfix'				=> '<i> (pixels)</i>',
									'width_px'				=> '60',
									'options_name'			=> 'canon_options_appearance',
								)); 

								fw_option(array(
									'type'					=> 'number',
									'title' 				=> esc_html__('Animation duration', 'loc_canon_venuex'),
									'slug' 					=> 'anim_menus_duration',
									'min'					=> '0',
									'max'					=> '10000',
									'step'					=> '10',
									'postfix'				=> '<i> (milliseconds)</i>',
									'width_px'				=> '60',
									'options_name'			=> 'canon_options_appearance',
								)); 

								fw_option(array(
									'type'					=> 'number',
									'title' 				=> esc_html__('Delay between elements', 'loc_canon_venuex'),
									'slug' 					=> 'anim_menus_delay',
									'min'					=> '0',
									'max'					=> '10000',
									'step'					=> '10',
									'postfix'				=> '<i> (milliseconds)</i>',
									'width_px'				=> '60',
									'options_name'			=> 'canon_options_appearance',
								)); 

							?>

						</table>







					<?php submit_button(); ?>
				</form>
			</div> <!-- end table container -->	

	
		</div>

	</div>