	<div class="wrap">

		<div id="icon-themes" class="icon32"></div>

		<h2><?php printf( "%s %s - %s", esc_attr(wp_get_theme()->Name), esc_html__("Settings", "loc_canon_venuex"), esc_html__("Header & Footer", "loc_canon_venuex") ); ?></h2>

		<?php 
			//delete_option('canon_options_frame');
			$canon_options_frame = get_option('canon_options_frame'); 

			// ARRAY VALUES
			$canon_options_frame['social_links'] = array_values($canon_options_frame['social_links']);
			update_option('canon_options_frame', $canon_options_frame);

			// GET ARRAY OF REGISTERED SIDEBARS
			$registered_sidebars_array = array();
			foreach ($GLOBALS['wp_registered_sidebars'] as $key => $value) { array_push($registered_sidebars_array, $value); }

			// var_dump($canon_options_frame);
		?>

		<br>
		
		<div class="options_wrapper canon-options">
		
			<div class="table_container">

				<form method="post" action="options.php" enctype="multipart/form-data">
					<?php settings_fields('group_canon_options_frame'); ?>				<!-- very important to add these two functions as they mediate what wordpress generates automatically from the functions.php -->
					<?php do_settings_sections('handle_canon_options_frame'); ?>		


					<?php submit_button(); ?>
					
					<!-- 

						INDEX

						HEADER: GENERAL
						HEADER LAYOUT
						MAIN HEADER ADJUSTMENTS
						HEADER ELEMENT: LOGO
						HEADER ELEMENT: BACKGROUND IMAGE
						HEADER ELEMENT: BANNER
						HEADER ELEMENT: HEADER TEXT
						SOCIAL LINKS 
						HEADER ELEMENT: TOOLBAR
						HEADER ELEMENT: COUNTDOWN
						FOOTER
						WIDGETIZED FOOTER

					-->


					<!-- 
					--------------------------------------------------------------------------
						HEADER: GENERAL
				    -------------------------------------------------------------------------- 
					-->

						<h3><?php esc_html_e("Header: General Settings", "loc_canon_venuex"); ?> <img src="<?php echo get_template_directory_uri() . '/img/help.png' ?>"></h3>

						<div class='contextual-help'>
							<?php 

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Boxed header content', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Make header content boxed.', 'loc_canon_venuex'),
									),
								)); 

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Sticky headers', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Make the headers stick to the top of the page when scrolling down.', 'loc_canon_venuex'),
									),
								)); 

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Header opacity', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Set the opacity of each header section. Values must be between 0 and 1. 0 is completely transparent. 1 is completely solid/opaque.', 'loc_canon_venuex'),
									),
								)); 

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Sticky header shadow', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Apply a shadow to the sticky header when scrolling down.', 'loc_canon_venuex'),
									),
								)); 

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Stickyness in responsive mode', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Turn off stickyness in responsive mode. Choose the viewport size under which stickyness will be disabled. The optimal setting depends on your content. If you have many sticky elements or tall sticky elements you might want to turn off stickyness at a higher viewport size to avoid the sticky elements taking up the whole viewport.', 'loc_canon_venuex'),
									),
								)); 

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Add search button to primary or secondary menu', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Select this to put a search button at the end of your primary or secondary menu', 'loc_canon_venuex'),
									),
								)); 

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Scroll-to menu items', 'loc_canon_venuex'),
									'content' 				=> array(
										wp_kses_post(__('Usually the current page user is on is highlighted in the menu. This however may not be desirable when putting scroll-to links in the menu as this can lead to multiple menu items being highlighted at the same time. Use this option to determine how scroll-to links in the menu are highlighted.<br>', 'loc_canon_venuex')),
										wp_kses_post(__('<i><strong>Act normal</strong></i>: Scroll-to links act as normal menu items which means they are highlighted when on current page.', 'loc_canon_venuex')),
										wp_kses_post(__('<i><strong>Auto highlight</strong></i>: Scroll-to links in the menu are highlighted automatically when the scroll-to target element scrolls into view.', 'loc_canon_venuex')),
										wp_kses_post(__('<i><strong>Disable highlight</strong></i>: Scroll-to links are never highlighted.', 'loc_canon_venuex')),

									),
								)); 
	
							 ?>		

						</div>

						<table class='form-table'>


							<?php 

								fw_option(array(
									'type'					=> 'checkbox',
									'title' 				=> esc_html__('Boxed header content', 'loc_canon_venuex'),
									'slug' 					=> 'use_boxed_header',
									'options_name'			=> 'canon_options_frame',
								)); 

								fw_option(array(
									'type'					=> 'checkbox',
									'title' 				=> esc_html__('Sticky pre-header', 'loc_canon_venuex'),
									'slug' 					=> 'use_sticky_preheader',
									'options_name'			=> 'canon_options_frame',
								)); 

								fw_option(array(
									'type'					=> 'checkbox',
									'title' 				=> esc_html__('Sticky main header', 'loc_canon_venuex'),
									'slug' 					=> 'use_sticky_header',
									'options_name'			=> 'canon_options_frame',
								)); 

								fw_option(array(
									'type'					=> 'checkbox',
									'title' 				=> esc_html__('Sticky post-header', 'loc_canon_venuex'),
									'slug' 					=> 'use_sticky_postheader',
									'options_name'			=> 'canon_options_frame',
								)); 

								fw_option(array(
									'type'					=> 'number',
									'title' 				=> esc_html__('Pre-header opacity', 'loc_canon_venuex'),
									'slug' 					=> 'preheader_opacity',
									'min'					=> '0',										// optional
									'max'					=> '1',										// optional
									'step'					=> '0.05',									// optional
									'width_px'				=> '60',									// optional
									'options_name'			=> 'canon_options_frame',
								)); 

								fw_option(array(
									'type'					=> 'number',
									'title' 				=> esc_html__('Main header opacity', 'loc_canon_venuex'),
									'slug' 					=> 'header_opacity',
									'min'					=> '0',										// optional
									'max'					=> '1',										// optional
									'step'					=> '0.05',									// optional
									'width_px'				=> '60',									// optional
									'options_name'			=> 'canon_options_frame',
								)); 

								fw_option(array(
									'type'					=> 'number',
									'title' 				=> esc_html__('Post-header opacity', 'loc_canon_venuex'),
									'slug' 					=> 'postheader_opacity',
									'min'					=> '0',										// optional
									'max'					=> '1',										// optional
									'step'					=> '0.05',									// optional
									'width_px'				=> '60',									// optional
									'options_name'			=> 'canon_options_frame',
								)); 

								fw_option(array(
									'type'					=> 'checkbox',
									'title' 				=> esc_html__('Sticky header shadow', 'loc_canon_venuex'),
									'slug' 					=> 'use_sticky_shadow',
									'options_name'			=> 'canon_options_frame',
								)); 

								fw_option(array(
									'type'					=> 'select',
									'title' 				=> esc_html__('Stickyness in responsive mode', 'loc_canon_venuex'),
									'slug' 					=> 'sticky_turn_off_width',
									'select_options'		=> array(
										'0'					=> esc_html__('Stickyness is always on', 'loc_canon_venuex'),
										'768'				=> esc_html__('Turn off @ viewport width below 768px', 'loc_canon_venuex'),
										'600'				=> esc_html__('Turn off @ viewport width below 600px', 'loc_canon_venuex'),
										'480'				=> esc_html__('Turn off @ viewport width below 480px', 'loc_canon_venuex'),
									),
									'options_name'			=> 'canon_options_frame',
								)); 

								fw_option(array(
									'type'					=> 'checkbox',
									'title' 				=> esc_html__('Add search button to primary menu', 'loc_canon_venuex'),
									'slug' 					=> 'add_search_btn_to_primary',
									'options_name'			=> 'canon_options_frame',
								)); 

								fw_option(array(
									'type'					=> 'checkbox',
									'title' 				=> esc_html__('Add search button to secondary menu', 'loc_canon_venuex'),
									'slug' 					=> 'add_search_btn_to_secondary',
									'options_name'			=> 'canon_options_frame',
								)); 

								fw_option(array(
									'type'					=> 'select',
									'title' 				=> esc_html__('Scroll-to menu items', 'loc_canon_venuex'),
									'slug' 					=> 'status_of_scroll_to_menu_items',
									'select_options'		=> array(
										'normal'				=> esc_html__('Act normal', 'loc_canon_venuex'),
										'auto'					=> esc_html__('Auto highlight', 'loc_canon_venuex'),
										'disable'				=> esc_html__('Disable highlight', 'loc_canon_venuex'),
									),
									'options_name'			=> 'canon_options_frame',
								)); 

							?>

						</table>

					<!-- 
					--------------------------------------------------------------------------
						HEADER LAYOUT
				    -------------------------------------------------------------------------- 
					-->

						<h3><?php esc_html_e("Header Builder", "loc_canon_venuex"); ?> <img src="<?php echo get_template_directory_uri() . '/img/help.png' ?>"></h3>

						<div class='contextual-help'>
							<?php 


								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Header builder', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Build your own header. Select elements for each header section using the selects. Settings for each element can be found below.', 'loc_canon_venuex'),
										esc_html__('Notice that some elements are only available for certain spots e.g. logo which can only be placed in the main header etc.', 'loc_canon_venuex'),
									),
								)); 

								fw_option_help(array(
									'type'					=> 'ul',
									'title' 				=> esc_html__('Available elements', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Primary menu.', 'loc_canon_venuex'),
										esc_html__('Secondary menu', 'loc_canon_venuex'),
										esc_html__('Logo', 'loc_canon_venuex'),
										esc_html__('Header image.', 'loc_canon_venuex'),
										esc_html__('Social icons', 'loc_canon_venuex'),
										esc_html__('Text', 'loc_canon_venuex'),
										esc_html__('Toolbar (search button)', 'loc_canon_venuex'),
										esc_html__('Ad banner', 'loc_canon_venuex'),
										esc_html__('Countdown', 'loc_canon_venuex'),
										esc_html__('Breadcrumbs', 'loc_canon_venuex'),
									),
								)); 


							 ?>		

						</div>


						<table class='form-table header-layout'>

						<!-- PRE HEADER -->

							<?php 

								fw_option(array(
									'type'					=> 'select',
									'title' 				=> esc_html__('Pre-header', 'loc_canon_venuex'),
									'slug' 					=> 'pre_header_layout',
									'colspan'				=> 2,
									'select_options'		=> array(
										'off'							=> esc_html__('Off', 'loc_canon_venuex'),
										'pre_custom_center'				=> esc_html__('Custom Center', 'loc_canon_venuex'),
										'pre_custom_left_right'			=> esc_html__('Custom Left + Right', 'loc_canon_venuex'),
										'pre_image'						=> esc_html__('Background image header', 'loc_canon_venuex'),
									),
									'options_name'			=> 'canon_options_frame',
								)); 

							?>

						<!-- PRE HEADER: CUSTOM CENTER -->

							<tr class="dynamic_option" data-listen_to="#pre_header_layout" data-listen_for="pre_custom_center">
								<th></th>
								<td colspan="2">
					
									<select id="pre_custom_center" name="canon_options_frame[pre_custom_center]"> 

						     			<option value="off" <?php if (isset($canon_options_frame['pre_custom_center'])) {if ($canon_options_frame['pre_custom_center'] == 'off') echo "selected='selected'";} ?>><?php esc_html_e("Off", "loc_canon_venuex"); ?></option> 
						     			<option value="primary" <?php if (isset($canon_options_frame['pre_custom_center'])) {if ($canon_options_frame['pre_custom_center'] == 'primary') echo "selected='selected'";} ?>><?php esc_html_e("Primary Menu", "loc_canon_venuex"); ?></option> 
						     			<option value="secondary" <?php if (isset($canon_options_frame['pre_custom_center'])) {if ($canon_options_frame['pre_custom_center'] == 'secondary') echo "selected='selected'";} ?>><?php esc_html_e("Secondary Menu", "loc_canon_venuex"); ?></option> 
						     			<option value="social" <?php if (isset($canon_options_frame['pre_custom_center'])) {if ($canon_options_frame['pre_custom_center'] == 'social') echo "selected='selected'";} ?>><?php esc_html_e("Social Icons", "loc_canon_venuex"); ?></option> 
						     			<option value="text" <?php if (isset($canon_options_frame['pre_custom_center'])) {if ($canon_options_frame['pre_custom_center'] == 'text') echo "selected='selected'";} ?>><?php esc_html_e("Text", "loc_canon_venuex"); ?></option> 
						     			<option value="toolbar" <?php if (isset($canon_options_frame['pre_custom_center'])) {if ($canon_options_frame['pre_custom_center'] == 'toolbar') echo "selected='selected'";} ?>><?php esc_html_e("Toolbar", "loc_canon_venuex"); ?></option> 
						     			<option value="countdown" <?php if (isset($canon_options_frame['pre_custom_center'])) {if ($canon_options_frame['pre_custom_center'] == 'countdown') echo "selected='selected'";} ?>><?php esc_html_e("Countdown", "loc_canon_venuex"); ?></option> 
						     			<option value="breadcrumbs" <?php if (isset($canon_options_frame['pre_custom_center'])) {if ($canon_options_frame['pre_custom_center'] == 'breadcrumbs') echo "selected='selected'";} ?>><?php esc_html_e("Breadcrumbs", "loc_canon_venuex"); ?></option> 
						     	
									</select> 
					
							</tr>

						<!-- PRE HEADER: CUSTOM LEFT RIGHT -->

							<tr class="dynamic_option" data-listen_to="#pre_header_layout" data-listen_for="pre_custom_left_right">
								<th></th>
								<td>
					
									<select id="pre_custom_left" name="canon_options_frame[pre_custom_left]"> 

						     			<option value="off" <?php if (isset($canon_options_frame['pre_custom_left'])) {if ($canon_options_frame['pre_custom_left'] == 'off') echo "selected='selected'";} ?>><?php esc_html_e("Off", "loc_canon_venuex"); ?></option> 
						     			<option value="primary" <?php if (isset($canon_options_frame['pre_custom_left'])) {if ($canon_options_frame['pre_custom_left'] == 'primary') echo "selected='selected'";} ?>><?php esc_html_e("Primary Menu", "loc_canon_venuex"); ?></option> 
						     			<option value="secondary" <?php if (isset($canon_options_frame['pre_custom_left'])) {if ($canon_options_frame['pre_custom_left'] == 'secondary') echo "selected='selected'";} ?>><?php esc_html_e("Secondary Menu", "loc_canon_venuex"); ?></option> 
						     			<option value="social" <?php if (isset($canon_options_frame['pre_custom_left'])) {if ($canon_options_frame['pre_custom_left'] == 'social') echo "selected='selected'";} ?>><?php esc_html_e("Social Icons", "loc_canon_venuex"); ?></option> 
						     			<option value="text" <?php if (isset($canon_options_frame['pre_custom_left'])) {if ($canon_options_frame['pre_custom_left'] == 'text') echo "selected='selected'";} ?>><?php esc_html_e("Text", "loc_canon_venuex"); ?></option> 
						     			<option value="toolbar" <?php if (isset($canon_options_frame['pre_custom_left'])) {if ($canon_options_frame['pre_custom_left'] == 'toolbar') echo "selected='selected'";} ?>><?php esc_html_e("Toolbar", "loc_canon_venuex"); ?></option> 
						     			<option value="countdown" <?php if (isset($canon_options_frame['pre_custom_left'])) {if ($canon_options_frame['pre_custom_left'] == 'countdown') echo "selected='selected'";} ?>><?php esc_html_e("Countdown", "loc_canon_venuex"); ?></option> 
						     			<option value="breadcrumbs" <?php if (isset($canon_options_frame['pre_custom_left'])) {if ($canon_options_frame['pre_custom_left'] == 'breadcrumbs') echo "selected='selected'";} ?>><?php esc_html_e("Breadcrumbs", "loc_canon_venuex"); ?></option> 

									</select> 
					

								</td>
								<td>

									<select id="pre_custom_right" name="canon_options_frame[pre_custom_right]"> 

						     			<option value="off" <?php if (isset($canon_options_frame['pre_custom_right'])) {if ($canon_options_frame['pre_custom_right'] == 'off') echo "selected='selected'";} ?>><?php esc_html_e("Off", "loc_canon_venuex"); ?></option> 
						     			<option value="primary" <?php if (isset($canon_options_frame['pre_custom_right'])) {if ($canon_options_frame['pre_custom_right'] == 'primary') echo "selected='selected'";} ?>><?php esc_html_e("Primary Menu", "loc_canon_venuex"); ?></option> 
						     			<option value="secondary" <?php if (isset($canon_options_frame['pre_custom_right'])) {if ($canon_options_frame['pre_custom_right'] == 'secondary') echo "selected='selected'";} ?>><?php esc_html_e("Secondary Menu", "loc_canon_venuex"); ?></option> 
						     			<option value="social" <?php if (isset($canon_options_frame['pre_custom_right'])) {if ($canon_options_frame['pre_custom_right'] == 'social') echo "selected='selected'";} ?>><?php esc_html_e("Social Icons", "loc_canon_venuex"); ?></option> 
						     			<option value="text" <?php if (isset($canon_options_frame['pre_custom_right'])) {if ($canon_options_frame['pre_custom_right'] == 'text') echo "selected='selected'";} ?>><?php esc_html_e("Text", "loc_canon_venuex"); ?></option> 
						     			<option value="toolbar" <?php if (isset($canon_options_frame['pre_custom_right'])) {if ($canon_options_frame['pre_custom_right'] == 'toolbar') echo "selected='selected'";} ?>><?php esc_html_e("Toolbar", "loc_canon_venuex"); ?></option> 
						     			<option value="countdown" <?php if (isset($canon_options_frame['pre_custom_right'])) {if ($canon_options_frame['pre_custom_right'] == 'countdown') echo "selected='selected'";} ?>><?php esc_html_e("Countdown", "loc_canon_venuex"); ?></option> 
						     			<option value="breadcrumbs" <?php if (isset($canon_options_frame['pre_custom_right'])) {if ($canon_options_frame['pre_custom_right'] == 'breadcrumbs') echo "selected='selected'";} ?>><?php esc_html_e("Breadcrumbs", "loc_canon_venuex"); ?></option> 
						     	
									</select> 

 								</td>
							</tr>


						<!-- MAIN HEADER -->

							<?php

								fw_option(array(
									'type'					=> 'select',
									'title' 				=> esc_html__('Main header', 'loc_canon_venuex'),
									'slug' 					=> 'main_header_layout',
									'colspan'				=> 2,
									'select_options'		=> array(
										'off'							=> esc_html__('Off', 'loc_canon_venuex'),
										'main_custom_center'			=> esc_html__('Custom Center', 'loc_canon_venuex'),
										'main_custom_left_right'		=> esc_html__('Custom Left + Right', 'loc_canon_venuex'),
										'main_image'					=> esc_html__('Background image header', 'loc_canon_venuex'),
									),
									'options_name'			=> 'canon_options_frame',
								)); 

							?>

						<!-- MAIN HEADER: CUSTOM CENTER -->

							<tr class="dynamic_option" data-listen_to="#main_header_layout" data-listen_for="main_custom_center">
								<th></th>
								<td colspan="2">
					
									<select id="main_custom_center" name="canon_options_frame[main_custom_center]"> 

						     			<option value="off" <?php if (isset($canon_options_frame['main_custom_center'])) {if ($canon_options_frame['main_custom_center'] == 'off') echo "selected='selected'";} ?>><?php esc_html_e("Off", "loc_canon_venuex"); ?></option> 
						     			<option value="logo" <?php if (isset($canon_options_frame['main_custom_center'])) {if ($canon_options_frame['main_custom_center'] == 'logo') echo "selected='selected'";} ?>><?php esc_html_e("Logo", "loc_canon_venuex"); ?></option> 
						     			<option value="primary" <?php if (isset($canon_options_frame['main_custom_center'])) {if ($canon_options_frame['main_custom_center'] == 'primary') echo "selected='selected'";} ?>><?php esc_html_e("Primary Menu", "loc_canon_venuex"); ?></option> 
						     			<option value="secondary" <?php if (isset($canon_options_frame['main_custom_center'])) {if ($canon_options_frame['main_custom_center'] == 'secondary') echo "selected='selected'";} ?>><?php esc_html_e("Secondary Menu", "loc_canon_venuex"); ?></option> 
						     			<option value="social" <?php if (isset($canon_options_frame['main_custom_center'])) {if ($canon_options_frame['main_custom_center'] == 'social') echo "selected='selected'";} ?>><?php esc_html_e("Social Icons", "loc_canon_venuex"); ?></option> 
						     			<option value="text" <?php if (isset($canon_options_frame['main_custom_center'])) {if ($canon_options_frame['main_custom_center'] == 'text') echo "selected='selected'";} ?>><?php esc_html_e("Text", "loc_canon_venuex"); ?></option> 
						     			<option value="toolbar" <?php if (isset($canon_options_frame['main_custom_center'])) {if ($canon_options_frame['main_custom_center'] == 'toolbar') echo "selected='selected'";} ?>><?php esc_html_e("Toolbar", "loc_canon_venuex"); ?></option> 
						     			<option value="banner" <?php if (isset($canon_options_frame['main_custom_center'])) {if ($canon_options_frame['main_custom_center'] == 'banner') echo "selected='selected'";} ?>><?php esc_html_e("Ad Banner", "loc_canon_venuex"); ?></option> 
						     			<option value="countdown" <?php if (isset($canon_options_frame['main_custom_center'])) {if ($canon_options_frame['main_custom_center'] == 'countdown') echo "selected='selected'";} ?>><?php esc_html_e("Countdown", "loc_canon_venuex"); ?></option> 
						     			<option value="breadcrumbs" <?php if (isset($canon_options_frame['main_custom_center'])) {if ($canon_options_frame['main_custom_center'] == 'breadcrumbs') echo "selected='selected'";} ?>><?php esc_html_e("Breadcrumbs", "loc_canon_venuex"); ?></option> 
						     	
									</select> 
					
							</tr>

						<!-- MAIN HEADER: CUSTOM LEFT RIGHT -->

							<tr class="dynamic_option" data-listen_to="#main_header_layout" data-listen_for="main_custom_left_right">
								<th></th>
								<td>
					
									<select id="main_custom_left" name="canon_options_frame[main_custom_left]"> 

						     			<option value="off" <?php if (isset($canon_options_frame['main_custom_left'])) {if ($canon_options_frame['main_custom_left'] == 'off') echo "selected='selected'";} ?>><?php esc_html_e("Off", "loc_canon_venuex"); ?></option> 
						     			<option value="logo" <?php if (isset($canon_options_frame['main_custom_left'])) {if ($canon_options_frame['main_custom_left'] == 'logo') echo "selected='selected'";} ?>><?php esc_html_e("Logo", "loc_canon_venuex"); ?></option> 
						     			<option value="primary" <?php if (isset($canon_options_frame['main_custom_left'])) {if ($canon_options_frame['main_custom_left'] == 'primary') echo "selected='selected'";} ?>><?php esc_html_e("Primary Menu", "loc_canon_venuex"); ?></option> 
						     			<option value="secondary" <?php if (isset($canon_options_frame['main_custom_left'])) {if ($canon_options_frame['main_custom_left'] == 'secondary') echo "selected='selected'";} ?>><?php esc_html_e("Secondary Menu", "loc_canon_venuex"); ?></option> 
						     			<option value="social" <?php if (isset($canon_options_frame['main_custom_left'])) {if ($canon_options_frame['main_custom_left'] == 'social') echo "selected='selected'";} ?>><?php esc_html_e("Social Icons", "loc_canon_venuex"); ?></option> 
						     			<option value="text" <?php if (isset($canon_options_frame['main_custom_left'])) {if ($canon_options_frame['main_custom_left'] == 'text') echo "selected='selected'";} ?>><?php esc_html_e("Text", "loc_canon_venuex"); ?></option> 
						     			<option value="toolbar" <?php if (isset($canon_options_frame['main_custom_left'])) {if ($canon_options_frame['main_custom_left'] == 'toolbar') echo "selected='selected'";} ?>><?php esc_html_e("Toolbar", "loc_canon_venuex"); ?></option> 
						     			<option value="banner" <?php if (isset($canon_options_frame['main_custom_left'])) {if ($canon_options_frame['main_custom_left'] == 'banner') echo "selected='selected'";} ?>><?php esc_html_e("Ad Banner", "loc_canon_venuex"); ?></option> 
						     			<option value="countdown" <?php if (isset($canon_options_frame['main_custom_left'])) {if ($canon_options_frame['main_custom_left'] == 'countdown') echo "selected='selected'";} ?>><?php esc_html_e("Countdown", "loc_canon_venuex"); ?></option> 
						     			<option value="breadcrumbs" <?php if (isset($canon_options_frame['main_custom_left'])) {if ($canon_options_frame['main_custom_left'] == 'breadcrumbs') echo "selected='selected'";} ?>><?php esc_html_e("Breadcrumbs", "loc_canon_venuex"); ?></option> 
						     	
									</select> 
					

								</td>
								<td>

									<select id="main_custom_right" name="canon_options_frame[main_custom_right]"> 

						     			<option value="off" <?php if (isset($canon_options_frame['main_custom_right'])) {if ($canon_options_frame['main_custom_right'] == 'off') echo "selected='selected'";} ?>><?php esc_html_e("Off", "loc_canon_venuex"); ?></option> 
						     			<option value="logo" <?php if (isset($canon_options_frame['main_custom_right'])) {if ($canon_options_frame['main_custom_right'] == 'logo') echo "selected='selected'";} ?>><?php esc_html_e("Logo", "loc_canon_venuex"); ?></option> 
						     			<option value="primary" <?php if (isset($canon_options_frame['main_custom_right'])) {if ($canon_options_frame['main_custom_right'] == 'primary') echo "selected='selected'";} ?>><?php esc_html_e("Primary Menu", "loc_canon_venuex"); ?></option> 
						     			<option value="secondary" <?php if (isset($canon_options_frame['main_custom_right'])) {if ($canon_options_frame['main_custom_right'] == 'secondary') echo "selected='selected'";} ?>><?php esc_html_e("Secondary Menu", "loc_canon_venuex"); ?></option> 
						     			<option value="social" <?php if (isset($canon_options_frame['main_custom_right'])) {if ($canon_options_frame['main_custom_right'] == 'social') echo "selected='selected'";} ?>><?php esc_html_e("Social Icons", "loc_canon_venuex"); ?></option> 
						     			<option value="text" <?php if (isset($canon_options_frame['main_custom_right'])) {if ($canon_options_frame['main_custom_right'] == 'text') echo "selected='selected'";} ?>><?php esc_html_e("Text", "loc_canon_venuex"); ?></option> 
						     			<option value="toolbar" <?php if (isset($canon_options_frame['main_custom_right'])) {if ($canon_options_frame['main_custom_right'] == 'toolbar') echo "selected='selected'";} ?>><?php esc_html_e("Toolbar", "loc_canon_venuex"); ?></option> 
						     			<option value="banner" <?php if (isset($canon_options_frame['main_custom_right'])) {if ($canon_options_frame['main_custom_right'] == 'banner') echo "selected='selected'";} ?>><?php esc_html_e("Ad Banner", "loc_canon_venuex"); ?></option> 
						     			<option value="countdown" <?php if (isset($canon_options_frame['main_custom_right'])) {if ($canon_options_frame['main_custom_right'] == 'countdown') echo "selected='selected'";} ?>><?php esc_html_e("Countdown", "loc_canon_venuex"); ?></option> 
						     			<option value="breadcrumbs" <?php if (isset($canon_options_frame['main_custom_right'])) {if ($canon_options_frame['main_custom_right'] == 'breadcrumbs') echo "selected='selected'";} ?>><?php esc_html_e("Breadcrumbs", "loc_canon_venuex"); ?></option> 
						     	
									</select> 

 								</td>
							</tr>

						<!-- POST HEADER -->

							<?php							
								
								fw_option(array(
									'type'					=> 'select',
									'title' 				=> esc_html__('Post-header', 'loc_canon_venuex'),
									'slug' 					=> 'post_header_layout',
									'colspan'				=> 2,
									'select_options'		=> array(
										'off'						=> esc_html__('Off', 'loc_canon_venuex'),
										'post_custom_center'		=> esc_html__('Custom Center', 'loc_canon_venuex'),
										'post_custom_left_right'	=> esc_html__('Custom Left + Right', 'loc_canon_venuex'),
									),
									'options_name'			=> 'canon_options_frame',
								)); 
								
							?>

						<!-- POST HEADER: CUSTOM CENTER -->

							<tr class="dynamic_option" data-listen_to="#post_header_layout" data-listen_for="post_custom_center">
								<th></th>
								<td colspan="2">
					
									<select id="post_custom_center" name="canon_options_frame[post_custom_center]"> 

						     			<option value="off" <?php if (isset($canon_options_frame['post_custom_center'])) {if ($canon_options_frame['post_custom_center'] == 'off') echo "selected='selected'";} ?>><?php esc_html_e("Off", "loc_canon_venuex"); ?></option> 
						     			<option value="primary" <?php if (isset($canon_options_frame['post_custom_center'])) {if ($canon_options_frame['post_custom_center'] == 'primary') echo "selected='selected'";} ?>><?php esc_html_e("Primary Menu", "loc_canon_venuex"); ?></option> 
						     			<option value="secondary" <?php if (isset($canon_options_frame['post_custom_center'])) {if ($canon_options_frame['post_custom_center'] == 'secondary') echo "selected='selected'";} ?>><?php esc_html_e("Secondary Menu", "loc_canon_venuex"); ?></option> 
						     			<option value="social" <?php if (isset($canon_options_frame['post_custom_center'])) {if ($canon_options_frame['post_custom_center'] == 'social') echo "selected='selected'";} ?>><?php esc_html_e("Social Icons", "loc_canon_venuex"); ?></option> 
						     			<option value="text" <?php if (isset($canon_options_frame['post_custom_center'])) {if ($canon_options_frame['post_custom_center'] == 'text') echo "selected='selected'";} ?>><?php esc_html_e("Text", "loc_canon_venuex"); ?></option> 
						     			<option value="toolbar" <?php if (isset($canon_options_frame['post_custom_center'])) {if ($canon_options_frame['post_custom_center'] == 'toolbar') echo "selected='selected'";} ?>><?php esc_html_e("Toolbar", "loc_canon_venuex"); ?></option> 
						     			<option value="countdown" <?php if (isset($canon_options_frame['post_custom_center'])) {if ($canon_options_frame['post_custom_center'] == 'countdown') echo "selected='selected'";} ?>><?php esc_html_e("Countdown", "loc_canon_venuex"); ?></option> 
						     			<option value="breadcrumbs" <?php if (isset($canon_options_frame['post_custom_center'])) {if ($canon_options_frame['post_custom_center'] == 'breadcrumbs') echo "selected='selected'";} ?>><?php esc_html_e("Breadcrumbs", "loc_canon_venuex"); ?></option> 
						     	
									</select> 
					
							</tr>

						<!-- POST HEADER: CUSTOM LEFT RIGHT -->

							<tr class="dynamic_option" data-listen_to="#post_header_layout" data-listen_for="post_custom_left_right">
								<th></th>
								<td>
					
									<select id="post_custom_left" name="canon_options_frame[post_custom_left]"> 

						     			<option value="off" <?php if (isset($canon_options_frame['post_custom_left'])) {if ($canon_options_frame['post_custom_left'] == 'off') echo "selected='selected'";} ?>><?php esc_html_e("Off", "loc_canon_venuex"); ?></option> 
						     			<option value="primary" <?php if (isset($canon_options_frame['post_custom_left'])) {if ($canon_options_frame['post_custom_left'] == 'primary') echo "selected='selected'";} ?>><?php esc_html_e("Primary Menu", "loc_canon_venuex"); ?></option> 
						     			<option value="secondary" <?php if (isset($canon_options_frame['post_custom_left'])) {if ($canon_options_frame['post_custom_left'] == 'secondary') echo "selected='selected'";} ?>><?php esc_html_e("Secondary Menu", "loc_canon_venuex"); ?></option> 
						     			<option value="social" <?php if (isset($canon_options_frame['post_custom_left'])) {if ($canon_options_frame['post_custom_left'] == 'social') echo "selected='selected'";} ?>><?php esc_html_e("Social Icons", "loc_canon_venuex"); ?></option> 
						     			<option value="text" <?php if (isset($canon_options_frame['post_custom_left'])) {if ($canon_options_frame['post_custom_left'] == 'text') echo "selected='selected'";} ?>><?php esc_html_e("Text", "loc_canon_venuex"); ?></option> 
						     			<option value="toolbar" <?php if (isset($canon_options_frame['post_custom_left'])) {if ($canon_options_frame['post_custom_left'] == 'toolbar') echo "selected='selected'";} ?>><?php esc_html_e("Toolbar", "loc_canon_venuex"); ?></option> 
						     			<option value="countdown" <?php if (isset($canon_options_frame['post_custom_left'])) {if ($canon_options_frame['post_custom_left'] == 'countdown') echo "selected='selected'";} ?>><?php esc_html_e("Countdown", "loc_canon_venuex"); ?></option> 
						     			<option value="breadcrumbs" <?php if (isset($canon_options_frame['post_custom_left'])) {if ($canon_options_frame['post_custom_left'] == 'breadcrumbs') echo "selected='selected'";} ?>><?php esc_html_e("Breadcrumbs", "loc_canon_venuex"); ?></option> 
						     	
									</select> 
					

								</td>
								<td>

									<select id="post_custom_right" name="canon_options_frame[post_custom_right]"> 

						     			<option value="off" <?php if (isset($canon_options_frame['post_custom_right'])) {if ($canon_options_frame['post_custom_right'] == 'off') echo "selected='selected'";} ?>><?php esc_html_e("Off", "loc_canon_venuex"); ?></option> 
						     			<option value="primary" <?php if (isset($canon_options_frame['post_custom_right'])) {if ($canon_options_frame['post_custom_right'] == 'primary') echo "selected='selected'";} ?>><?php esc_html_e("Primary Menu", "loc_canon_venuex"); ?></option> 
						     			<option value="secondary" <?php if (isset($canon_options_frame['post_custom_right'])) {if ($canon_options_frame['post_custom_right'] == 'secondary') echo "selected='selected'";} ?>><?php esc_html_e("Secondary Menu", "loc_canon_venuex"); ?></option> 
						     			<option value="social" <?php if (isset($canon_options_frame['post_custom_right'])) {if ($canon_options_frame['post_custom_right'] == 'social') echo "selected='selected'";} ?>><?php esc_html_e("Social Icons", "loc_canon_venuex"); ?></option> 
						     			<option value="text" <?php if (isset($canon_options_frame['post_custom_right'])) {if ($canon_options_frame['post_custom_right'] == 'text') echo "selected='selected'";} ?>><?php esc_html_e("Text", "loc_canon_venuex"); ?></option> 
						     			<option value="toolbar" <?php if (isset($canon_options_frame['post_custom_right'])) {if ($canon_options_frame['post_custom_right'] == 'toolbar') echo "selected='selected'";} ?>><?php esc_html_e("Toolbar", "loc_canon_venuex"); ?></option> 
						     			<option value="countdown" <?php if (isset($canon_options_frame['post_custom_right'])) {if ($canon_options_frame['post_custom_right'] == 'countdown') echo "selected='selected'";} ?>><?php esc_html_e("Countdown", "loc_canon_venuex"); ?></option> 
						     			<option value="breadcrumbs" <?php if (isset($canon_options_frame['post_custom_right'])) {if ($canon_options_frame['post_custom_right'] == 'breadcrumbs') echo "selected='selected'";} ?>><?php esc_html_e("Breadcrumbs", "loc_canon_venuex"); ?></option> 
						     	
									</select> 

 								</td>
							</tr>

						</table>


					<!-- 
					--------------------------------------------------------------------------
						MAIN HEADER ADJUSTMENTS
				    -------------------------------------------------------------------------- 
					-->

						<h3><?php esc_html_e("Main Header Adjustments", "loc_canon_venuex"); ?> <img src="<?php echo get_template_directory_uri() . '/img/help.png' ?>"></h3>

						<div class='contextual-help'>
							<?php 

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Padding top & Padding bottom', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Used to position your header elements.', 'loc_canon_venuex'),
										esc_html__('Increase padding top to create space above the header elements.', 'loc_canon_venuex'),
										esc_html__('Increase padding bottom to create space below the header elements.', 'loc_canon_venuex'),
									),
								)); 

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Adjust left element relative position', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('You can fine adjust the left element position. Values are pixels from top-left.', 'loc_canon_venuex'),
									),
								)); 

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Adjust right element relative position', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('You can fine adjust the right element position. Values are pixels from top-right.', 'loc_canon_venuex'),
									),
								)); 

							 ?>		

						</div>

						<table class='form-table header-layout'>

							<?php 

								fw_option(array(
									'type'					=> 'number',
									'title' 				=> esc_html__('Header padding top', 'loc_canon_venuex'),
									'slug' 					=> 'header_padding_top',
									'min'					=> '0',										// optional
									'max'					=> '1000',									// optional
									'width_px'				=> '60',									// optional
									'postfix'				=> '<i>(pixels)</i>',						// optional
									'options_name'			=> 'canon_options_frame',
								)); 

								fw_option(array(
									'type'					=> 'number',
									'title' 				=> esc_html__('Header padding bottom', 'loc_canon_venuex'),
									'slug' 					=> 'header_padding_bottom',
									'min'					=> '0',										// optional
									'max'					=> '1000',									// optional
									'width_px'				=> '60',									// optional
									'postfix'				=> '<i>(pixels)</i>',						// optional
									'options_name'			=> 'canon_options_frame',
								)); 

							 ?>		


							<tr valign='top'>
								<th scope='row'><?php esc_html_e("Adjust left element relative position", "loc_canon_venuex"); ?></th>
								<td>
									<input 
										type='number' 
										id='pos_left_element_top' 
										name='canon_options_frame[pos_left_element_top]' 
										min='-1000'
										max='1000'
										style='width: 60px;'
										value='<?php if (isset($canon_options_frame['pos_left_element_top'])) echo esc_attr($canon_options_frame['pos_left_element_top']); ?>'
									> <i>(pixels from top)</i>
									<input 
										type='number' 
										id='pos_left_element_left' 
										name='canon_options_frame[pos_left_element_left]' 
										min='-1000'
										max='1000'
										style='width: 60px;'
										value='<?php if (isset($canon_options_frame['pos_left_element_left'])) echo esc_attr($canon_options_frame['pos_left_element_left']); ?>'
									> <i>(pixels from left)</i>
								</td> 
							</tr>

							<tr valign='top'>
								<th scope='row'><?php esc_html_e("Adjust right element relative position", "loc_canon_venuex"); ?></th>
								<td>
									<input 
										type='number' 
										id='pos_right_element_top' 
										name='canon_options_frame[pos_right_element_top]' 
										min='-1000'
										max='1000'
										style='width: 60px;'
										value='<?php if (isset($canon_options_frame['pos_right_element_top'])) echo esc_attr($canon_options_frame['pos_right_element_top']); ?>'
									> <i>(pixels from top)</i>
									<input 
										type='number' 
										id='pos_right_element_right' 
										name='canon_options_frame[pos_right_element_right]' 
										min='-1000'
										max='1000'
										style='width: 60px;'
										value='<?php if (isset($canon_options_frame['pos_right_element_right'])) echo esc_attr($canon_options_frame['pos_right_element_right']); ?>'
									> <i>(pixels from right)</i>
								</td> 
							</tr>

						</table>

					<!-- 
					--------------------------------------------------------------------------
						HEADER ELEMENT: LOGO
				    -------------------------------------------------------------------------- 
					-->

						<h3><?php esc_html_e("Logo", "loc_canon_venuex"); ?> <img src="<?php echo get_template_directory_uri() . '/img/help.png' ?>"></h3>

						<div class='contextual-help'>
							<?php 

								fw_option_help(array(
									'type'					=> 'ul',
									'title' 				=> esc_html__('General logo hierarchy', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('by default the theme logo will be displayed', 'loc_canon_venuex'),
										esc_html__('if you enter a logo image URL this image will be displayed instead of the theme logo.', 'loc_canon_venuex'),
										esc_html__('if you enter text as logo this text will be displayed instead of any custom logo image and instead of the theme logo.', 'loc_canon_venuex'),
									),
								)); 

								fw_option_help(array(
									'type'					=> 'ul',
									'title' 				=> esc_html__('Logo URL', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Enter a complete URL to the image you want to use or', 'loc_canon_venuex'),
										esc_html__('Click the "Upload" button, upload an image and make sure you click the "Use as logo" button or', 'loc_canon_venuex'),
										esc_html__('Click the "Upload" button and choose an image from the media library tab. Make sure you click the "Use as logo" button.', 'loc_canon_venuex'),
										esc_html__('If you leave the URL text field empty the default logo will be displayed.', 'loc_canon_venuex'),
										esc_html__('Remember to save your changes.', 'loc_canon_venuex'),
									),
								)); 

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Use text as logo', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('This text will be displayed instead of any logo image. You can select font for logo text by going to Appearance > Google Webfonts > Logo text.', 'loc_canon_venuex'),
									),
								)); 

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Text as logo size', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('If using text as logo this option lets you set a font size.', 'loc_canon_venuex'),
									),
								)); 

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Logo max width', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('You can control the size of your logo by setting the maximum allowed width of your logo image.', 'loc_canon_venuex'),
										esc_html__('To make your logo HD-ready/retina-ready you should set the logo max width to half the original width of your image (compression ratio: 2)', 'loc_canon_venuex'),
									),
								)); 

							?>
						</div>

						<table class='form-table'>

							<tr valign='top'>
								<th scope='row'></th>
								<td>
									 <br>
								</td>
							</tr>

							<tr valign='top'>
								<th scope='row'><?php esc_html_e("Logo Preview", "loc_canon_venuex"); ?></th>
								<td>
									<?php 

				                        if (!empty($canon_options_frame['logo_url'])) {
				                            $logo_url = $canon_options_frame['logo_url'];
				                        } else {
				                            $logo_url = get_template_directory_uri() .'/img/logo@2x.png';
				                        }
				                        $logo_size = getimagesize($logo_url);
				                        if (!empty($canon_options_frame['logo_max_width'])) {
					                        $compression_ratio = $logo_size[0] / (int) $canon_options_frame['logo_max_width'];
				                        } else {
					                        $compression_ratio = 999;
				                        }

									 ?>
									<img class="thelogo" width="<?php if (!empty($canon_options_frame['logo_max_width'])) echo esc_attr($canon_options_frame['logo_max_width']); ?>" src="<?php echo esc_url($logo_url); ?>"><br><br>
									<?php printf("<i>(%s%s %s%s%s)</i>", esc_html__("Original size: Width: ", "loc_canon_venuex"), $logo_size[0], esc_html__("pixels, height: ", "loc_canon_venuex") , $logo_size[1], esc_html__(" pixels", "loc_canon_venuex")); ?><br>
                                    <?php printf("<i>(%s%s %s%.2f)</i>",esc_html__("Resized to max width: ", "loc_canon_venuex") , esc_attr($canon_options_frame['logo_max_width']), esc_html__("pixels. Compression ratio: ", "loc_canon_venuex"), $compression_ratio); ?><br>
									<br><br>
								</td>
							</tr>

							<?php 

								fw_option(array(
									'type'					=> 'upload',
									'title' 				=> esc_html__('Logo URL', 'loc_canon_venuex'),
									'slug' 					=> 'logo_url',
									'btn_text'				=> 'Upload logo',
									'options_name'			=> 'canon_options_frame',
								)); 

								fw_option(array(
									'type'					=> 'text',
									'title' 				=> esc_html__('Use text as logo', 'loc_canon_venuex'),
									'slug' 					=> 'logo_text',
									'options_name'			=> 'canon_options_frame',
								)); 

								fw_option(array(
									'type'					=> 'number',
									'title' 				=> esc_html__('Text as logo size', 'loc_canon_venuex'),
									'slug' 					=> 'logo_text_size',
									'min'					=> '1',										// optional
									'max'					=> '1000',									// optional
									'step'					=> '1',										// optional
									'width_px'				=> '60',									// optional
									'postfix'				=> '<i>(pixels)</i>',						// optional
									'options_name'			=> 'canon_options_frame',
								)); 

								fw_option(array(
									'type'					=> 'number',
									'title' 				=> esc_html__('Logo max width', 'loc_canon_venuex'),
									'slug' 					=> 'logo_max_width',
									'min'					=> '1',										// optional
									'max'					=> '1000',									// optional
									'step'					=> '1',										// optional
									'width_px'				=> '60',									// optional
									'postfix'				=> '<i>(pixels)</i>',						// optional
									'options_name'			=> 'canon_options_frame',
								)); 

							?>

						</table>

					<!-- 
					--------------------------------------------------------------------------
						HEADER ELEMENT: BACKGROUND IMAGE
				    -------------------------------------------------------------------------- 
					-->

						<h3><?php esc_html_e("Header Background Image", "loc_canon_venuex"); ?> <img src="<?php echo get_template_directory_uri() . '/img/help.png' ?>"></h3>

						<div class='contextual-help'>
							<?php 

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('A general note on the header background image', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Please notice that the header background image is for decorative purposes only. It should not be used to display images where precise positioning of the image content is required e.g. logos etc. The background image does not have the same responsive capabilities that inline images have and the image appearance (aspect ratio, cropping etc.) can vary widely across different viewport sizes.', 'loc_canon_venuex'),
									),
								)); 

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Show only on homepage', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('The header background image will only be displayed on the homepage.', 'loc_canon_venuex'),
									),
								)); 

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Background image URL', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Insert URL to use as header background image or click Select Image button to choose from media library.', 'loc_canon_venuex'),
									),
								)); 

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Background color', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Set header background color. Useful when using transparent image files.', 'loc_canon_venuex'),
									),
								)); 

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Header height', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Set header height', 'loc_canon_venuex'),
									),
								)); 

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Use Parallax Scrolling', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Enable parallax scrolling.', 'loc_canon_venuex'),
									),
								)); 

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Parallax ratio', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Number usually between 0 and 1 (but can be up to 2). 1 is no parallax scroll. 0 is max parallax scroll (image does not move when window is scrolling). Above 1 and image will scroll more than window.', 'loc_canon_venuex'),
									),
								)); 

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Image text', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Text to display over the header background image.', 'loc_canon_venuex'),
									),
								)); 

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Image text top margin', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Increase number to position text further down.', 'loc_canon_venuex'),
									),
								)); 


							?>
						</div>

						<table class='form-table'>


							<?php 

								fw_option(array(
									'type'					=> 'checkbox',
									'title' 				=> esc_html__('Show only on homepage', 'loc_canon_venuex'),
									'slug' 					=> 'header_img_homepage_only',
									'options_name'			=> 'canon_options_frame',
								)); 

								fw_option(array(
									'type'					=> 'upload',
									'title' 				=> esc_html__('Background image URL', 'loc_canon_venuex'),
									'slug' 					=> 'header_img_url',
									'btn_text'				=> esc_html__('Select Image', 'loc_canon_venuex'),
									'options_name'			=> 'canon_options_frame',
								)); 

								fw_option(array(
									'type'					=> 'color',
									'title' 				=> esc_html__('Background color', 'loc_canon_venuex'),
									'slug' 					=> 'header_img_bg_color',
									'options_name'			=> 'canon_options_frame',
								)); 
							
								fw_option(array(
									'type'					=> 'number',
									'title' 				=> esc_html__('Header height', 'loc_canon_venuex'),
									'slug' 					=> 'header_img_height',
									'min'					=> '0',
									'max'					=> '10000',
									'step'					=> '10',
									'postfix'				=> '<i> (pixels)</i>',
									'width_px'				=> '60',
									'options_name'			=> 'canon_options_frame',
								)); 

								fw_option(array(
									'type'					=> 'checkbox',
									'title' 				=> esc_html__('Use Parallax Scrolling', 'loc_canon_venuex'),
									'slug' 					=> 'header_img_use_parallax',
									'options_name'			=> 'canon_options_frame',
								)); 

								fw_option(array(
									'type'					=> 'number',
									'title' 				=> esc_html__('Parallax ratio', 'loc_canon_venuex'),
									'slug' 					=> 'header_img_parallax_ratio',
									'min'					=> '0',
									'max'					=> '2',
									'step'					=> '0.05',
									'postfix'				=> '<i> (0 for no-scroll)</i>',
									'width_px'				=> '60',
									'options_name'			=> 'canon_options_frame',
								)); 

								fw_option(array(
									'type'					=> 'textarea',
									'title' 				=> esc_html__('Image text', 'loc_canon_venuex'),
									'slug' 					=> 'header_img_text',
									'rows'					=> '5',
									'options_name'			=> 'canon_options_frame',
								)); 

								fw_option(array(
									'type'					=> 'select',
									'title' 				=> esc_html__('Image text alignment', 'loc_canon_venuex'),
									'slug' 					=> 'header_img_text_alignment',
									'select_options'		=> array(
										'centered'			=> esc_html__('Center', 'loc_canon_venuex'),
										'left'				=> esc_html__('Left', 'loc_canon_venuex'),
										'right'				=> esc_html__('Right', 'loc_canon_venuex'),
									),
									'options_name'			=> 'canon_options_frame',
								)); 

								fw_option(array(
									'type'					=> 'number',
									'title' 				=> esc_html__('Image text top margin', 'loc_canon_venuex'),
									'slug' 					=> 'header_img_text_margin_top',
									'min'					=> '0',
									'max'					=> '10000',
									'step'					=> '10',
									'postfix'				=> '<i> (pixels)</i>',
									'width_px'				=> '60',
									'options_name'			=> 'canon_options_frame',
								)); 


							?>

						</table>

					<!-- 
					--------------------------------------------------------------------------
						HEADER ELEMENT: BANNER
				    -------------------------------------------------------------------------- 
					-->

						<h3><?php esc_html_e("Ad Banner", "loc_canon_venuex"); ?> <img src="<?php echo get_template_directory_uri() . '/img/help.png' ?>"></h3>

						<div class='contextual-help'>
							<?php 

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Ad Code', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Insert your ad code or ad HTML here. If you are unsure what code to use you should consult your ad provider.', 'loc_canon_venuex'),
									),
								)); 


							?>
						</div>

						<table class='form-table'>


							<?php 

								fw_option(array(
									'type'					=> 'textarea',
									'title' 				=> esc_html__('Ad code', 'loc_canon_venuex'),
									'slug' 					=> 'header_banner_code',
									'rows'					=> '5',
									'options_name'			=> 'canon_options_frame',
								)); 

							?>

						</table>

					<!-- 
					--------------------------------------------------------------------------
						HEADER ELEMENT: HEADER TEXT
				    -------------------------------------------------------------------------- 
					-->

						<h3><?php esc_html_e("Header Text", "loc_canon_venuex"); ?> <img src="<?php echo get_template_directory_uri() . '/img/help.png' ?>"></h3>

						<div class='contextual-help'>
							<?php 

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Header text', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Text to display in header. Can contain HTML.', 'loc_canon_venuex'),
									),
								)); 


							?>
						</div>

						<table class='form-table'>


							<?php 

								fw_option(array(
									'type'					=> 'text',
									'title' 				=> esc_html__('Header text', 'loc_canon_venuex'),
									'class'					=> 'widefat',
									'slug' 					=> 'header_text',
									'options_name'			=> 'canon_options_frame',
								)); 

							?>

						</table>

					<!-- 
					--------------------------------------------------------------------------
						SOCIAL LINKS 
				    -------------------------------------------------------------------------- 
					-->

						<h3><?php esc_html_e("Social Links", "loc_canon_venuex"); ?> <img src="<?php echo get_template_directory_uri() . '/img/help.png' ?>"></h3>

						<div class='contextual-help'>
							<?php 

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Show social icons in footer', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('If checked your social icons will be displayed at the bottom right area of the footer. Notice that social links can also be placed in your header using the Header Builder.', 'loc_canon_venuex'),
									),
								)); 

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Open links in new window', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Clicking a social link will open it up in a new window.', 'loc_canon_venuex'),
									),
								)); 

								fw_option_help(array(
									'type'					=> 'ul',
									'title' 				=> esc_html__('Social links', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Choose an icon in the select and attach this to a social link.', 'loc_canon_venuex'),
										esc_html__('Make sure you put the whole URL to your social site in the text input.', 'loc_canon_venuex'),
										esc_html__('You can add a new social link to the end of the list by clicking "Add social link', 'loc_canon_venuex'),
										esc_html__('You can remove the last social link in your list by clicking "Remove social link".', 'loc_canon_venuex'),
										wp_kses_post(__('You can see a full list of the Font Awesome icons <a href="http://fortawesome.github.io/Font-Awesome/cheatsheet/" target="_blank">here</a>.', 'loc_canon_venuex')),
									),
								)); 

							?>
						</div>

						<table class='form-table social_links'>

							<?php 

								fw_option(array(
									'type'					=> 'checkbox',
									'title' 				=> esc_html__('Show social icons in footer', 'loc_canon_venuex'),
									'slug' 					=> 'show_social_icons',
									'options_name'			=> 'canon_options_frame',
								)); 

								fw_option(array(
									'type'					=> 'checkbox',
									'title' 				=> esc_html__('Open links in new window', 'loc_canon_venuex'),
									'slug' 					=> 'social_in_new',
									'options_name'			=> 'canon_options_frame',
								)); 

							?>

							<?php 
							if (isset($canon_options_frame['social_links'])) {

								$font_awesome_array = mb_get_font_awesome_icon_names_in_array();

								
								?>

								<tr valign='top' class='social_links_row'>
									<th scope='row'><?php esc_html_e("Social links", "loc_canon_venuex"); ?></th>
									<td>
										<ul class="ul_sortable"  data-split_index="2" data-placeholder="social_links_sortable_placeholder">
											<?php for ($i = 0; $i < count($canon_options_frame['social_links']); $i++) : ?>

												<li>
													<select class="social_links_icon fa_select li_option" name="canon_options_frame[social_links][<?php echo esc_attr($i); ?>][0]"> 
														<?php 

															for ($n = 0; $n < count($font_awesome_array); $n++) {  
															?>
										     					<option value="<?php echo esc_attr($font_awesome_array[$n]); ?>" <?php if (isset($canon_options_frame['social_links'][$i][0])) {if ($canon_options_frame['social_links'][$i][0] == $font_awesome_array[$n]) echo "selected='selected'";} ?>><?php echo esc_attr($font_awesome_array[$n]); ?></option> 
															<?php
															}

														?>
													</select> 

													<i class="fa <?php if (isset($canon_options_frame['social_links'][$i][0])) { echo esc_attr($canon_options_frame['social_links'][$i][0]); } else { echo "fa-flag"; } ?>"></i>
													<input type='text' class='social_links_link li_option' name='canon_options_frame[social_links][<?php echo esc_attr($i); ?>][1]' value='<?php if (isset($canon_options_frame['social_links'][$i][1])) echo esc_url($canon_options_frame['social_links'][$i][1]); ?>'>
													<button class="button ul_del_this"><?php esc_html_e("delete", "loc_canon_venuex"); ?></button>

												</li>

											<?php endfor; ?>

										</ul>

										<div class="ul_control" data-min="1" data-max="1000">
											<input type="button" class="button ul_add" value="<?php esc_html_e("Add", "loc_canon_venuex"); ?>" />
											<input type="button" class="button ul_del" value="<?php esc_html_e("Delete", "loc_canon_venuex"); ?>" />
										</div>

									</td>
								</tr>

								<?php

							}

							?>

						</table>


					<!-- 
					--------------------------------------------------------------------------
						HEADER ELEMENT: TOOLBAR
				    -------------------------------------------------------------------------- 
					-->

						<h3><?php esc_html_e("Toolbar", "loc_canon_venuex"); ?> <img src="<?php echo get_template_directory_uri() . '/img/help.png' ?>"></h3>

						<div class='contextual-help'>
							<?php 

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Toolbar', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Select what tools to add to the toolbar.', 'loc_canon_venuex'),
									),
								)); 


							?>
						</div>

						<table class='form-table'>


							<?php 

								fw_option(array(
									'type'					=> 'checkbox',
									'title' 				=> esc_html__('Search button', 'loc_canon_venuex'),
									'slug' 					=> 'toolbar_search_button',
									'options_name'			=> 'canon_options_frame',
								)); 

							?>

						</table>


					<!-- 
					--------------------------------------------------------------------------
						HEADER ELEMENT: COUNTDOWN
				    -------------------------------------------------------------------------- 
					-->

						<h3><?php esc_html_e("Countdown", "loc_canon_venuex"); ?> <img src="<?php echo get_template_directory_uri() . '/img/help.png' ?>"></h3>

						<div class='contextual-help'>
							<?php 

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Countdown to', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Must be in the format Month DD, YYYY HH:MM:SS e.g. December 31, 2023 23:59:59', 'loc_canon_venuex'),
									),
								)); 

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('GMT offset', 'loc_canon_venuex'),
									'content' 				=> array(
										wp_kses_post(__('GMT offset of your current timezone. You can search for your timezone <a href="http://www.worldtimezone.com/" target="_blank">here</a>.', 'loc_canon_venuex')),
									),
								)); 

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Description', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Countdown description.', 'loc_canon_venuex'),
									),
								)); 

							?>
						</div>

						<table class='form-table'>


							<?php 

								fw_option(array(
									'type'					=> 'text',
									'title' 				=> esc_html__('Countdown to', 'loc_canon_venuex'),
									'class'					=> 'widefat',
									'slug' 					=> 'countdown_datetime_string',
									'options_name'			=> 'canon_options_frame',
								)); 

								fw_option(array(
									'type'					=> 'text',
									'title' 				=> esc_html__('GMT Offset', 'loc_canon_venuex'),
									'class'					=> 'widefat',
									'slug' 					=> 'countdown_gmt_offset',
									'options_name'			=> 'canon_options_frame',
								)); 

								fw_option(array(
									'type'					=> 'text',
									'title' 				=> esc_html__('Description', 'loc_canon_venuex'),
									'class'					=> 'widefat',
									'slug' 					=> 'countdown_description',
									'options_name'			=> 'canon_options_frame',
								)); 

							?>

						</table>


					<!-- 
					--------------------------------------------------------------------------
						FOOTER
				    -------------------------------------------------------------------------- 
					-->

						<h3><?php esc_html_e("Footer", "loc_canon_venuex"); ?> <img src="<?php echo get_template_directory_uri() . '/img/help.png' ?>"></h3>

						<div class='contextual-help'>
							<?php 

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Show breadcrumbs footer', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Footer containing breadcrumbs.', 'loc_canon_venuex'),
									),
								)); 

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Show widgetized footer', 'loc_canon_venuex'),
									'content' 				=> array(
										wp_kses_post(__('Footer containing four widgetized areas. Go to <i>Appearance > Widgets</i> to add widgets.', 'loc_canon_venuex')),
									),
								)); 

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Show social footer', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Footer containing footer text and social icons.', 'loc_canon_venuex'),
									),
								)); 

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Footer text', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Text to be displayed in the left side of the social footer area.', 'loc_canon_venuex'),
									),
								)); 

							?>
						</div>

						<table class='form-table'>

							<?php 

								fw_option(array(
									'type'					=> 'checkbox',
									'title' 				=> esc_html__('Show breadcrumbs footer', 'loc_canon_venuex'),
									'slug' 					=> 'show_pre_footer',
									'options_name'			=> 'canon_options_frame',
								)); 

								fw_option(array(
									'type'					=> 'checkbox',
									'title' 				=> esc_html__('Show widgetized footer', 'loc_canon_venuex'),
									'slug' 					=> 'show_main_footer',
									'options_name'			=> 'canon_options_frame',
								)); 

								fw_option(array(
									'type'					=> 'checkbox',
									'title' 				=> esc_html__('Show social footer', 'loc_canon_venuex'),
									'slug' 					=> 'show_post_footer',
									'options_name'			=> 'canon_options_frame',
								)); 

								fw_option(array(
									'type'					=> 'textarea',
									'title' 				=> esc_html__('Footer text', 'loc_canon_venuex'),
									'slug' 					=> 'footer_text',
									'rows'					=> '5',
									'options_name'			=> 'canon_options_frame',
								)); 

							?>


						</table>


					<!-- 
					--------------------------------------------------------------------------
						WIDGETIZED FOOTER
				    -------------------------------------------------------------------------- 
					-->

						<h3><?php esc_html_e("Widgetized Footer", "loc_canon_venuex"); ?> <img src="<?php echo get_template_directory_uri() . '/img/help.png' ?>"></h3>

						<div class='contextual-help'>
							<?php 

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Layout', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Select a layout for the widgetized footer.', 'loc_canon_venuex'),
									),
								)); 

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Footer Widget Areas', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Select what widget areas to display.', 'loc_canon_venuex'),
									),
								)); 

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Example', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('The layout "third + two-thirds" is a three column layout meaning that the footer will be divided into three columns. The first widget area will occupy the first column and the second widget area will span the last two columns.', 'loc_canon_venuex'),
									),
								)); 


							?>
						</div>

						<table class='form-table'>

							<?php 

								fw_option(array(
									'type'					=> 'select_reverse',
									'title' 				=> esc_html__('Layout', 'loc_canon_venuex'),
									'slug' 					=> 'widgetized_footer_layout',
									'select_options'		=> array(
										esc_html__('TWO COLUMN LAYOUT', 'loc_canon_venuex')							=> 'half_half',
										esc_html__('half + half', 'loc_canon_venuex')									=> 'half_half',
										''																=> 'third_third_third',
										esc_html__('THREE COLUMN LAYOUTS', 'loc_canon_venuex')							=> 'third_third_third',
										esc_html__('third + third + third', 'loc_canon_venuex')						=> 'third_third_third',
										esc_html__('two-thirds + third', 'loc_canon_venuex')							=> 'two-thirds_third',
										esc_html__('third + two-thirds', 'loc_canon_venuex')							=> 'third_two-thirds',
										' '																=> 'fourth_fourth_fourth_fourth',
										esc_html__('FOUR COLUMN LAYOUTS', 'loc_canon_venuex')							=> 'fourth_fourth_fourth_fourth',
										esc_html__('fourth + fourth + fourth + fourth', 'loc_canon_venuex')			=> 'fourth_fourth_fourth_fourth',
										esc_html__('fourth + half + fourth', 'loc_canon_venuex')						=> 'fourth_half_fourth',
										esc_html__('fourth + fourth + half', 'loc_canon_venuex')						=> 'fourth_fourth_half',
										'   '															=> 'fifth_fifth_fifth_fifth_fifth',
										esc_html__('FIVE COLUMN LAYOUTS', 'loc_canon_venuex')							=> 'fifth_fifth_fifth_fifth_fifth',
										esc_html__('fifth + fifth + fifth + fifth + fifth', 'loc_canon_venuex')		=> 'fifth_fifth_fifth_fifth_fifth',
									),
									'options_name'			=> 'canon_options_frame',
								)); 

								for ($i = 1; $i < 6; $i++) {  

									fw_option(array(
										'type'					=> 'select_sidebar',
										'title' 				=> esc_html__('Footer Widget Area ', 'loc_canon_venuex') . $i,
										'slug' 					=> 'widget_footer_widget_area_' .$i,
										'options_name'			=> 'canon_options_frame',
									)); 

								}

							?>


						</table>




					<!-- BOTTOM OF PAGE -->						
					<?php submit_button(); ?>

				</form>
			</div> <!-- end table container -->	

	
		</div>

	</div>

