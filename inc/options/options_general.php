<div class="wrap">

		<div id="icon-themes" class="icon32"></div>

		<h2><?php printf( "%s %s - %s", esc_attr(wp_get_theme()->Name), esc_html__("Settings", "loc_canon_venuex"), esc_html__("General", "loc_canon_venuex") ); ?></h2>

		<?php 
			//delete_option('canon_options');
			$canon_options = get_option('canon_options'); 
			$canon_options_frame = get_option('canon_options_frame'); 
			$canon_options_post = get_option('canon_options_post'); 
			$canon_options_appearance = get_option('canon_options_appearance');
			$canon_options_advanced = get_option('canon_options_advanced'); 

			//detect dev
			$dev = (isset($_GET['dev'])) ? $_GET['dev'] : "false";

			$dev_options = array(

				// GENERAL OPTION
				array( 'slug' => 'use_boxed_design', 'set' => 'canon_options'),

				// FRAME OPTIONS
				array( 'slug' => 'use_boxed_header', 'set' => 'canon_options_frame'),

				array( 'slug' => 'use_sticky_preheader', 'set' => 'canon_options_frame'),
				array( 'slug' => 'use_sticky_header', 'set' => 'canon_options_frame'),
				array( 'slug' => 'use_sticky_postheader', 'set' => 'canon_options_frame'),

				array( 'slug' => 'pre_header_layout', 'set' => 'canon_options_frame'),
				array( 'slug' => 'main_header_layout', 'set' => 'canon_options_frame'),
				array( 'slug' => 'post_header_layout', 'set' => 'canon_options_frame'),

				array( 'slug' => 'pre_custom_center', 'set' => 'canon_options_frame'),
				array( 'slug' => 'pre_custom_left', 'set' => 'canon_options_frame'),
				array( 'slug' => 'pre_custom_right', 'set' => 'canon_options_frame'),

				array( 'slug' => 'main_custom_center', 'set' => 'canon_options_frame'),
				array( 'slug' => 'main_custom_left', 'set' => 'canon_options_frame'),
				array( 'slug' => 'main_custom_right', 'set' => 'canon_options_frame'),

				array( 'slug' => 'post_custom_center', 'set' => 'canon_options_frame'),
				array( 'slug' => 'post_custom_left', 'set' => 'canon_options_frame'),
				array( 'slug' => 'post_custom_right', 'set' => 'canon_options_frame'),

				array( 'slug' => 'preheader_opacity', 'set' => 'canon_options_frame'),
				array( 'slug' => 'header_opacity', 'set' => 'canon_options_frame'),
				array( 'slug' => 'postheader_opacity', 'set' => 'canon_options_frame'),

				// POST OPTIONS

				// APPEARANCE
				array( 'slug' => 'anim_menus', 'set' => 'canon_options_appearance'),

				// ADVANCED

			);

			// GENERATE DEV MODE URL PARAM
			$url_param = "";
			foreach ($dev_options as $key => $value) {
				extract($value);
				$this_checkbox_slug = "dev_option-$set-$slug";
				if (isset($canon_options[$this_checkbox_slug])) { if ($canon_options[$this_checkbox_slug] == "checked") {
					$option = get_option($set);
					if (isset($option[$slug])) {
						$this_option_value = $option[$slug];
						$url_param .= "&$slug=$this_option_value";
					}
				} }
			}
			$url_param = trim($url_param, "&");
			$url_param = "?" . $url_param;

			// var_dump($canon_options);


		?>

		<br>
		
		<div class="options_wrapper canon-options">
		
			<div class="table_container">

				<form method="post" action="options.php" enctype="multipart/form-data">
					<?php settings_fields('group_canon_options'); ?>				<!-- very important to add these two functions as they mediate what wordpress generates automatically from the functions.php -->
					<?php do_settings_sections('handle_canon_options'); ?>		


					<?php submit_button(); ?>

					<!-- 

						INDEX

						DEVELOPER TOOLS
						GENERAL 
						MAIN SEARCH AUTOCOMPLETE
						COMPATIBILITY

					-->
					
					<?php if ($dev === "true") : ?>

					<!-- 
					--------------------------------------------------------------------------
						DEVELOPER TOOLS
				    -------------------------------------------------------------------------- 
					-->

							<h3><?php esc_html_e('Developer Tools', 'loc_canon_venuex'); ?> <img src="<?php echo get_template_directory_uri() . '/img/help.png' ?>"></h3>

							<div class='contextual-help'>
								<?php 

									fw_option_help(array(
										'type'					=> 'standard',
										'title' 				=> esc_html__('Developer mode', 'loc_canon_venuex'),
										'content' 				=> array(
											esc_html__('Turns developer mode on. Other developer options will only take effect when developer mode is turned on.', 'loc_canon_venuex'),
										),
									)); 

									fw_option_help(array(
										'type'					=> 'standard',
										'title' 				=> esc_html__('Dev options URL param generator', 'loc_canon_venuex'),
										'content' 				=> array(
											esc_html__('Check the checkboxes of the options you would like to generate URL parameters for. Placing these parameters at the end of a URL will overwrite the theme options.', 'loc_canon_venuex'),
											esc_html__('When you have selected you checkboxes make sure you save this selection. The URL parameters string will show the last saved selection.', 'loc_canon_venuex'),
											esc_html__('Workflow is easiest with a two server setup (e.g. demo server and local server). You could then use your local server to experiment/setup for example your header and then generate and copy/paste the url param string and use on your demo server to overwrite the server header options.', 'loc_canon_venuex'),
										),
									)); 

								?>

							</div>

							<table class='form-table'>

								<?php
								
									fw_option(array(
										'type'					=> 'checkbox',
										'title' 				=> esc_html__('Developer mode', 'loc_canon_venuex'),
										'slug' 					=> 'dev_mode',
										'options_name'			=> 'canon_options',
									));

								?>

								 <!-- DIVIDER -->
								 <tr><td colspan="2"><hr></td></tr>

								 <tr><td colspan="2"><strong>Dev options URL param generator:</strong></td></tr>

								<?php

									foreach ($dev_options as $key => $value) {
									 	extract($value);

										fw_option(array(
											'type'					=> 'checkbox',
											'title' 				=> $slug,
											'slug' 					=> "dev_option-$set-$slug",
											'options_name'			=> 'canon_options',
											'postfix'				=> "<i>($set)</i>",
										));

									 } 

								?>

								<tr><td colspan="2"><input type="text" class="widefat" onclick="this.select()" value="<?php echo esc_attr($url_param); ?>" readonly /></td></tr>

							</table>

						 		
					<?php else: ?>

						<input type="hidden" name="canon_options[dev_mode]" value="<?php echo esc_attr($canon_options['dev_mode']); ?>" />

					<?php endif; ?>
					


					<!-- 
					--------------------------------------------------------------------------
						GENERAL 
				    -------------------------------------------------------------------------- 
					-->

						<h3><?php esc_html_e("General", "loc_canon_venuex"); ?> <img src="<?php echo get_template_directory_uri() . '/img/help.png' ?>"></h3>

						<div class='contextual-help'>
							<?php
								
								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Use responsive design', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Responsive design changes the way your site looks depending on the browser size. This is done to optimize the viewing experience on different devices.', 'loc_canon_venuex'),
										esc_html__('Turning off responsive design will make the site look the same across all devices and browser sizes.', 'loc_canon_venuex'),
									),
								)); 

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Use boxed design', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Use boxed design for site layout. Otherwise site will display in full width layout.', 'loc_canon_venuex'),
									),
								)); 

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Construction mode', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Activating construction mode will mean that only logged in users can see the content of your site. Only exception are pages using the placeholder template which can still be seen by all.', 'loc_canon_venuex'),
										esc_html__('We suggest that if you use this function you also use a placeholder page as a "static homepage" to let people know that your site is under construction.', 'loc_canon_venuex'),
									),
								)); 

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Construction mode message', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('The message that will be displayed to visitors when in construction mode.', 'loc_canon_venuex'),
										esc_html__('Remember that you can set up a placeholder page (using the placeholder page-template) and use as a homepage as this page type will always display even when construction mode is active.', 'loc_canon_venuex'),
									),
								)); 

								fw_option_help(array(
									'type'					=> 'ul',
									'title' 				=> esc_html__('Favicon URL', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Enter a complete URL to the image you want to use or', 'loc_canon_venuex'),
										esc_html__('Click the "Upload" button, upload an image and make sure you click the "Use as favicon" button or', 'loc_canon_venuex'),
										esc_html__('Click the "Upload" button and choose an image from the media library tab. Make sure you click the "Use as favicon" button.', 'loc_canon_venuex'),
										esc_html__('If you leave the URL text field empty the default favicon will be displayed.', 'loc_canon_venuex'),
										esc_html__('Remember to save your changes.', 'loc_canon_venuex'),
									),
								)); 

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Read-more-links text', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Text to use on general read-more-links', 'loc_canon_venuex'),
									),
								)); 

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Sidebars alignment', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Choose which side to have sidebars on.', 'loc_canon_venuex'),
									),
								)); 

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Overlay header', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Overlay header on top element. Only available on top elements that accepts header overlay (slider, full width featured images etc).', 'loc_canon_venuex'),
									),
								)); 


								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Responsive overlay', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('You can turn overlays off at certain responsive break points. Use this if overlays interfere with content on smaller viewport sizes.', 'loc_canon_venuex'),
									),
								)); 

							?>			

						</div>

						<table class='form-table'>

							<?php 

								fw_option(array(
									'type'					=> 'checkbox',
									'title' 				=> esc_html__('Use responsive design', 'loc_canon_venuex'),
									'slug' 					=> 'use_responsive_design',
									'options_name'			=> 'canon_options',
								)); 

								fw_option(array(
									'type'					=> 'checkbox',
									'title' 				=> esc_html__('Use boxed design', 'loc_canon_venuex'),
									'slug' 					=> 'use_boxed_design',
									'options_name'			=> 'canon_options',
								)); 

								fw_option(array(
									'type'					=> 'checkbox',
									'title' 				=> esc_html__('Construction mode', 'loc_canon_venuex'),
									'postfix'				=> wp_kses_post(__('<i>(Warning: only logged-in users will be able to see your site pages.)</i>', 'loc_canon_venuex')),
									'slug' 					=> 'use_construction_mode',
									'options_name'			=> 'canon_options',
								)); 

								fw_option(array(
									'type'					=> 'text',
									'title' 				=> esc_html__('Construction mode message', 'loc_canon_venuex'),
									'slug' 					=> 'construction_msg',
									'class'					=> 'widefat',
									'options_name'			=> 'canon_options',
								)); 

								fw_option(array(
									'type'					=> 'upload',
									'title' 				=> esc_html__('Favicon URL', 'loc_canon_venuex'),
									'slug' 					=> 'favicon_url',
									'btn_text'				=> 'Upload favicon',
									'options_name'			=> 'canon_options',
								)); 

								fw_option(array(
									'type'					=> 'text',
									'title' 				=> esc_html__('Read-more-links text', 'loc_canon_venuex'),
									'slug' 					=> 'read_more_text',
									'class'					=> 'widefat',
									'options_name'			=> 'canon_options',
								)); 

								fw_option(array(
									'type'					=> 'select',
									'title' 				=> esc_html__('Sidebars alignment', 'loc_canon_venuex'),
									'slug' 					=> 'sidebars_alignment',
									'select_options'		=> array(
										'left'					=> esc_html__('Left', 'loc_canon_venuex'),
										'right'					=> esc_html__('Right', 'loc_canon_venuex')
									),
									'options_name'			=> 'canon_options',
								)); 

								fw_option(array(
									'type'					=> 'checkbox',
									'title' 				=> esc_html__('Overlay header', 'loc_canon_venuex'),
									'slug' 					=> 'overlay_header',
									'options_name'			=> 'canon_options',
								)); 


								fw_option(array(
									'type'					=> 'select',
									'title' 				=> esc_html__('Responsive overlay header', 'loc_canon_venuex'),
									'slug' 					=> 'overlay_header_turn_off_width',
									'select_options'		=> array(
										'0'					=> esc_html__('Overlay header stays the same', 'loc_canon_venuex'),
										'768'				=> esc_html__('Turn off @ viewport width below 768px', 'loc_canon_venuex'),
										'600'				=> esc_html__('Turn off @ viewport width below 600px', 'loc_canon_venuex'),
										'480'				=> esc_html__('Turn off @ viewport width below 480px', 'loc_canon_venuex'),
									),
									'options_name'			=> 'canon_options',
								)); 


							 ?>		

						</table>

					<!-- 
					--------------------------------------------------------------------------
						MAIN SEARCH AUTOCOMPLETE
				    -------------------------------------------------------------------------- 
					-->

						<h3><?php esc_html_e("Main Search Autocomplete", "loc_canon_venuex"); ?> <img src="<?php echo get_template_directory_uri() . '/img/help.png' ?>"></h3>

						<div class='contextual-help'>
							<?php 

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Search words', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('When typing a search term in the main search box the autocomplete function will make search suggestions.', 'loc_canon_venuex'),
										esc_html__('Enter words or phrases to give as search suggestions. Separate terms with a comma.', 'loc_canon_venuex'),
									),
								)); 

							?>

						</div>

						<table class='form-table'>

							<?php 

								fw_option(array(
									'type'					=> 'textarea',
									'title' 				=> esc_html__('Suggest these words', 'loc_canon_venuex'),
									'slug' 					=> 'autocomplete_words',
									'rows'					=> '5',
									'options_name'			=> 'canon_options',
								)); 

							?>

						</table>


					<!-- 
					--------------------------------------------------------------------------
						COMPATIBILITY
				    -------------------------------------------------------------------------- 
					-->

						<h3><?php esc_html_e("Compatibility", "loc_canon_venuex"); ?> <img src="<?php echo get_template_directory_uri() . '/img/help.png' ?>"></h3>

						<div class='contextual-help'>
							<?php 

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Suppress theme meta description', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('If using a 3rd party SEO plugin the theme meta description can sometimes interfere with the plugin meta description.', 'loc_canon_venuex'),
										esc_html__('Use this option to suppress the theme meta description and use the plugin meta description instead.', 'loc_canon_venuex'),
									),
								)); 

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Suppress theme Open Graph data', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('Open Graph is a protocol used by Facebook to gather information about your site that can be utilized when sharing content on Facebook.', 'loc_canon_venuex'),
										esc_html__('If using a 3rd party SEO plugin the theme Open Graph data can sometimes interfere with the plugin Open Graph data.', 'loc_canon_venuex'),
										esc_html__('Use this option to suppress the theme Open Graph data and use the plugin Open Graph data instead.', 'loc_canon_venuex'),
									),
								)); 

								fw_option_help(array(
									'type'					=> 'standard',
									'title' 				=> esc_html__('Chrome/Safari @font-face fix', 'loc_canon_venuex'),
									'content' 				=> array(
										esc_html__('On some server configurations a known bug in Chrome and Safari can prevent the rendering of serverside @font-face fonts leaving a page blank except for images and other non-text media. If your site experiences this bug make sure you turn on the Chrome/Safari @font-face fix.', 'loc_canon_venuex'),
									),
								)); 

							?>

						</div>

						<table class='form-table'>

							<?php 

								fw_option(array(
									'type'					=> 'checkbox',
									'title' 				=> esc_html__('Suppress theme meta description', 'loc_canon_venuex'),
									'slug' 					=> 'hide_theme_meta_description',
									'options_name'			=> 'canon_options',
								)); 

								fw_option(array(
									'type'					=> 'checkbox',
									'title' 				=> esc_html__('Suppress theme Open Graph data', 'loc_canon_venuex'),
									'slug' 					=> 'hide_theme_og',
									'options_name'			=> 'canon_options',
								)); 

								fw_option(array(
									'type'					=> 'checkbox',
									'title' 				=> esc_html__('Chrome/Safari @font-face fix', 'loc_canon_venuex'),
									'slug' 					=> 'fontface_fix',
									'options_name'			=> 'canon_options',
								)); 

							?>

						</table>



					<?php submit_button(); ?>


				</form>
			</div> <!-- end table container -->	

	
		</div>

	</div>

