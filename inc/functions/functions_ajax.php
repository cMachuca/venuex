<?php

/**************************************

INDEX

VOTE POLL

***************************************/


/**************************************
VOTE POLL
***************************************/

	add_action('wp_ajax_vote_poll', 'vote_poll');
	add_action('wp_ajax_nopriv_vote_poll', 'vote_poll');

	function vote_poll() {

		// GET VARS
		$id = $_REQUEST['id'];
		$selected_radio_button_index = $_REQUEST['selected_radio_button_index'];

		// BOUNCER
		if (!wp_verify_nonce($_REQUEST['nonce'], 'vote_poll_' . $id )) {
			exit('NONCE INCORRECT!');
		}
		if (!isset($_COOKIE['venuex_cookie'])) die();

		// KICK IF USER HAS ALREADY VOTED
		if (mb_cookie_check_key_for_value('venuex_cookie', 'voted-polls', $id)) { die();};

		//UPDATE POLL VOTES
		$cmb_poll_answers = get_post_meta($id, 'cmb_poll_answers', true);
		$cmb_poll_answers[$selected_radio_button_index]['votes'] = $cmb_poll_answers[$selected_radio_button_index]['votes']+1;
		update_post_meta($id,'cmb_poll_answers',$cmb_poll_answers);

		//UPDATE COOKIE
		$voted_polls_string = mb_cookie_get_key_value ("venuex_cookie", "voted-polls");
		$voted_polls_string = mb_add_value_to_delim_string ($voted_polls_string, $id, ",", false);
		mb_update_cookie_key_value('venuex_cookie', 'voted-polls', $voted_polls_string);

		//OUTPUT
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			echo "success";
		}

		die();

	}


