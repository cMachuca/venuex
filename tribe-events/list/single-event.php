<?php
/**
 * List View Single Event
 * This file contains one event in the list view
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/list/single-event.php
 *
 * @package TribeEventsCalendar
 *
 */
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

// Setup an array of venue details for use later in the template
$venue_details = tribe_get_venue_details();

// Venue
$has_venue_address = ( ! empty( $venue_details['address'] ) ) ? ' location' : '';

// Organizer
$organizer = tribe_get_organizer();

$event_thumbnail = tribe_event_featured_image(null, 'large');

?>


<!-- Event Title -->
<?php do_action( 'tribe_events_before_the_event_title' ) ?>
<h2 class="tribe-events-list-event-title">
	<a class="tribe-event-url" href="<?php echo esc_url( tribe_get_event_link() ); ?>" title="<?php the_title_attribute() ?>" rel="bookmark">
		<?php the_title() ?>
	</a>
</h2>
<?php do_action( 'tribe_events_after_the_event_title' ) ?>


<div class="clearfix">

	<!-- Event Image -->
	<?php if (!empty($event_thumbnail)) { printf('<div class="fourth first-fourth">%s</div>', wp_kses_post($event_thumbnail)); } ?>
	

	<div class="fourth">
		
		<!-- DETAILS -->
		<h4 class="tribe-events-list-meta-title tri-details"><?php esc_html_e('Details', 'loc_canon_venuex'); ?></h4>
		
		<!-- Schedule & Recurrence Details -->
		<div class="tribe-event-schedule-details">
			<?php echo tribe_events_event_schedule_details() ?>
		</div>
		
		<!-- Event Cost -->
		<?php if ( tribe_get_cost() ) : ?>
			<div class="tribe-events-event-cost">
				<span><?php echo tribe_get_cost( null, true ); ?></span>
			</div>
		<?php endif; ?>
		
		<!-- Event Meta -->
		<?php do_action( 'tribe_events_before_the_meta' ) ?>
		<div class="tribe-events-event-meta clearfix">
			<div class="author <?php echo esc_attr( $has_venue_address ); ?>">
		
				<!-- VENUE -->
				<h4 class="tribe-events-list-meta-title tri-venue"><?php esc_html_e('Venue', 'loc_canon_venuex'); ?></h4>
				
				<?php if ( $venue_details ) : ?>
					<!-- Venue Display Info -->
					<div class="tribe-events-venue-details">
						<?php echo implode( ', ', $venue_details ); ?>
					</div> <!-- .tribe-events-venue-details -->
				<?php endif; ?>
		
			</div>
		</div><!-- .tribe-events-event-meta -->
		<?php do_action( 'tribe_events_after_the_meta' ) ?>
		
	</div>
	
	
	<div class="<?php if (empty($event_thumbnail)) { echo "three-fourths"; } else { echo "half"; } ?> last">
	
		<!-- DESCRIPTION -->
		<h4 class="tribe-events-list-meta-title tri-desciption"><?php esc_html_e('Description', 'loc_canon_venuex'); ?></h4>
		
		<!-- Event Content -->
		<?php do_action( 'tribe_events_before_the_content' ) ?>
		<div class="tribe-events-list-event-description tribe-events-content">
			<?php echo tribe_events_get_the_excerpt(); ?>
		</div>
		<?php do_action( 'tribe_events_after_the_content' ) ?>
		
		<a href="<?php echo esc_url( tribe_get_event_link() ); ?>" class="tribe-events-read-more centered" rel="bookmark"><?php esc_html_e( 'More Details', 'loc_canon_venuex' ) ?></a>
	</div>

</div>