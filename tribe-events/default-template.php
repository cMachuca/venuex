<?php
/**
 * Default Events Template
 * This file is the basic wrapper template for all the views if 'Default Events Template' 
 * is selected in Events -> Settings -> Template -> Events Template.
 * 
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/default-template.php
 *
 * @package TribeEventsCalendar
 * @since  3.0
 * @author Modern Tribe Inc.
 *
 */

if ( !defined('ABSPATH') ) { die('-1'); }

?>

<?php 

	$canon_options = get_option('canon_options');
	$canon_options_post = get_option('canon_options_post'); 

	// DEFAULTS
	if (!isset($canon_options_post['use_events_sidebar'])) { $canon_options_post['use_events_sidebar'] = "unchecked"; }

	// SET LAYOUT
	$has_sidebar = ($canon_options_post['use_events_sidebar'] == "checked") ? true : false;
	if (is_single()) {
		$cmb_event_layout = get_post_meta($post->ID, 'cmb_event_layout', true);
		if ($cmb_event_layout != "default" && !empty($cmb_event_layout)) {
			if ($cmb_event_layout == "full") { $has_sidebar = false; }
			if ($cmb_event_layout == "sidebar") { $has_sidebar = true; }
		}
	}

	// SET MAIN CONTENT CLASS
	$main_content_class = "main-content";
	if ($has_sidebar) { 
		$main_content_class .= " three-fourths"; 
		if ($canon_options['sidebars_alignment'] == 'left') { $main_content_class .= " left-main-content"; }
	}

	// SET SLIDER ALIAS
	$alias = $canon_options_post['events_slider'];
	if (is_single()) {
		$cmb_event_slider = get_post_meta($post->ID, 'cmb_event_slider', true);
		if ($cmb_event_slider != "default" && !empty($cmb_event_slider)) {
			$alias = $cmb_event_slider;	
		}
	}


?>

<?php get_header(); ?>
	
		
		<!-- Start Outter Wrapper -->   
		<div class="outter-wrapper feature">
			<hr>
		</div>
		<!-- End Outter Wrapper --> 

		
		<!-- SLIDER -->
		<?php 

			if (function_exists("putRevSlider") && (!empty($alias)) && ($alias != "no_slider")) { 
				echo '<div class="outter-wrapper feature event-slider">';
				putRevSlider($alias); 
				echo '</div>';
			} 

		?>

		<?php if (is_single()) : ?>

			<!-- SINGLE EVENT NAV -->
			<div class="outter-wrapper tribe-single-event-nav">

				<div class="wrapper">

					<div class="tribe-events-crumbs clearfix ">

						<!-- Event Breadcrumb -->
						<p class="tribe-events-back">
							<a href="<?php echo esc_url( tribe_get_events_link() ); ?>"><?php printf('<em class="fa fa-calendar"></em> %s', esc_html__('All Events', 'loc_canon_venuex')); ?></a>
						</p>
						
						<!-- Event header -->
						<div id="tribe-events-header" <?php tribe_events_the_header_attributes() ?>>
							<!-- Navigation -->
							<ul class="tribe-events-sub-nav">
								<li class="tribe-events-nav-previous"><?php tribe_the_prev_event_link( '<em class="fa fa-caret-left"></em> %title%' ) ?></li>
								<li class="tribe-events-nav-next"><?php tribe_the_next_event_link( '%title% <em class="fa fa-caret-right"></em>' ) ?></li>
							</ul>
							<!-- .tribe-events-sub-nav -->
						</div>
						<!-- #tribe-events-header -->

					</div>

				</div>

			</div>

		<?php endif; ?>

		
		<!-- start outter-wrapper -->   
		<div class="outter-wrapper">
			<!-- start main-container -->
			<div class="main-container">
				<!-- start main wrapper -->
				<div class="main wrapper clearfix">
					<!-- start main-content -->
					<div class="<?php echo esc_attr($main_content_class); ?>">

						<!-- Start Post --> 
						<div class="clearfix">

							<!-- tribe-events-pg-template -->
							<div id="tribe-events-pg-template" class="canon-events">

								<?php tribe_events_before_html(); ?>
								<?php tribe_get_view(); ?>
								<?php tribe_events_after_html(); ?>
													 
							</div> 
							<!-- #tribe-events-pg-template -->

						</div>

					</div>
					<!-- end main-content -->

					<!-- SIDEBAR -->
					<?php if ($has_sidebar) { get_sidebar('events'); } ?> 

				</div>
				<!-- end main wrapper -->
			</div>
			 <!-- end main-container -->
		</div>
		<!-- end outter-wrapper -->
		

<?php get_footer(); ?>