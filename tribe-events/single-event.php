<?php
/**
 * Single Event Template
 * A single event. This displays the event title, description, meta, and
 * optionally, the Google map for the event.
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/single-event.php
 *
 * @package TribeEventsCalendar
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}


		$event_id = get_the_ID();

		// CMB
		$cmb_event_show_ticket_section = get_post_meta($event_id, 'cmb_event_show_ticket_section', true);
		$cmb_event_ticket_section = get_post_meta($event_id, 'cmb_event_ticket_section', true);
		$cmb_event_display_products = get_post_meta($event_id, 'cmb_event_display_products', true);

		if ($cmb_event_show_ticket_section == "checked" && !empty($cmb_event_display_products)) {
				
			// GET ATTACHED PRODUCTS
			$args = array(
			    'post_type' 		=> 'product',
				'meta_query' 		=> array(
					array(
						'key'     		=> 'cmb_attach_product_to_event',
						'value'			=> $event_id
					),
				),
			);

			// SORTING
			if ($cmb_event_display_products == "price_asc") {
			    $args = array_merge($args, array(
					'orderby'		=> 'meta_value_num',
					'meta_key'		=> '_price',
					'order'			=> 'ASC',
			    ));
			} elseif ($cmb_event_display_products == "price_desc") {
			    $args = array_merge($args, array(
					'orderby'		=> 'meta_value_num',
					'meta_key'		=> '_price',
					'order'			=> 'DESC',
			    ));
			}


			// QUERY
			$products = get_posts($args);	

			// GENERATE SHORTCODE
			$shortcodes_string = "";
			foreach ($products as $key => $product) {
				$shortcodes_string .= sprintf('[product id="%s"]', esc_attr($product->ID));
			}
			
		}


?>

<div id="tribe-events-content" class="tribe-events-single clearfix">
	
	<!-- Notices -->
	<?php tribe_the_notices() ?>
	
	<!-- Event Info -->
	<div class="tribe-single-event-info">
		
		<div class="tribe-events-schedule tribe-clearfix">
		 
			<?php the_title( '<h1 class="tribe-events-single-event-title">', '</h1>' ); ?>
			<?php if ( tribe_get_cost() ) : ?>
				<span class="tribe-events-cost"><?php echo tribe_get_cost( null, true ) ?></span>
			<?php endif; ?>

		</div>
		
		<!-- Event meta -->
		<?php do_action( 'tribe_events_single_event_before_the_meta' ) ?>
		<?php tribe_get_template_part( 'modules/meta' ); ?>
		
		
		
	</div>

	<!-- Event Main -->
	<div class="tribe-single-event-main">
		
		<?php while ( have_posts() ) :  the_post(); ?>

			<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<!-- Event featured image, but exclude link -->
				<?php echo tribe_event_featured_image( $event_id, 'full', false ); ?>
	
				<!-- Event content -->
				<?php do_action( 'tribe_events_single_event_before_the_content' ) ?>
				<div class="tribe-events-single-event-description tribe-events-content">
					<?php the_content(); ?>
				</div>
				<?php do_action( 'tribe_events_single_event_after_the_content' ) ?>
				
				<?php 

					// TICKETS
					if ($cmb_event_show_ticket_section == "checked") {
						echo '<div class="tribe-single-event-ticket-section">'; 
						
						// DISPLAY CUSTOM TICKET SECTION
						echo apply_filters('the_content', $cmb_event_ticket_section);
						
						// DISPLAY ATTACHED PRODUCTS
						if (!empty($cmb_event_display_products)) { echo do_shortcode($shortcodes_string); }

						echo '</div>'; 
					} 

				?>
				
				
				<?php do_action( 'tribe_events_single_event_after_the_meta' ) ?>

			</div> 
				
		<?php endwhile; ?>
	
	</div>
	
	<!-- COMMENTS -->
	<?php if ( get_post_type() == Tribe__Events__Main::POSTTYPE && tribe_get_option( 'showComments', false ) ) comments_template() ?>

</div>
<!-- #tribe-events-content -->
