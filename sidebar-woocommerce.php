<?php 

	$canon_options = get_option('canon_options');
	$canon_options_post = get_option('canon_options_post'); 

    $sidebar = $canon_options_post['buddypress_sidebar'];
	$aside_class = ($canon_options['sidebars_alignment'] == 'left') ? 'left-aside fourth' : 'right-aside fourth last';

    // FAILSAFE DEFAULT
    if (empty($sidebar)) { $sidebar = "canon_page_sidebar_widget_area"; }

	$sidebar_name = $GLOBALS['wp_registered_sidebars'][$sidebar]['name'];

?>

				 <!-- Start Main Sidebar -->
				<aside class="<?php echo esc_attr($aside_class); ?>">

					<?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar($canon_options_post['woocommerce_sidebar'])) : ?>  
						
						<h4><?php echo esc_attr($sidebar_name); ?></h4>
					    <p><i><?php esc_html_e("Please login and add some widgets to this widget area.", "loc_canon_venuex"); ?></i></p>
					
			        <?php endif; ?>  

				</aside>
				 <!-- Finish Sidebar -->


