<?php 

	$canon_options = get_option('canon_options');
	$canon_options_post = get_option('canon_options_post'); 
	
	$aside_class = ($canon_options['sidebars_alignment'] == 'left') ? 'left-aside fourth' : 'right-aside fourth last';

    // SET SIDEBAR
    $sidebar = $canon_options_post['events_sidebar'];
    if (is_single()) {
		$cmb_event_sidebar_id = get_post_meta($post->ID, 'cmb_event_sidebar_id', true);
		if ($cmb_event_sidebar_id != "default" && !empty($cmb_event_sidebar_id)) {
			$sidebar = $cmb_event_sidebar_id;	
		}
    }

	$sidebar_name = $GLOBALS['wp_registered_sidebars'][$sidebar]['name'];

?>

				 <!-- Start Main Sidebar -->
				<aside class="<?php echo esc_attr($aside_class); ?>">

					<?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar($sidebar)) : ?>  
						
						<h4><?php echo esc_attr($sidebar_name); ?></h4>
					    <p><i><?php esc_html_e("Please login and add some widgets to this widget area.", "loc_canon_venuex"); ?></i></p>
					
			        <?php endif; ?>  

				</aside>
				 <!-- Finish Sidebar -->


